<?php
get_header();
?>

<section class="container" id="content">
    <div class="loop">
        <div class="full-width background_repeat bg-center panel-row-style">
            <div class="widget widget-404">
                <div class="inside">
                    <div class="header">
                        <h1>Coś poszło nie tak</h1>
                    </div>
                    <a class="element-bt2" href="<?php bloginfo('home'); ?>">
						<span class="text">Zapraszamy do strony głównej</span>
                    </a>
                </div>


                <div class="background"></div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>
