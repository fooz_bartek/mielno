<?php
function register_post_areas_init() {
    $args = array(
      'public' => true,
      'label'  => 'Działki',
      'menu_icon'  => 'dashicons-admin-multisite',
    );
    register_post_type( 'areas', $args );
}
add_action( 'init', 'register_post_areas_init' );


add_action('init', 'my_rem_editor_from_post_type');
function my_rem_editor_from_post_type() {
    remove_post_type_support('areas', 'custom-fields');
    remove_post_type_support('areas', 'editor');
    remove_post_type_support('areas', 'title');
}






add_filter('manage_edit-areas_columns', 'add_new_areas_columns');
function add_new_areas_columns($areas_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';

    $new_columns['title'] = _x('areas Name', 'column name');

    $new_columns['number_registration'] = __('Nr. ewidencyjny');
    $new_columns['area'] = __('Powierzchnia');
    $new_columns['price_net'] = __('Cena netto');
    $new_columns['price_net_area'] = __('Cena netto/m<sup>2</sup>');
    $new_columns['price_net'] = __('Cena netto');
    $new_columns['status'] = __('Status');
    $new_columns['date'] = _x('Date', 'column name');

    return $new_columns;
}

// Add to admin_init function
add_action('manage_areas_posts_custom_column', 'manage_areas_columns', 10, 2);
function manage_areas_columns($column_name, $id) {
    global $wpdb;
    $variable = get_post_meta(get_the_ID());

    switch ($column_name) {
        case 'id':
            echo $id;
            break;

        case 'number_registration':
            echo $variable['number_registration'][0];
            break;

        case 'area':
            echo $variable['areafield'][0].' m<sup>2</sup>';
            break;

        case 'price_net':
            echo $variable['price_net'][0].' m<sup>2</sup>';
            break;

        case 'price_net_area':
            echo $variable['price_net_area'][0].' m<sup>2</sup>';
            break;

        case 'status':
            echo $variable['status'][0];
            break;

        default:
        break;
    }
}

add_action('save_post', 'set_rating_title', 12 );
function set_rating_title ($post_id) {
    if ($_POST['post_type'] == 'areas') {
        global $wpdb;

        $variable = get_post_meta(get_the_ID());
        $title = 'Działka nr.'.$variable['number'][0];
        $where = array( 'ID' => $post_id );
        $wpdb->update( $wpdb->posts, array(
            'post_title' => $title,
            'post_name' => sanitize_title($title)
        ), $where );
    }
}
