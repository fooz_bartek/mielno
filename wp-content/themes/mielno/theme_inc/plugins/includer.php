<?php
// Lista wtyczek

if(class_exists('SiteOrigin_Widget')){
	function fpweb_exclude_icon_families_filter( $families ){
	 // if( isset( $families['fontawesome'] ) unset( $families['fontawesome'] );
		if( isset( $families['icomoon'] ) ) unset( $families['icomoon'] );
		if( isset( $families['genericons'] ) ) unset( $families['genericons'] );
		if( isset( $families['typicons'] ) ) unset( $families['typicons'] );
		if( isset( $families['elegantline'] ) ) unset( $families['elegantline'] );

		return $families;

	}
	add_filter( 'siteorigin_widgets_icon_families', 'fpweb_exclude_icon_families_filter' );


	// SO Plugin adds
	function fpweb_mytheme_add_widget_tabs($tabs) {
		$tabs[] = array(
				'title' => __(TEMPL_AUTHOR, 'engine'),
				'filter' => array(
						'groups' => array(TEMPL_NAME)
				)
		);

		return $tabs;
	}
	add_filter('siteorigin_panels_widget_dialog_tabs', 'fpweb_mytheme_add_widget_tabs', 20);

	// Dodanie opcji do rzędu w postaci chekboxa
	//https://siteorigin.com/docs/page-builder/hooks/custom-row-settings/
	function custom_row_style_fields($fields) {

		$fields['box_height'] = array(
				'name'        => __('Section height', 'engine'),
				'type'        => 'measurement',
				'group'       => 'attributes',
				'description' => __('Set minimal height of element', 'engine'),
				'priority'    => 4,
		);

		$fields['background_repeat'] = array(
				'name'        => __('No repeat bg', 'engine'),
				'type'        => 'checkbox',
				'group'       => 'design',
				'description' => __('Set br repeat', 'engine'),
				'priority'    => 8,
		);
		$fields['background_position'] = array(
				'name'        => __('Bg postion', 'engine'),
				'type'        => 'select',
				'group'       => 'design',
				'options' => array(
					'bg-center' => __( 'center', 'engine' ),
					'parallax' => __( 'brak dla parallaxy', 'engine' ),
					'bg-l_u' => __( 'left up', 'engine' ),
					'bg-l_d' => __( 'left down', 'engine' ),
					'bg-p_u' => __( 'right up', 'engine' ),
					'bg-p_d' => __( 'right down', 'engine' ),
				),
				'priority'    => 8,
		);
		return $fields;
	}
	add_filter( 'siteorigin_panels_row_style_fields', 'custom_row_style_fields' );

	function custom_row_style_attributes( $attributes, $args ) {

			if( !empty( $args['box_height'] ) ) {
					$attributes['style'] .= 'height:'.$args['box_height'].';';
			}
			if( $args['background_repeat'] ) {
					array_push($attributes['class'], 'background_norepeat');
			}else{
					array_push($attributes['class'], 'background_repeat');
			}
			if( !empty( $args['background_position'] ) ) {
					array_push($attributes['class'], $args['background_position'] );
			}

			return $attributes;
	}
	add_filter('siteorigin_panels_row_style_attributes', 'custom_row_style_attributes', 10, 2);

		// Register Widgets
		//require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/przykladowy/przykladowy.php');

		//require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/tekst/tekst.php');
		//require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/slick/slick.php');
		//require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/search_form/search_form.php');
		//require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/widget_menu/widget_menu.php');
		//require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/copy_clipboard/copy_clipboard.php');

 		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_welcome/mielno_welcome.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_welcome2/mielno_welcome2.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_welcome3/mielno_welcome3.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_welcome4/mielno_welcome4.php');
		
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_quotation/mielno_quotation.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_bigbutton/mielno_bigbutton.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_text_photo/mielno_text_photo.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_text_photo_artist/mielno_text_photo_artist.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_icons/mielno_icons.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_icons_slider/mielno_icons_slider.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_headerlinks/mielno_headerlinks.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_areas_list/mielno_areas_list.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_gallery/mielno_gallery.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_footer/mielno_footer.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_footer2/mielno_footer2.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_section_link/mielno_section_link.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_opinion/mielno_opinion.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_localization/mielno_localization.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_contact_list/mielno_contact_list.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_map/mielno_map.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_invest_step/mielno_invest_step.php');
		require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_invest_button/mielno_invest_button.php');


		//require_once(THEME_PLUGIN_DIRECTORY.'so_plugins/widget_top/widget_top.php');

} // end iff siteorigin activated


// add numeric pagination
		require_once(THEME_PLUGIN_DIRECTORY.'pagination/pagination.php');

?>
