<?php
$nazwa_widgetu="Twoja nazwa";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
$widgetstyle = ' style="';
if( $instance['ustawienia']['border-top'] > 0){
    $widgetstyle .= ' border-top:'.$instance['ustawienia']['border-top'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-botom'] > 0){
    $widgetstyle .= ' border-bottom:'.$instance['ustawienia']['border-bottom'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-left'] > 0){
    $widgetstyle .= ' border-left:'.$instance['ustawienia']['border-left'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-right'] > 0){
    $widgetstyle .= ' border-right:'.$instance['ustawienia']['border-right'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
$widgetstyle .= '" ';
$imgatr = array( 'class' => 'img-responsive' );
?>
<div id="<?php echo $widgetid; ?>" class="so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="<?php echo $nazwa_widgetu . ' ' . $instance['panels_info']['style']['class'] . ' ' . $widgetpanelstyle; ?>" id="<?php echo $instance['panels_info']['style']['id']; ?>" <?php
    echo $widgetanimation . $widgetstyle; ?> >
        <?php /** Content your widget **/ ?>

        <div class="col-xs-12 col-sm-12">
            <!-- Wyświetlanie plików pdf -->
	        <?php
	        $field = get_field('download_zip_file');
	        if( $field ): ?>
                <div class="files">
			        <?php

				        $file_url = wp_get_attachment_url($field);
				        $file_new_window = 'onclick="window.open(this.href); return false;"';
				        $file_description = $instance['opis_pliku'];

				        $pdf_extension = '<i class="fa fa-download" aria-hidden="true"></i>';

				        ?>

                        <a class="pobierz-plik-wrapper" href="<?php echo $file_url ?>"><i class="fa fa-download" aria-hidden="true"></i><br/></a>

                </div>
	        <?php endif; ?>
        </div>

        <?php /** End ontent your widget **/ ?>
    </div>
</div>
