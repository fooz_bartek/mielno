<?php

/*
Widget Name: Mielno - Opinion
Description: Widget na top strony
Author: Bartek Szewczyk
*/


class Opinion_Widget extends SiteOrigin_Widget {

		function __construct() {
            define('SO_PLUGIN_SLUG_Opinion', 'mielno_opinion');
            define('SO_PLUGIN_DIR_Opinion', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_opinion/');
            define('SO_PLUGIN_URL_Opinion', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_opinion/');
            define('SO_PLUGIN_VER_Opinion', '1');

			$form_options = array(
				'background' => array(
		    		'type' => 'media',
		       		'label' => __( 'Tło', 'engine' ),
				    'choose' => __( 'Choose image', 'engine' ),
				    'update' => __( 'Set image', 'engine' ),
				    'library' => 'image',
				    'fallback' => true
		    	),
				'header' => array(
			        'type' => 'tinymce',
			        'label' => __('Nagłówek', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			        ),
			    ),
				'text' => array(
			        'type' => 'textarea',
			        'label' => __('Tekst', 'engine' ),
			        'rows' => 5,
			    ),
				'opinions' => array(
			        'type' => 'repeater',
			        'label' => __( 'Opinia' , 'engine' ),
			        'item_name'  => __( 'Opinia', 'engine' ),
			        'item_label' => array(
			            'selector'     => "[id*='opinion_author']",
			            'update_event' => 'change',
			            'value_method' => 'val'
			        ),
			        'fields' => array(
			            'opinion_text' => array(
			                'type' => 'textarea',
					        'rows' => 2,
			                'label' => __( 'Treść', 'engine' )
			            ),
						'opinion_author' => array(
					    	'type' => 'text',
				            'label' => __( 'Autor', 'engine' )
				        ),
			        )
			    ),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'opinion-widget',
				__('Opinie klientów', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_opinion/'
				);
		}

        function initialize(){
            $this->register_frontend_scripts(
                array(
                    array(SO_PLUGIN_SLUG_Opinion, SO_PLUGIN_URL_Opinion . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER_Opinion, true)
                )
            );
        }

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Opinion Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_opinion/', 'Opinion_Widget');
?>
