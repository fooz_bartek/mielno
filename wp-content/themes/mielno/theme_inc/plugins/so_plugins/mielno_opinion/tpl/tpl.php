<?php
$nazwa_widgetu = "opinion-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-opinion so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header"><?php echo $instance['header']; ?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-5 col-md-5 cl1">
				<div class="line"></div>
				<div class="text"><?php echo $instance['text']; ?></div>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 cl2">
				<div class="opinions slick">
					<?php foreach ($instance['opinions'] as $key => $value) { ?>
						<div class="opinion">
							<div class="opinion_text"><div class="in"><?php echo $value['opinion_text']; ?></div></div>
							<div class="opinion_author"><?php echo $value['opinion_author']; ?></div>
							<div class="background"></div>
							<div class="triangle"></div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php if($instance['background']){ ?>
		<div class="background" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>
	<?php } ?>
</div>
