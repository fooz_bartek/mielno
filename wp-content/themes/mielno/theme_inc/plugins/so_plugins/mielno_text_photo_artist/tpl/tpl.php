<?php
$nazwa_widgetu = "textphotoartist-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>
<?php
$scheme = $instance['scheme'];
if(!$instance['scheme'])
	$scheme = 'scheme1';
?>
<div class="widget widget-textphotoartist so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" data-scheme="<?php echo $scheme; ?>">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="header"><?php echo $instance['header']; ?></div>
			</div>
		</div>
		<div class="row">
			<?php if($scheme == 'scheme1'){ ?>
				<div class="col-sm-4 col-md-4 cl1">
					<div class="photo photo1" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
				</div>
				<div class="col-sm-4 col-md-4 cl2">
					<div class="line"></div>
					<div class="text"><?php echo nl2br($instance['text']); ?></div>
					<div class="photo photo2" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
				</div>
				<div class="col-sm-4 col-md-4 cl3">
					<div class="text"><?php echo nl2br($instance['text2']); ?></div>
					<?php
					if($instance['button_active'] == 'active'){
						$href = '#';
                        if(strlen($instance['button_url']) > 0){
                            $href = $instance['button_url'];
                            if(strpos($instance['button_url'], 'post: ') == 0){
                                $hreflId = str_replace('post: ','', $instance['button_url']);
                                $href = get_permalink($hreflId);
                            }
                        }
								 ?>
						<a class="element-bt-arrow hover-black" href="<?php echo $href; ?>">
							<?php echo $instance['button_text']; ?>
							<svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
								<line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
								<line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
								<line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
							</svg>
						</a>
					<?php } ?>
					<div class="photo photo3" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
				</div>
			<?php } ?>


			<?php if($scheme == 'scheme2'){ ?>
				<div class="col-sm-4 col-md-4 cl1">
					<div class="photo photo1" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
				</div>
				<div class="col-sm-4 col-md-4 cl2">
					<div class="photo photo2" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
				</div>
				<div class="col-sm-4 col-md-4 cl3">
					<div class="line"></div>
					<div class="text"><?php echo nl2br($instance['text']); ?></div>
					<?php
					if($instance['button_active'] == 'active'){
						$href = '#';
                        if(strlen($instance['button_url']) > 0){
                            if(strpos($instance['button_url'], 'post:') === false){
                                $href = $instance['button_url'];
                            } else {
                                $hreflId = str_replace('post: ','', $instance['button_url']);
                                $href = get_permalink($hreflId);
                            }
                        }
								 ?>
						<a class="element-bt-arrow hover-black" href="<?php echo $href; ?>">
							<?php echo $instance['button_text']; ?>
							<svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
								<line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
								<line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
								<line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
							</svg>
						</a>
					<?php } ?>
					<div class="photo photo3" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
				</div>
			<?php } ?>

		</div>
	<?php //echo '<pre>',print_r($instance,1),'</pre>'; ?>
</div>
