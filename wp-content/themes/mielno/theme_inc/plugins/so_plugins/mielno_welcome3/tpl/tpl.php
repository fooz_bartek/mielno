<?php
$nazwa_widgetu = "welcome3-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-welcome3 so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-3">
				<img class="detail" src="<?php	echo wp_get_attachment_image_src($instance['detail'], 'full', true)[0]; ?>" />
			</div>
			<div class="col-sm-1 col-md-1"></div>
			<div class="col-xs-12 col-sm-8 col-md-8">
				<div class="row">
					<div class="col-md-12 col-md-12"><div class="header"><?php echo $instance['header']; ?></div></div>
				</div>
				<div class="row">
					<div class="col-md-12 col-md-12 cl1">
						<div class="line"></div>
						<div class="text text1"><?php echo $instance['text']; ?></div>
						<div class="text text2"><?php echo $instance['text2']; ?></div>
						<?php
						if($instance['button_active'] == 'active'){
							$href = '#';
                            if(strlen($instance['button_url']) > 0){
                                if(strpos($instance['button_url'], 'post:') === false){
                                    $href = $instance['button_url'];
                                } else {
                                    $hreflId = str_replace('post: ','', $instance['button_url']);
                                    $href = get_permalink($hreflId);
                                }
                            }
									 ?>
							<a class="element-bt-arrow hover-black" href="<?php echo $href; ?>">
								<?php echo $instance['button_text']; ?>
								<svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
									<line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
									<line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
									<line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
								</svg>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="background" style="background-image: url(<?php	echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>
</div>
