<?php
$nazwa_widgetu = "headerlinks-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-headerlinks so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<a href="<?php echo $instance['link']; ?>" target="_blank">
		<img src="<?php	echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>" />
		<?php echo $instance['text']; ?>
	</a>
	<?php //echo '<pre>',print_r($instance,1),'</pre>'; ?>
</div>
