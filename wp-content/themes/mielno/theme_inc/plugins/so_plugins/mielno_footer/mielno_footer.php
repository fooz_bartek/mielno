<?php

/*
Widget Name: Mielno - footer
Description:
Author: Bartek Szewczyk
*/

class Footer_Widget extends SiteOrigin_Widget {

		function __construct() {
            define('SO_PLUGIN_SLUG_Footer', 'mielno_footer-widget');
            define('SO_PLUGIN_DIR_Footer', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_footer/');
            define('SO_PLUGIN_URL_Footer', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_footer/');
            define('SO_PLUGIN_VER_Footer', '1');

			$form_options = array(
				'photo1' => array(
			       'type' => 'media',
			       'label' => __( 'Osoba 1: zdjęcie', 'engine' ),
			       'choose' => __( 'Choose image', 'engine' ),
			       'update' => __( 'Set image', 'engine' ),
			       'library' => 'image'
			    ),
				'name1' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 1: Imię i nazwisko', 'engine' ),
			    ),
				'position1' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 1: Stanowisko', 'engine' ),
			    ),
				'email1' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 1: Email', 'engine' ),
			    ),
				'phone1' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 1: Phone', 'engine' ),
			    ),


				'photo2' => array(
			       'type' => 'media',
			       'label' => __( 'Osoba 2: zdjęcie', 'engine' ),
			       'choose' => __( 'Choose image', 'engine' ),
			       'update' => __( 'Set image', 'engine' ),
			       'library' => 'image'
			    ),
				'name2' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 2: Imię i nazwisko', 'engine' ),
			    ),
				'position2' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 2: Stanowisko', 'engine' ),
			    ),
				'email2' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 2: Email', 'engine' ),
			    ),
				'phone2' => array(
			       'type' => 'text',
			       'label' => __( 'Osoba 2: Phone', 'engine' ),
			    ),


				'link_text' => array(
				    'type' => 'text',
				    'label' => __( 'Link: Treść', 'engine' ),
				),
				'link_url' => array(
			       'type' => 'text',
			       'label' => __( 'Link: Adres', 'engine' ),
			    ),


				'form_header' => array(
			        'type' => 'tinymce',
			        'label' => __('Nagłówek', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
			            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
			            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
			            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
			            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
			        ),
			    ),
				'form' => array(
					 'type' => 'text',
					 'label' => __('Formularz kontaktowy', 'engine' ),
					 'rows' => 5
				)
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'footer-widget',
				__('Footer widget', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_footer/'
				);
		}

        function initialize(){
            $this->register_frontend_scripts(
                array(
                    array(SO_PLUGIN_SLUG_Footer, SO_PLUGIN_URL_Footer . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER_Footer, true)
                )
            );
        }

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Footer Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_footer/', 'Footer_Widget');
?>
