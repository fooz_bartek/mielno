$(document).on('focus', 'footer input[type="text"], footer input[type="email"], footer textarea', function() {
    $(this).closest('label').addClass('focus');
})
$(document).on('blur', 'footer input[type="text"], footer input[type="email"], footer textarea', function() {
    $('footer label').removeClass('focus');

    input_text_length = $(this).val()
    if (input_text_length.length > 0)
        $(this).closest('label').addClass('edit');
    else
        $(this).closest('label').removeClass('edit');
});

$('footer input[type="checkbox"]').on('click', function() {
    $(this).closest('label').toggleClass('checked');
});
