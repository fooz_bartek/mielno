<?php
$nazwa_widgetu = "footer-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-footer so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="container">
		<div class="row table-row">
			<div class="col-md-1 hidden-xs hidden-sm col-block"></div>
			<div class="col-xs-12 col-sm-5 col-md-5 col-block">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="photo" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['photo1'], 'full', true)[0]; ?>)"></div>
						<div class="name"><?php echo $instance['name1']; ?></div>
						<div class="position"><?php echo $instance['position1']; ?></div>
						<a class="email" href="mailto:<?php echo $instance['email1']; ?>"><?php echo $instance['email1']; ?></a>
						<a class="phone" href="tel:<?php echo $instance['phone1']; ?>">tel. <?php echo $instance['phone1']; ?></a>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="photo" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['photo2'], 'full', true)[0]; ?>)"></div>
						<div class="name"><?php echo $instance['name2']; ?></div>
						<div class="position"><?php echo $instance['position2']; ?></div>
						<a class="email" href="mailto:<?php echo $instance['email2']; ?>"><?php echo $instance['email2']; ?></a>
						<a class="phone" href="tel:<?php echo $instance['phone2']; ?>">tel. <?php echo $instance['phone2']; ?></a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<a class="link" href="<?php echo $instance['link_url']; ?>"><?php echo $instance['link_text']; ?></a>
					</div>
				</div>
			</div>
			<div class="col-sm-1 col-md-1 hidden-xs col-block">
				<div class="line"></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-block">
				<div class="header"><?php echo $instance['form_header']; ?></div>
				<div class="form"><?php echo do_shortcode($instance['form']); ?></div>
			</div>
			<div class="col-md-1 hidden-sm hidden-xs hidden-sm col-block"></div>
		</div>
	</div>
</div>
