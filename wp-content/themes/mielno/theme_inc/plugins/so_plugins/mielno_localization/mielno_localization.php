<?php

/*
Widget Name: Mielno - Localization
Description: Widget na top strony
Author: Bartek Szewczyk
*/

class Localization_Widget extends SiteOrigin_Widget {

		function __construct() {
            define('SO_PLUGIN_SLUG_Localization', 'mielno_localization');
            define('SO_PLUGIN_DIR_Localization', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_localization/');
            define('SO_PLUGIN_URL_Localization', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_localization/');
            define('SO_PLUGIN_VER_Localization', '1');

			$form_options = array(
				'background' => array(
		    		'type' => 'media',
		       		'label' => __( 'Tło', 'engine' ),
				    'choose' => __( 'Choose image', 'engine' ),
				    'update' => __( 'Set image', 'engine' ),
				    'library' => 'image',
				    'fallback' => true
		    	),
				'icons' => array(
					'type' => 'repeater',
					'label' => __( 'Ikony' , 'engine' ),
					'item_name'  => __( 'Ikona', 'siteorigin-widgets' ),
					'item_label' => array(
						'selector'     => "[id*='icon_header']",
						'update_event' => 'change',
						'value_method' => 'val'
					),
					'fields' => array(
						'icon_header' => array(
							'type' => 'text',
							'label' => __( 'Nagłowek', 'engine' )
						),
						'icon_desc' => array(
				        	'type' => 'textarea',
				            'label' => __( 'Opis', 'engine' )
				        ),
						'icon_picture' => array(
							'type' => 'media',
							'label' => __( 'Grafika', 'engine' ),
							'choose' => __( 'Dodaj grafike', 'engine' ),
							'update' => __( 'Ustaw grafike', 'engine' ),
							'library' => 'image'
						),
					)
				),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'localization-widget',
				__('Lokalizacja - mapa', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_localization/'
				);
		}

        function initialize(){
            $this->register_frontend_scripts(
                array(
                    array(SO_PLUGIN_SLUG_Localization, SO_PLUGIN_URL_Localization . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER_Localization, true)
                )
            );
            $this->register_frontend_styles(
                array(
                    array(SO_PLUGIN_SLUG_Localization, SO_PLUGIN_URL_Localization . 'tpl/main.css', array(), SO_PLUGIN_VER_Localization)
                )
            );
        }

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Localization Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_localization/', 'localization_Widget');
?>
