$('.widget-localization .slick').slick({
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: false,

    slidesToShow: 8,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 1400,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});



// $('.widget-localization .maps').scrollTop(100);
// $('.widget-localization .maps').scrollLeft(550);

clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");
$(document).on(clickHandler, '.widget-localization .maps', function() {
    $('.widget-localization .hint_mobile').hide();
});
