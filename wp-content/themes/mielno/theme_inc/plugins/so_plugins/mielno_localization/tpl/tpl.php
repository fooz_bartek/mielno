<?php
$nazwa_widgetu = "mielno_localization";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-localization so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="icons">
			<div class="row">
				<div class="col-md-12">
					<div class="slider slick">
						<?php
						if($instance['icons'])
						foreach ($instance['icons'] as $key => $value) {
							?>
							<div class="icon">
								<div class="picture">
									<?php
									$imageInfo = pathinfo(wp_get_attachment_image_src($value['icon_picture'], 'full', true)[0]);
									$svgconvert = '';
									if($imageInfo['extension'] == 'svg')
										$svgconvert = 'svg-convert';
									 ?>
									<img class="<?php echo $imageInfo['extension']; ?> <?php echo $svgconvert; ?>" src="<?php	echo wp_get_attachment_image_src($value['icon_picture'], 'full', true)[0]; ?>" alt="<?php echo $value['icon_header']; ?>" />
								</div>
								<div class="title"><?php echo $value['icon_header']; ?></div>
								<div class="desc"><?php echo $value['icon_desc']; ?></div>
							</div>
						<?php }	?>
					</div>
				</div>
			</div>
		<div class="background" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>
	</div>

	<div class="maps">
        <div class="hint_mobile">
            Przesuń palcem mapę
            <svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
				<line fill="none" stroke="#353535" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"></line>
				<line fill="none" stroke="#353535" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"></line>
				<line fill="none" stroke="#353535" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"></line>
			</svg>
        </div>
		<div class="areamaps">
			<div class="hotspots">
				<div class="hotspot" data-id="1">
					<div class="icon"></div>
				</div>
				<div class="hotspot" data-id="2">
					<div class="icon"></div>
				</div>
				<div class="hotspot" data-id="3">
					<div class="icon"></div>
					<div class="text">Unieście  II<br />Przystań</div>
				</div>
				<div class="hotspot" data-id="4">
					<div class="icon"></div>
					<div class="text">Port rybacki<br />świeża ryba</div>
				</div>
				<div class="hotspot" data-id="5">
					<div class="icon"></div>
				</div>
				<div class="hotspot" data-id="6">
					<div class="icon"></div>
					<div class="text">Unieście  I<br />Przystań</div>
				</div>
				<div class="hotspot" data-id="7">
					<div class="icon"></div>
				</div>
				<div class="hotspot" data-id="8">
					<div class="icon"></div>
					<div class="text">Slip do<br />wodowania</div>
				</div>
				<div class="hotspot" data-id="9">
					<div class="icon"></div>
				</div>
				<div class="hotspot" data-id="10">
					<div class="icon"></div>
					<div class="text">Kanał<br />Jemneński</div>
				</div>
				<div class="hotspot" data-id="11">
					<div class="icon"></div>
				</div>
				<div class="hotspot" data-id="12">
					<div class="icon"></div>
					<div class="text">Jamno<br />przystań</div>
				</div>
				<div class="hotspot" data-id="mielno">
					<div class="icon"></div>
					<a class="area" href="#"></a>
					<div class="text">Apartamenty<br />Bluemarine</div>
				</div>
			</div>
			<div class="anim">
				<svg class="line" x="0px" y="0px" width="1920px" height="1099px" viewBox="0 0 1920 1099" enable-background="new 0 0 1920 1099" xml:space="preserve">
					<g id="boats">
						<g>
							<polygon fill="#E6EBED" points="-1.119,-25.91 -1.119,11.846 -23.434,11.846"/>
							<polygon fill="#E6EBED" points="2.204,-16.747 2.204,11.845 22.481,11.845"/>
							<path fill="#E6EBED" d="M-23.586,15.169h47.172c0,0-4.234,10.741-23.519,10.741S-23.586,15.169-23.586,15.169"/>
							<animateMotion dur="30s" repeatCount="indefinite"><mpath xlink:href="#boatpath1"/></animateMotion>
						</g>
						<g>
							<polygon fill="#E6EBED" points="-1.119,-25.91 -1.119,11.846 -23.434,11.846"/>
							<polygon fill="#E6EBED" points="2.204,-16.747 2.204,11.845 22.481,11.845"/>
							<path fill="#E6EBED" d="M-23.586,15.169h47.172c0,0-4.234,10.741-23.519,10.741S-23.586,15.169-23.586,15.169"/>
							<animateMotion dur="50s" repeatCount="indefinite"><mpath xlink:href="#boatpath2"/></animateMotion>
						</g>
						<g>
							<polygon fill="#E6EBED" points="-1.119,-25.91 -1.119,11.846 -23.434,11.846"/>
							<polygon fill="#E6EBED" points="2.204,-16.747 2.204,11.845 22.481,11.845"/>
							<path fill="#E6EBED" d="M-23.586,15.169h47.172c0,0-4.234,10.741-23.519,10.741S-23.586,15.169-23.586,15.169"/>
							<animateMotion dur="30s" repeatCount="indefinite"><mpath xlink:href="#boatpath3"/></animateMotion>
						</g>
						<g>
							<polygon fill="#E6EBED" points="-1.119,-25.91 -1.119,11.846 -23.434,11.846"/>
							<polygon fill="#E6EBED" points="2.204,-16.747 2.204,11.845 22.481,11.845"/>
							<path fill="#E6EBED" d="M-23.586,15.169h47.172c0,0-4.234,10.741-23.519,10.741S-23.586,15.169-23.586,15.169"/>
							<animateMotion dur="50s" repeatCount="indefinite"><mpath xlink:href="#boatpath4"/></animateMotion>
						</g>
						<path id="boatpath1" fill="none" stroke="none" stroke-miterlimit="10" d="M558.76,780.445
							c27.772-28.28,191.232-121.685,244.84-105.729c53.609,15.955,166.498,2.436,156.4,36.365
							c-10.097,33.928-166.899,59.705-307.893,98.995c-19.985,5.569-92.694,83.714-106.153,72.642
							C523.127,863.937,543.157,796.333,558.76,780.445z"/>
						<path id="boatpath2" fill="none" stroke="none" stroke-miterlimit="10" d="M1877.444,447.34
							c23.86,81.762-32.139,279.295-161.324,316.994c-129.187,37.699-282.639-98.715-306.499-180.479
							c-23.86-81.76,75.815-129.626,205.002-167.325c47.918-13.983,113.6-197.463,155.257-192.641
							C1840.529,232.069,1862.435,395.905,1877.444,447.34z"/>
						<path id="boatpath3" fill="none" stroke="none" stroke-miterlimit="10" d="M1203.048,589.083
							c-7.214-41.89,120.532-151.969,296.285-182.23c175.755-30.261,332.962,30.756,340.175,72.647
							c7.214,41.889-133.736,75.286-309.49,105.548c-65.192,11.226-201.307,114.539-253.148,115.212
							C1188.946,701.399,1207.585,615.436,1203.048,589.083z"/>
						<path id="boatpath4" fill="none" stroke="none" stroke-miterlimit="10" d="M1435.887,963.197
							c-36.931-14.395-256.865-105.062-263.305-160.621c-6.44-55.562-105.66-125.604-70.496-129.681
							c35.163-4.075,200.609,99.995,332.158,164.163c43.498,21.218,166.194,80.778,161.316,97.511
							C1587.283,962.947,1456.634,971.284,1435.887,963.197z"/>
					</g>
					<path class="dash" fill="none" stroke="#577a98" stroke-miterlimit="10" d="M1205,373l394.885,609.318l-119.633-37.266L1038,761l-171-11l-269.48-40.269L954,620l99.5-36.5L1125,514l41.227-47.367L1200.367,417L1205,373z"/>
				</svg>
			</div>
		</div>
		<div class="areamaps_size"></div>
	</div>
</div>
