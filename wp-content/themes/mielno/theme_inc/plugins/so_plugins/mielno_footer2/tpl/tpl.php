<?php
$nazwa_widgetu = "footer2-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-footer2 so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-4 col-block">
				<div class="text"><?php echo $instance['text1']; ?></div>
				<div class="socials">
					<?php
					foreach ($instance['social'] as $social) {
						echo '<a class="'.$social['social_type'].'" href="'.$social['social_url'].'"></a>';
					}
					?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-block">
				<div class="text"><?php echo $instance['text2']; ?></div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-block">
				<div class="text"><?php echo $instance['text3']; ?></div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-2 col-block">
				<div class="text">
					<?php echo $instance['text4']; ?>
					<div class="links">
						<?php
						$items = wp_get_nav_menu_items('Menu');
						foreach ($items as $item) {
		                    $href = $item->url;
							foreach ($item->classes as $keyClass => $valClass){
		                        if($valClass == 'off')
		                            $href = '#';
		                    }
							echo '<a href="'.$href.'">'.$item->title.'</a>';
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<hr />
				<div class="text2">Wszystkie przedstawione parametry, wizualizacje i możliwości inwestycyjne stanowią wyłącznie wartości szacunkowe opracowane przez Saveinvest Sp. z o.o. Nie gwarantują one stałości prawa miejscowegodla danej gminy, realizacji w identycznej formie przedstawianych inwestycji, utrzymania się w czasie parametrów zabudowy na oferowanych działkach i sukcesu zakładanych przez Kupującego scenariuszy inwestycyjnych, jak również nie mogą stanowić podstawy do formułowania w stosunku do Saveinvest Sp. z o.o. jakichkolwiek roszczeń.</div>
				<div class="text3">© 2018 Mielno Marina. Projekt i realizacja: <a href="https://www.funktional.pl/realizacje/" target="_blank">Funktional</a></div>
			</div>
		</div>
	</div>
	<div class="background" style="background-image: url(<?php	echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>

	<?php //echo '<pre>',print_r($instance,1),'</pre>'; ?>
</div>
