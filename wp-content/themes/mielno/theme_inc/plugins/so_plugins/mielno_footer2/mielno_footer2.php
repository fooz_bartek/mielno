<?php

/*
Widget Name: Mielno - Footer2
Description: Widget na top strony
Author: Bartek Szewczyk
*/


class Footer2_Widget extends SiteOrigin_Widget {

		function __construct() {
			define('SO_PLUGIN_SLUG', 'footer2-widget');
	        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_footer2/');
	        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_footer2/');
	        define('SO_PLUGIN_VER', '1');

			$form_options = array(
				'background' => array(
			       'type' => 'media',
			       'label' => __( 'Tło', 'engine' ),
			       'choose' => __( 'Choose image', 'engine' ),
			       'update' => __( 'Set image', 'engine' ),
			       'library' => 'image',
			       'fallback' => true
			    ),
				'text1' => array(
			        'type' => 'tinymce',
			        'label' => __('Kolumna 1', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			        ),
			    ),
				'text2' => array(
			        'type' => 'tinymce',
			        'label' => __('Kolumna 2', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			        ),
			    ),
				'text3' => array(
			        'type' => 'tinymce',
			        'label' => __('Kolumna 3', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			        ),
			    ),
				'text4' => array(
			        'type' => 'tinymce',
			        'label' => __('Kolumna 4', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			        ),
			    ),
				'social' => array(
			        'type' => 'repeater',
			        'label' => __( 'Social' , 'engine' ),
			        'item_name'  => __( 'Social', 'siteorigin-widgets' ),
			        'item_label' => array(
			            'selector'     => "[id*='repeat_text']",
			            'update_event' => 'change',
			            'value_method' => 'val'
			        ),
			        'fields' => array(
			            'social_url' => array(
			                'type' => 'text',
			                'label' => __('Url', 'engine' )
			            ),
						'social_type' => array(
					        'type' => 'select',
					        'label' => __('Typ kanału', 'engine' ),
					        'default' => 'the_other_thing',
					        'options' => array(
					            'facebook' => __( 'Facebook', 'engine' ),
					            'youtube' => __( 'Youtube', 'engine' ),
					        )
					    )
			        )
			    ),

			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'footer2-widget',
				__('Footer2 widget', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_footer2/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Footer2 Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_footer2/', 'Footer2_Widget');
?>
