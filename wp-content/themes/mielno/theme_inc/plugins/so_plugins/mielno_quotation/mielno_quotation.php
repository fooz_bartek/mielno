<?php

/*
Widget Name: Mielno - Quotation
Description: Widget dla cytatow
Author: Bartek Szewczyk
*/


class Quotation_Widget extends SiteOrigin_Widget {
		function __construct() {
			define('SO_PLUGIN_SLUG', 'quotation-widget');
	        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_quotation/');
	        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_quotation/');
	        define('SO_PLUGIN_VER', '1');

			$form_options = array(
				'text' => array(
			       'type' => 'textarea',
			       'label' => __( 'Type a message', 'engine' ),
			       'rows' => 4
			    ),
				'picture' => array(
			       'type' => 'media',
			       'label' => __( 'Choose a media thing', 'engine' ),
			       'choose' => __( 'Choose image', 'engine' ),
			       'update' => __( 'Set image', 'engine' ),
			       'library' => 'image',
			       'fallback' => true
			    ),
				'name' => array(
			       'type' => 'text',
			       'label' => __( 'Imię i nazwisko', 'engine' ),
			    ),
				'position' => array(
			       'type' => 'text',
			       'label' => __( 'Stanowisko', 'engine' ),
			    ),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'quotation-widget',
				__('Cytat', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_quotation/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Quotation Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_quotation/', 'Quotation_Widget');
?>
