<?php
$nazwa_widgetu = "quotation-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>


<div class="widget widget-quotation so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="row">
		<div class="col-sm-2 col-md-2 hidden-xs"><div class="quotation_marks"></div></div>
		<div class="col-sm-2 col-md-2 hidden-xs"><div class="line"></div></div>
		<div class="col-xs-8 col-sm-5 col-md-5">
			<div class="text text20"><?php echo $instance['text']; ?></div>
		</div>
		<div class="col-sm-1 col-md-1 hidden-xs"></div>
		<div class="col-xs-4 col-sm-2 col-md-2">
			<div class="photo" style="background-image: url(<?php	echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
			<div class="name"><?php echo $instance['name']; ?></div>
			<div class="position"><?php echo $instance['position']; ?></div>
		</div>
	</div>
	<?php //echo '<pre>',print_r($instance,1),'</pre>'; ?>
</div>
