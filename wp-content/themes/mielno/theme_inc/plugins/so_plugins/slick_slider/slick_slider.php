<?php

/*
Widget Name: Home Top slider widget
Description: Top slider.
Author: Wojciech Zdziejowski
*/

siteorigin_widget_register('Home top', THEME_PLUGIN_DIRECTORY . 'so_plugins/home_top/', 'Home_Top_Widget');
class Home_Top_Widget extends SiteOrigin_Widget
{

    const SO_PLUGIN_SLUG = 'home-top-widget';
    const SO_PLUGIN_DIR =  THEME_PLUGIN_DIRECTORY . 'so_plugins/slick_slider/';
    const SO_PLUGIN_URL =  THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/slick_slider/';
    const SO_PLUGIN_VER =  1;

    function __construct()
    {

        //================================
        //Call the parent constructor with the required arguments.
        //DATA FOR WIDGET
        //================================
        parent::__construct(
        // The unique id for your widget.
            self::SO_PLUGIN_SLUG,

            // The name of the widget for display purposes.
            __('Top strony głównej', 'engine'),

            // The $widget_options array, which is passed through to WP_Widget.
            // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
            array(
                'description' => __('Obraz i tekst strony głównej', 'engine'),
                'help' => TEMPL_OWNER_URL,
                'panels_groups' => array(TEMPL_NAME),
                'panels_icon' => 'dashicons dashicons-welcome-view-site'
            ),

            //The $control_options array, which is passed through to WP_Widget
            array(),

            //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
            false,

            //! The $base_folder path string.
            self::SO_PLUGIN_DIR
        );
        add_filter('siteorigin_widgets_sanitize_field_date', array($this, 'sanitize_date'));

    }

    function get_widget_form()
    {
        //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.
        return array(
            'repeater' => array(
                'type' => 'repeater',
                'label' => __('Slider.', 'engine'),
                'item_name' => __('Dodaj kolejny element', 'siteorigin-widgets'),
                'scroll_count' => 10,
                'item_label' => array(
                    'selector' => "[id*='title1']".' '."[id*='title2']",
                    'update_event' => 'change',
                    'value_method' => 'val'
                ),
                'fields' => array(
                    'img' => array(
                        'type' => 'media',
                        'label' => __('Dodaj obraz', 'engine'),
                        'choose' => __('Wybierz', 'engine'),
                        'update' => __('Ustaw', 'engine'),
                        'library' => 'image',//'image', 'audio', 'video', 'file'
                        'fallback' => true
                    ),
                    'title1' => array(
                        'type' => 'text',
                        'label' => __('Wprowadź tekst', 'engine'),
                        'default' => ''
                    ),
                    'title2' => array(
                        'type' => 'text',
                        'label' => __('Wprowadź tekst', 'engine'),
                        'default' => ''
                    ),
                    'przycisk' => array(
	                    'type' => 'text',
	                    'label' => __('Wprowadź tekst przycisku', 'engine'),
	                    'default' => ''
                    ),
                    'link' => array(
	                    'type' => 'text',
	                    'label' => __('Wprowadź link przycisku', 'engine'),
	                    'default' => ''
                    ),
                )
            ),
            'slider-settings' => array(
                'type' => 'section',
                'label' => __( 'Ustawienia slidera.' , 'engine' ),
                'hide' => true,
                'fields' => array(
                    'autoplay' => array(
                        'type' => 'checkbox',
                        'label' => __( 'Automatyczne uruchomienie', 'engine' ),
                        'default' => true
                    ),
                    'speed' => array(
                        'type' => 'slider',
                        'label' => __( 'Ile czasu ma być wyświetlany slajd (s)', 'engine' ),
                        'default' => 30,
                        'min' => 2,
                        'max' => 60
                    ),
                )
            ),

            /* element stały dla paneli */

            'ustawienia' => array(
                'type' => 'section',
                'label' => __('Ustawienia sekcji.', 'engine'),
                'hide' => true,
                'fields' => array(
                    'border-top' => array(
                        'type' => 'number',
                        'label' => __('Ustaw górną grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),
                    'border-bottom' => array(
                        'type' => 'number',
                        'label' => __('Ustaw dolną grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),
                    'border-left' => array(
                        'type' => 'number',
                        'label' => __('Ustaw lewą grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),
                    'border-right' => array(
                        'type' => 'number',
                        'label' => __('Ustaw prawą grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),

                    'animation' => array(
                        'type' => 'checkbox',
                        'label' => __('Zaznacz jeśli chcesz dodać animację do tego elementu', 'engine'),
                        'default' => false
                    ),
                    'makeanimation' => array(
                        'type' => 'select',
                        'label' => __('wybierz typ animacji pojawienia się elementu', 'engine'),
                        'prompt' => __('Wybierz opcję', 'engine'),
                        'options' => array(
                            'bounceIn' => 'bounceIn',
                            'bounceInDown' => 'bounceInDown',
                            'bounceInLeft' => 'bounceInLeft',
                            'bounceInRight' => 'bounceInRight',
                            'bounceInUp' => 'bounceInUp',
                            'fadeIn' => 'fadeIn',
                            'fadeInDown' => 'fadeInDown',
                            'fadeInDownBig' => 'fadeInDownBig',
                            'fadeInLeft' => 'fadeInLeft',
                            'fadeInLeftBig' => 'fadeInLeftBig',
                            'fadeInRight' => 'fadeInRight',
                            'fadeInRightBig' => 'fadeInRightBig',
                            'fadeInUp' => 'fadeInUp',
                            'fadeInUpBig' => 'fadeInUpBig',
                            'flipInX' => 'flipInX',
                            'flipInY' => 'flipInY',
                            'slideInUp' => 'slideInUp',
                            'slideInDown' => 'slideInDown',
                            'slideInLeft' => 'slideInLeft',
                            'slideInRight' => 'slideInRight',
                            'zoomIn' => 'zoomIn',
                            'zoomInDown' => 'zoomInDown',
                            'zoomInLeft' => 'zoomInLeft',
                            'zoomInRight' => 'zoomInRight',
                            'zoomInUp' => 'zoomInUp',
                        )
                    ),
                )
            ),
            /* end element stały dla paneli */
        );
    }

    function initialize()
    {
        $this->register_frontend_scripts(
            array(
                array(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/script.js', array('jquery')),
                array('slick', self::SO_PLUGIN_URL . 'assets/slick-master/slick.min.js', array('jquery'))
            )
        );
        //wp_enqueue_script('slick', TEMPL_DIR . 'js/slick-1.7.1/slick.min.js', array('jquery'));

        $this->register_frontend_styles(
            array(
                array(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/main.css', array(), self::SO_PLUGIN_VER),
                array('slick', self::SO_PLUGIN_URL . 'assets/slick-master/slick.css', array(), self::SO_PLUGIN_VER),
                array('slick-theme', self::SO_PLUGIN_URL . 'assets/slick-master/slick-theme.css', array(), self::SO_PLUGIN_VER)
            )
        );

    }

    public function widget($args, $instance)
    {
        include('tpl/tpl.php');
    }

    function get_template_name($instance)
    {
        return 'tpl';
    }

    function get_style_name($instance)
    {
        return '';
    }

    function filter_mce_buttons($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_2($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_3($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_4($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    public function quicktags_settings($settings, $editor_id)
    {
        $settings['buttons'] = preg_replace('/,fullscreen/', '', $settings['buttons']);
        $settings['buttons'] = preg_replace('/,dfw/', '', $settings['buttons']);
        return $settings;
    }

    function sanitize_date($date_to_sanitize)
    {
        // Perform custom date sanitization here.
        $sanitized_date = sanitize_text_field($date_to_sanitize);
        return $sanitized_date;
    }


}

/*
wszystkie dostępne animacje

        <optgroup label="Attention Seekers' => '
          'bounce' => 'bounce',
          'flash' => 'flash',
          'pulse' => 'pulse',
          'rubberBand' => 'rubberBand',
          'shake' => 'shake',
          'swing' => 'swing',
          'tada' => 'tada',
          'wobble' => 'wobble',
          'jello' => 'jello',
        </optgroup>

        <optgroup label="Bouncing Entrances' => '
          'bounceIn' => 'bounceIn',
          'bounceInDown' => 'bounceInDown',
          'bounceInLeft' => 'bounceInLeft',
          'bounceInRight' => 'bounceInRight',
          'bounceInUp' => 'bounceInUp',
        </optgroup>

        <optgroup label="Bouncing Exits' => '
          'bounceOut' => 'bounceOut',
          'bounceOutDown' => 'bounceOutDown',
          'bounceOutLeft' => 'bounceOutLeft',
          'bounceOutRight' => 'bounceOutRight',
          'bounceOutUp' => 'bounceOutUp',
        </optgroup>

        <optgroup label="Fading Entrances' => '
          'fadeIn' => 'fadeIn',
          'fadeInDown' => 'fadeInDown',
          'fadeInDownBig' => 'fadeInDownBig',
          'fadeInLeft' => 'fadeInLeft',
          'fadeInLeftBig' => 'fadeInLeftBig',
          'fadeInRight' => 'fadeInRight',
          'fadeInRightBig' => 'fadeInRightBig',
          'fadeInUp' => 'fadeInUp',
          'fadeInUpBig' => 'fadeInUpBig',
        </optgroup>

        <optgroup label="Fading Exits' => '
          'fadeOut' => 'fadeOut',
          'fadeOutDown' => 'fadeOutDown',
          'fadeOutDownBig' => 'fadeOutDownBig',
          'fadeOutLeft' => 'fadeOutLeft',
          'fadeOutLeftBig' => 'fadeOutLeftBig',
          'fadeOutRight' => 'fadeOutRight',
          'fadeOutRightBig' => 'fadeOutRightBig',
          'fadeOutUp' => 'fadeOutUp',
          'fadeOutUpBig' => 'fadeOutUpBig',
        </optgroup>

        <optgroup label="Flippers' => '
          'flip' => 'flip',
          'flipInX' => 'flipInX',
          'flipInY' => 'flipInY',
          'flipOutX' => 'flipOutX',
          'flipOutY' => 'flipOutY',
        </optgroup>

        <optgroup label="Lightspeed' => '
          'lightSpeedIn' => 'lightSpeedIn',
          'lightSpeedOut' => 'lightSpeedOut',
        </optgroup>

        <optgroup label="Rotating Entrances' => '
          'rotateIn' => 'rotateIn',
          'rotateInDownLeft' => 'rotateInDownLeft',
          'rotateInDownRight' => 'rotateInDownRight',
          'rotateInUpLeft' => 'rotateInUpLeft',
          'rotateInUpRight' => 'rotateInUpRight',
        </optgroup>

        <optgroup label="Rotating Exits' => '
          'rotateOut' => 'rotateOut',
          'rotateOutDownLeft' => 'rotateOutDownLeft',
          'rotateOutDownRight' => 'rotateOutDownRight',
          'rotateOutUpLeft' => 'rotateOutUpLeft',
          'rotateOutUpRight' => 'rotateOutUpRight',
        </optgroup>

        <optgroup label="Sliding Entrances' => '
          'slideInUp' => 'slideInUp',
          'slideInDown' => 'slideInDown',
          'slideInLeft' => 'slideInLeft',
          'slideInRight' => 'slideInRight',

        </optgroup>
        <optgroup label="Sliding Exits' => '
          'slideOutUp' => 'slideOutUp',
          'slideOutDown' => 'slideOutDown',
          'slideOutLeft' => 'slideOutLeft',
          'slideOutRight' => 'slideOutRight',

        </optgroup>

        <optgroup label="Zoom Entrances' => '
          'zoomIn' => 'zoomIn',
          'zoomInDown' => 'zoomInDown',
          'zoomInLeft' => 'zoomInLeft',
          'zoomInRight' => 'zoomInRight',
          'zoomInUp' => 'zoomInUp',
        </optgroup>

        <optgroup label="Zoom Exits' => '
          'zoomOut' => 'zoomOut',
          'zoomOutDown' => 'zoomOutDown',
          'zoomOutLeft' => 'zoomOutLeft',
          'zoomOutRight' => 'zoomOutRight',
          'zoomOutUp' => 'zoomOutUp',
        </optgroup>

        <optgroup label="Specials' => '
          'hinge' => 'hinge',
          'rollIn' => 'rollIn',
          'rollOut' => 'rollOut',
        </optgroup>


*/
?>
