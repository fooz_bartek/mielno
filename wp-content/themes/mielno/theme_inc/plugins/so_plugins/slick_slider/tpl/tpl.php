<?php
$nazwa_widgetu="slider-home";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
$widgetstyle = ' style="';
if( $instance['ustawienia']['border-top'] > 0){
    $widgetstyle .= ' border-top:'.$instance['ustawienia']['border-top'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-botom'] > 0){
    $widgetstyle .= ' border-bottom:'.$instance['ustawienia']['border-bottom'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-left'] > 0){
    $widgetstyle .= ' border-left:'.$instance['ustawienia']['border-left'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-right'] > 0){
    $widgetstyle .= ' border-right:'.$instance['ustawienia']['border-right'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
$widgetstyle .= '" ';
$imgatr = array( 'class' => 'img-responsive' );
?>
<div id="<?php echo $widgetid; ?>" class="so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="<?php echo $nazwa_widgetu . ' ' . $instance['panels_info']['style']['class'] . ' ' . $widgetpanelstyle; ?>" id="<?php echo $instance['panels_info']['style']['id']; ?>" <?php
    echo $widgetanimation . $widgetstyle; ?> >
        <?php /** Content your widget **/ ?>

        <div class="slick-slider-top">
            <?php
            if(count($instance['repeater'])) {
                foreach ($instance['repeater'] as $key => $value) {
                    ?>
                    <div class="slick-item animation title-animacja3">

                        <?php
                        $imgurl = wp_get_attachment_image_src($value['img'], 'slider', '');
                        ?>

                        <!--<div class="slick-color-wrapper"></div>-->
                        <div class="slick-gradient-wrapper"></div>

                            <div class="slick-desc-wrapper height_100" style="background-image: url(<?php echo $imgurl[0]; ?>)">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 slick-text-wrapper">
                                                <h2 class="slick-title-1"><?php echo $value['title1']; ?></h2>
                                                <p class="slick-title-2"><?php echo $value['title2']; ?></p>
                                                <?php
                                                    if ($value['przycisk']) { ?>
                                                        <h3><a href="<?php echo $value['link']; ?>"><button type="button" class="btn btn-default"><?php echo $value['przycisk']; ?></button></a></h3>
                                                    <?php } else {
                                                        
                                                    }
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                            </div>


                        <?php // echo wp_get_attachment_image( $value['img'], 'slider', '', $imgatr ); ?>
                    </div>

                    <?php
                }
            }
            ?>
        </div>
        <script type="text/javascript">
            //jQuery(window).load(function(){
            jQuery(document).ready(function() {
                jQuery('#<?php echo $widgetid; ?> .slick-slider-top').on('init', function(){
                    jQuery('#<?php echo $widgetid; ?> .slick-slider-top .slick-item ').removeClass('animation');
                    jQuery('#<?php echo $widgetid; ?> .slick-slider-top').find('[data-slick-index="0"]').addClass('animation');
                    console.log('init');
                });
                jQuery('#<?php echo $widgetid; ?> .slick-slider-top').slick({
                    adaptiveHeight:false,
                    autoplay: <?php if ($instance['slider-settings']['autoplay'] == 1){echo 'true';}else{echo 'false';} ?>,
                    autoplaySpeed: <?php echo $instance['slider-settings']['speed']*1000; ?>,
                    lazyLoad: 'progressive',
                    slidesToShow: 1,
                    infinite: true,
                    <?php if( count($instance['repeater']) > 1 ){?>
                    dots: false,
                    <?php }else{?>
                    dots: false,
                    <?php }?>
                    arrows: false
                });

                jQuery('#<?php echo $widgetid; ?> .slick-slider-top').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                    jQuery('#<?php echo $widgetid; ?> .slick-slider-top').find('[data-slick-index='+currentSlide+']').removeClass('animation');
                    jQuery('#<?php echo $widgetid; ?> .slick-slider-top').find('[data-slick-index='+nextSlide+']').addClass('animation');

                    jQuery('#<?php echo $widgetid; ?> .slick-slider-top').find('[data-slick-index='+currentSlide+']').removeClass('title-animacja3');
                    jQuery('#<?php echo $widgetid; ?> .slick-slider-top').find('[data-slick-index='+nextSlide+']').addClass('title-animacja3');
                });
            });
        </script>

        <?php /** End ontent your widget **/ ?>
    </div>
</div>
