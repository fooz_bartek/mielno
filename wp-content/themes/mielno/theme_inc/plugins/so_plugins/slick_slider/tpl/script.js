// Wyśrodkuj napisy w sliderze
jQuery(document).ready(function() {

    function setHeight() {
        var slickHeight = jQuery(".slick-slide").height();
        var slickTextHeight = jQuery(".slick-text-wrapper").innerHeight();
        var getWrapperSpace = slickHeight - slickTextHeight;
        var getMarginHeight = (getWrapperSpace / 2) - 50;
        if($(window).width()<768) {
            var getMarginHeight = (getWrapperSpace / 2);
        }
        jQuery(".slick-text-wrapper").css("margin-top",getMarginHeight);
    };
    setHeight();

    jQuery(window).resize(function() {
        setHeight();
    });

});