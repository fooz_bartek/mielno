<?php
$nazwa_widgetu="Twoja nazwa";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
$widgetstyle = ' style="';
if( $instance['ustawienia']['border-top'] > 0){
    $widgetstyle .= ' border-top:'.$instance['ustawienia']['border-top'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-botom'] > 0){
    $widgetstyle .= ' border-bottom:'.$instance['ustawienia']['border-bottom'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-left'] > 0){
    $widgetstyle .= ' border-left:'.$instance['ustawienia']['border-left'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-right'] > 0){
    $widgetstyle .= ' border-right:'.$instance['ustawienia']['border-right'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
$widgetstyle .= '" ';
$imgatr = array( 'class' => 'img-responsive' );
?>
<div id="<?php echo $widgetid; ?>" class="so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="<?php echo $nazwa_widgetu . ' ' . $instance['panels_info']['style']['class'] . ' ' . $widgetpanelstyle; ?>" id="<?php echo $instance['panels_info']['style']['id']; ?>" <?php
    echo $widgetanimation . $widgetstyle; ?> >
        <?php /** Content your widget **/ ?>

        <div id="map" style="<?php
        echo ' min-height:'.$instance['height'].'px; ';

        if(strlen($instance['ustawienia']['margin-top']) > 0){
          echo ' margin-top:'.    $instance['ustawienia']['margin-top']    .'px; ';
        }
        if(strlen($instance['ustawienia']['margin-bottom']) > 0){
          echo ' margin-bottom:'. $instance['ustawienia']['margin-bottom'] .'px; ';
        }
        if(strlen($instance['ustawienia']['padding-top']) > 0 && $instance['ustawienia']['padding-top'] >= 0 ){
          echo ' padding-top:'.   $instance['ustawienia']['padding-top']   .'px; ';
        }
        if(strlen($instance['ustawienia']['padding-bottom']) > 0 && $instance['ustawienia']['padding-bottom'] >= 0 ){
          echo ' padding-bottom:'.$instance['ustawienia']['padding-bottom'].'px; ';
        }
        ?>"></div>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
        <script type="text/javascript">
            function initialize() {
                var block_bg = colorToHex('#FFFFFF'),
                    map_data = jQuery("#map-canvas").data(),

                    map_canvas = document.getElementById('map'),
                    pos = new google.maps.LatLng(<?php echo $instance['lokalizacja'] ?>),

                    map_type = 'roadmap' == 'hybrid' ? google.maps.MapTypeId.HYBRID : google.maps.MapTypeId.ROADMAP,

                    map = new google.maps.Map(map_canvas, {
                        center: pos,
                        zoom: <?php echo ($instance['zoom']/10) ?>,
                        // mapTypeId: map_type,
                        scaleControl: false,
                        zoomControl: <?php if ($instance['zoom_control'] == 1){echo 'true';}else{echo 'false';} ?>,
                        panControl: false,
                        overviewMapControl: false,
                        scrollwheel: <?php  if ($instance['scrollwheele_control'] == 1){echo 'true';}else{echo 'false';}  ?>,
                        draggable: <?php  if ($instance['draggable_control'] == 1){echo 'true';}else{echo 'false';}  ?>,
                        navigationControl: <?php  if ($instance['navigation_control'] == 1){echo 'true';}else{echo 'false';}  ?>,
                        mapTypeControl: <?php  if ($instance['maptype_control'] == 1){echo 'true';}else{echo 'false';}  ?>,
                        scaleControl: false,
                        disableDoubleClickZoom: true
                    }),
                    grayscale =[{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-10},{"lightness":30}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":10}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":60}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]}

                    ],
                    mapType = new google.maps.StyledMapType(grayscale, {name: "Grayscale Map"});

                if (!jQuery('.block_bg_color').hasClass('no_map_styling')) {
                    map.mapTypes.set('grayscale', mapType);
                    map.setMapTypeId('grayscale');
                }


                // Marker
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo $instance['lokalizacja'] ?>),
                    map: map,
                    title: '<?php echo $instance['tytul']; ?>',
                    icon: '<?php if ($instance['pin']) {
                        echo wp_get_attachment_url($instance['pin']);
                    }else{
                        echo get_template_directory_uri().'/img/pin.png';
                    }?>',
                    <?php
                    if($instance['pin-url']){
                        echo "url: '".$instance['pin-url']."'";
                    }
                    ?>
                })

                // Click Enable Scroll
                google.maps.event.addListener(map, 'click', function (event) {
                    map.setOptions({scrollwheel: true})
                });

                google.maps.event.addListener(map, 'dragend', function (event) {
                    map.setOptions({scrollwheel: false})
                });

                google.maps.event.addListener(map, 'click', function(event){
                    this.setOptions({draggable:true });
                });
                google.maps.event.addListener(map, 'mouseout', function(event){
                    this.setOptions({scrollwheel:false, draggable:false});
                });
                google.maps.event.addListener(map, 'dblclick', function(event){
                    this.setOptions({scrollwheel:false, draggable:false});
                });
                <?php if($instance['pin-url']){
                ?>
                google.maps.event.addListener(marker, 'click', function() {
                    window.location.href = marker.url;
                });
                <?php
                } ?>


                // Hover Out Disable Scroll
                map_canvas.onmouseout = function (event) {
                    map.setOptions({scrollwheel: false})
                };


                // Resize Event
                jQuery(window).bind('afterresize', function (ev) {
                    map.panTo(<?php echo $instance['lokalizacja'] ?>);
                });
            }

            function colorToHex(color) {
                if (color.substr(0, 1) === '#') {
                    return color;
                }
                var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

                var red = parseInt(digits[2]);
                var green = parseInt(digits[3]);
                var blue = parseInt(digits[4]);

                var rgb = blue | (green << 8) | (red << 16);
                return digits[1] + '#' + rgb.toString(16);
            }


            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>

        <?php /** End ontent your widget **/ ?>
    </div>
</div>