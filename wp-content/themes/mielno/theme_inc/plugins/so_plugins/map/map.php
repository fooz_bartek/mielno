<?php
/*
Widget Name: Mapa widget
Description: Wzorcowy widget.
Author: Wojciech Zdziejowski
*/

siteorigin_widget_register('Mapa Widget', THEME_PLUGIN_DIRECTORY . 'so_plugins/map/', 'Mapa_Widget');
class Mapa_Widget extends SiteOrigin_Widget
{

    function __construct()
    {

        define('SO_PLUGIN_SLUG', 'mapa-widget');
        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/map/');
        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/map/');
        define('SO_PLUGIN_VER', '1');
        //================================
        //Call the parent constructor with the required arguments.
        //DATA FOR WIDGET
        //================================
        parent::__construct(
        // The unique id for your widget.
            SO_PLUGIN_SLUG,

            // The name of the widget for display purposes.
            __('Mapa w treści', 'engine'),

            // The $widget_options array, which is passed through to WP_Widget.
            // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
            array(
                'description' => __('Wyświetl mapę w treści.', 'engine'),
                'help' => TEMPL_OWNER_URL,
                'panels_groups' => array(TEMPL_NAME),
                'panels_icon' => 'dashicons dashicons-location-alt'
            ),

            //The $control_options array, which is passed through to WP_Widget
            array(),

            //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
            false,

            //! The $base_folder path string.
            SO_PLUGIN_DIR
        );
        add_filter('siteorigin_widgets_sanitize_field_date', array($this, 'sanitize_date'));


    }

    function get_widget_form()
    {
        //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.
        return array(
            'tytul' => array(
                'type' => 'text',
                'label' => __('Wprowadź tytuł dla mapy', 'engine'),
                'default' => ''
            ),
            'pin-url' => array(
                'type' => 'text',
                'label' => __('Wprowadź link jeśli pinezka ma być odnośnikiem', 'engine'),
                'default' => ''
            ),
            'lokalizacja' => array(
                'type' => 'text',
                'label' => __('Wprowadź lokalizację mapy, współrzędne GPS(np. 50.2656219,18.9758666)', 'engine'),
                'default' => '50.2656219,18.9758666'
            ),
            'pin' => array(
                'type' => 'media',
                'label' => __('Dodaj graficzny punkt', 'engine'),
                'choose' => __('Wybierz', 'engine'),
                'update' => __('Ustaw', 'engine'),
                'library' => 'image',//'image', 'audio', 'video', 'file'
                'fallback' => true
            ),
            'zoom' => array(
                'type' => 'slider',
                'label' => __('Wybierz przybliżenie', 'engine'),
                'default' => 150,
                'min' => 100,
                'max' => 210
            ),
            'zoom_control' => array(
                'type' => 'checkbox',
                'label' => __('Kontrolka skali (zoomControl) ', 'engine'),
                'default' => false
            ),
            'scrollwheele_control' => array(
                'type' => 'checkbox',
                'label' => __('Reakcja na rolkę myszki (scrollwheel) ', 'engine'),
                'default' => false
            ),
            'draggable_control' => array(
                'type' => 'checkbox',
                'label' => __('Możliwość przesuwania (draggable) ', 'engine'),
                'default' => false
            ),
            'navigation_control' => array(
                'type' => 'checkbox',
                'label' => __('Dodanie nawigacji (navigationControl) ', 'engine'),
                'default' => false
            ),
            'maptype_control' => array(
                'type' => 'checkbox',
                'label' => __('Kontrolka rozdaju mapy (mapTypeControl) ', 'engine'),
                'default' => false
            ),
            'height' => array(
                'type' => 'number',
                'label' => __('Ustaw wysokość mapy (px)', 'engine'),
                'default' => '500'
            ),

            /* element stały dla paneli */
            'ustawienia' => array(
                'type' => 'section',
                'label' => __('Ustawienia sekcji.', 'engine'),
                'hide' => true,
                'fields' => array(
                    'margin-top' => array(
                        'type' => 'number',
                        'label' => __('Ustaw górny margines zewnętrzny (px)', 'engine'),
                        'default' => ''
                    ),
                    'margin-bottom' => array(
                        'type' => 'number',
                        'label' => __('Ustaw dolny margines zewnętrzny (px)', 'engine'),
                        'default' => ''
                    ),
                    'padding-top' => array(
                        'type' => 'number',
                        'label' => __('Ustaw górny margines wewnętrzny (px)', 'engine'),
                        'default' => ''
                    ),
                    'padding-bottom' => array(
                        'type' => 'number',
                        'label' => __('Ustaw dolny margines wewnętrzny (px)', 'engine'),
                        'default' => ''
                    ),
                )
            ),
            /* element stały dla paneli */

        );
        add_filter('siteorigin_widgets_sanitize_field_date', array($this, 'sanitize_date'));
    }
    function initialize()
    {

        $this->register_frontend_scripts(
            array(
                array(SO_PLUGIN_SLUG, SO_PLUGIN_URL . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER, true)
            )
        );

        $this->register_frontend_styles(
            array(
                array(SO_PLUGIN_SLUG, SO_PLUGIN_URL . 'tpl/main.css', array(), SO_PLUGIN_VER)
            )
        );

    }

    public function widget($args, $instance)
    {
        include('tpl/tpl.php');
    }

    function get_template_name($instance)
    {
        return 'tpl';
    }

    function get_style_name($instance)
    {
        return '';
    }

    function filter_mce_buttons($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_2($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_3($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_4($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    public function quicktags_settings($settings, $editor_id)
    {
        $settings['buttons'] = preg_replace('/,fullscreen/', '', $settings['buttons']);
        $settings['buttons'] = preg_replace('/,dfw/', '', $settings['buttons']);
        return $settings;
    }

    function sanitize_date($date_to_sanitize)
    {
        // Perform custom date sanitization here.
        $sanitized_date = sanitize_text_field($date_to_sanitize);
        return $sanitized_date;
    }


}

?>
