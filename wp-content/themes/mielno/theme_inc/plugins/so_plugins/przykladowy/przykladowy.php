<?php

/*
Widget Name: Intra
Description:
Author:
*/

siteorigin_widget_register('Przykładowy Widget', THEME_PLUGIN_DIRECTORY . 'so_plugins/widget_modules_list/', 'ModulesList_Widget');
class ModulesList_Widget extends SiteOrigin_Widget {

    function __construct(){
        define('SO_PLUGIN_SLUG', 'widget_modules_list');
        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/widget_modules_list/');
        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/widget_modules_list/');
        define('SO_PLUGIN_VER', '1');

        //================================
        parent::__construct(
            SO_PLUGIN_SLUG,
            __('Sekcja: Lista modułów', 'engine'),
            array(
                'description' => __('Prezentacja możliwości widgetu.', 'engine'),
                'help' => TEMPL_OWNER_URL,
                'panels_groups' => array(TEMPL_NAME),
                'panels_icon' => 'dashicons dashicons-admin-settings'
            ),
            array(),
            false,
            SO_PLUGIN_DIR
        );
        add_filter('siteorigin_widgets_sanitize_field_date', array($this, 'sanitize_date'));

    }

    function get_widget_form(){
		$display = array();
		$display['header'] = array(
			'type' => 'text',
			'label' => __( 'Nagłówek', 'engine' ),
		);

		$query = siteorigin_widget_post_selector_process_query(array(
			'post_type' => 'modules',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'ignore_sticky_posts' => 0,
			'post_status' => 'publish'
		));
		$query_result = new WP_Query( $query );
		$fields = array();
		if($query_result->have_posts()) :
			while($query_result->have_posts()) : $query_result->the_post();
				$fields[sanitize_title(get_the_title())] = array(
						'type' => 'checkbox',
						'label' => get_the_title(),
						'default' => ''
				);
			endwhile;
		endif;

		$display['html'] = array(
			 'type' => 'html',
			 'label' => '',
			 'rows' => '<br/><h3>Lorem lipsum</h3><hr /><br/>'
		);

		$display['modules'] = array(
			'type' => 'section',
			'label' => __('Moduły do wyboru', 'engine'),
			'hide' => true,
			'fields' => $fields
		);

		return $display;
    }

    function initialize(){
        $this->register_frontend_scripts(
            array(
                array(SO_PLUGIN_SLUG, SO_PLUGIN_URL . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER, true)
            )
        );
    }

    public function widget($args, $instance){
        include('tpl/tpl.php');
    }

    function get_template_name($instance){
        return 'tpl';
    }

    function get_style_name($instance){
        return '';
    }

    function filter_mce_buttons($buttons, $editor_id){
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_2($buttons, $editor_id){
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_3($buttons, $editor_id){
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_4($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    public function quicktags_settings($settings, $editor_id)
    {
        $settings['buttons'] = preg_replace('/,fullscreen/', '', $settings['buttons']);
        $settings['buttons'] = preg_replace('/,dfw/', '', $settings['buttons']);
        return $settings;
    }

    function sanitize_date($date_to_sanitize)
    {
        // Perform custom date sanitization here.
        $sanitized_date = sanitize_text_field($date_to_sanitize);
        return $sanitized_date;
    }
}
?>
