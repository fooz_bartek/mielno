<?php

/*
Widget Name: Mielno - section_link
Description: Widget na top strony
Author: Bartek Szewczyk
*/


class SectionLink_Widget extends SiteOrigin_Widget {

		function __construct() {
			define('SO_PLUGIN_SLUG', 'SectionLink-widget');
	        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_section_link/');
	        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_section_link/');
	        define('SO_PLUGIN_VER', '1');

			$form_options = array(
				'header' => array(
			        'type' => 'tinymce',
			        'label' => __('Nagłówek', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			        ),
			    ),
				'background' => array(
					'type' => 'media',
					'label' => __( 'Tło sekcji', 'engine' ),
					'choose' => __( 'Dodaj grafike', 'engine' ),
					'update' => __( 'Ustaw grafike', 'engine' ),
					'library' => 'image'
				),
				'button_active' => array(
					'type' => 'checkbox',
					'label' => __( 'Przycisk - aktywacja', 'engine' ),
					'default' => false
				),
				'button_text' => array(
					 'type' => 'text',
					 'label' => __( 'Przycisk - tekst', 'engine' ),
				),
				'button_url' => array(
					'type' => 'link',
					'label' => __( 'Przycisk - link do podstrony', 'engine' ),
				),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'SectionLink-widget',
				__('Sekcja graficzna z przyciskiem', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_section_link/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('SectionLink Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_section_link/', 'SectionLink_Widget');
?>
