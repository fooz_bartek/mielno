<?php
//echo $args['before_widget'];
?>
<div class="so-wrapper slick-slider-wrapper" style="<?php
if(strlen($instance['ustawienia']['margin-top']) > 0){
  echo ' margin-top:'.    $instance['ustawienia']['margin-top']    .'px; ';
}
if(strlen($instance['ustawienia']['margin-bottom']) > 0){
  echo ' margin-bottom:'. $instance['ustawienia']['margin-bottom'] .'px; ';
}
if(strlen($instance['ustawienia']['padding-top']) > 0 && $instance['ustawienia']['padding-top'] >= 0 ){
  echo ' padding-top:'.   $instance['ustawienia']['padding-top']   .'px; ';
}
if(strlen($instance['ustawienia']['padding-bottom']) > 0 && $instance['ustawienia']['padding-bottom'] >= 0 ){
  echo ' padding-bottom:'.$instance['ustawienia']['padding-bottom'].'px; ';
}

$imgatr=array(
      'class' => 'img-responsive'
    );

?>">
  <div class="slick-slider">
    <?php
      foreach ($instance['slider'] as $key => $value) {
    ?>
      <div class="slick-item">
        <?php
          echo wp_get_attachment_image( $value['obraz'], 'slider', '', $imgatr );
          $imgurl = wp_get_attachment_image_src($value['obraz'], 'slider', '');
        ?>
        <div class="slick-desc-wrapper" style="background-image: url(<?php echo $imgurl[0]; ?>)">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-8 col-lg-7 slick-title">
                <h2><?php echo $value['tytul']; ?></h2>
				        <h3><?php echo $value['podtytul']; ?></h3>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-10 col-md-6 col-lg-5 slick-desc">
                <?php echo $value['text']; ?>
                <?php if($value['url']){ ?>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php
      }
    ?>
  </div>
  <div class="slick-nav-wrapper">
    <div class="container">
      <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
      <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
    </div>
  </div>
</div>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('.slick-slider').slick({
      adaptiveHeight:false,
      autoplay: <?php if ($instance['slider-settings']['autoplay'] == 1){echo 'true';}else{echo 'false';} ?>,
      autoplaySpeed: <?php echo $instance['slider-settings']['speed']*1000; ?>,
      lazyLoad: 'progressive',
      slidesToShow: 1,
      infinite: true,
      dots: false,
      arrows: false,
      prevArrow: $('.slick-nav-wrapper .prev'),
      nextArrow: $('.slick-nav-wrapper .next')
    });
    jQuery('.slick-nav-wrapper .prev').click(function(){
      jQuery('.slick-slider').slick('slickPrev');
    })

    jQuery('.slick-nav-wrapper .next').click(function(){
      jQuery('.slick-slider').slick('slickNext');
    })
  });
</script>
<?php
//echo $args['after_widget'];
// Add animate to slider animate.css
?>
