<?php

/*
Widget Name: Przykładowy widget
Description: Wzorcowy widget.
Author: Wojciech Zdziejowski
Author URI: http://www.fpweb.pl/
Widget URI: http://www.fpweb.pl/
Video URI: http://www.fpweb.pl/
*/


class Slick_Widget extends SiteOrigin_Widget {

		function __construct() {
		//Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.
			$form_options = array(
				'slider' => array(
					'type' => 'repeater',
					'label' => __( 'Element z możliwością dodania kolejnych.' , 'engine' ),
					'item_name'  => __( 'Dodaj kolejny element', 'engine' ),
					'scroll_count' => 10,
					'item_label' => array(
						'selector'     => "[id*='tytul']",
						'update_event' => 'change',
						'value_method' => 'val'
					),
					'fields' => array(
						'tytul' => array(
							'type' => 'text',
							'label' => __( 'Tytuł slajdu', 'engine' ),
							'default' => ''
						),
						'podtytul' => array(
							'type' => 'text',
							'label' => __( 'Podtytuł slajdu', 'engine' ),
							'default' => ''
						),
						'text' => array(
							'type' => 'tinymce',
							'label' => __( 'Edytor wizualny.', 'engine' ),
							'default' => '',
							'rows' => 5,
							'default_editor' => 'html',
							'button_filters' => array(
								'mce_buttons' => array( $this, 'filter_mce_buttons' ),
								'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
								'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
								'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
								'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
							),
						),
						'obraz' => array(
							'type' => 'media',
							'label' => __( 'Dodaj obraz', 'engine' ),
							'choose' => __( 'Wybierz', 'engine' ),
							'update' => __( 'Ustaw', 'engine' ),
							'library' => 'image',//'image', 'audio', 'video', 'file'
							'fallback' => true
						),
						'url' => array(
							'type' => 'link',
							'label' => __('Dodaj link', 'engine'),
							'default' => 'http://www.example.com'
						)
					)
				),
				'slider-settings' => array(
					'type' => 'section',
					'label' => __( 'Ustawienia slidera.' , 'engine' ),
					'hide' => true,
					'fields' => array(
						'autoplay' => array(
							'type' => 'checkbox',
							'label' => __( 'Automatyczne uruchomienie', 'engine' ),
							'default' => true
						),
						'speed' => array(
							'type' => 'slider',
							'label' => __( 'Ile czasu ma być wyświetlany slajd (s)', 'engine' ),
							'default' => 30,
							'min' => 2,
							'max' => 60
						),
					)
				),

				/* element stały dla paneli */
				'ustawienia' => array(
					'type' => 'section',
					'label' => __( 'Ustawienia sekcji.' , 'engine' ),
					'hide' => true,
					'fields' => array(
						'margin-top' => array(
							'type' => 'number',
							'label' => __( 'Ustaw górny margines zewnętrzny (px)', 'engine' ),
							'default' => ''
						),
						'margin-bottom' => array(
							'type' => 'number',
							'label' => __( 'Ustaw dolny margines zewnętrzny (px)', 'engine' ),
							'default' => ''
						),
						'padding-top' => array(
							'type' => 'number',
							'label' => __( 'Ustaw górny margines wewnętrzny (px)', 'engine' ),
							'default' => ''
						),
						'padding-bottom' => array(
							'type' => 'number',
							'label' => __( 'Ustaw dolny margines wewnętrzny (px)', 'engine' ),
							'default' => ''
						),
					)
				),
				/* element stały dla paneli */

			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

	//================================
	//Call the parent constructor with the required arguments.
	//
	//DATA FOR WIDGET
	//================================
		parent::__construct(
				// The unique id for your widget.
				'slick-widget',

				// The name of the widget for display purposes.
				__('Slider', 'engine'),

				// The $widget_options array, which is passed through to WP_Widget.
				// It has a couple of extras like the optional help URL, which should link to your sites help or support page.
				array(
						'description' => __('Slider - animacja z obrazami na stronę główną', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-slides'
				),

				//The $control_options array, which is passed through to WP_Widget
				array(
				),

				//The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
				$form_options,

				//The $base_folder path string.
				THEME_PLUGIN_DIRECTORY.'so_plugins/przykladowy/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Slick Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/slick/', 'Slick_Widget');
?>
