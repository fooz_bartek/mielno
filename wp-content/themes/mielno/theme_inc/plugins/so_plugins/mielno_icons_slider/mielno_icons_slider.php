<?php

/*
Widget Name: Mielno - Icons
Description: Widget na top strony
Author: Bartek Szewczyk
*/

class IconsSlider_Widget extends SiteOrigin_Widget {

		function __construct() {
            define('SO_PLUGIN_SLUG_IconsSlider', 'icons-slider-widget');
            define('SO_PLUGIN_DIR_IconsSlider', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_icons_slider/');
            define('SO_PLUGIN_URL_IconsSlider', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_icons_slider/');
            define('SO_PLUGIN_VER_IconsSlider', '1');

			$form_options = array(
				'column' => array(
			        'type' => 'slider',
			        'label' => __( 'Liczba kolumn', 'widget-form-fields-text-domain' ),
			        'default' => 3,
			        'min' => 2,
			        'max' => 6,
			        'integer' => true
			    ),

				'header' => array(
			        'type' => 'tinymce',
			        'label' => __('Nagłówek', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
			            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
			            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
			            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
			            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
			        ),
			    ),
				'background' => array(
					'type' => 'media',
					'label' => __( 'Tło sekcji', 'engine' ),
					'choose' => __( 'Dodaj grafike', 'engine' ),
					'update' => __( 'Ustaw grafike', 'engine' ),
					'library' => 'image'
				),
				'icons' => array(
			        'type' => 'repeater',
			        'label' => __( 'Ikony' , 'engine' ),
			        'item_name'  => __( 'Ikona', 'siteorigin-widgets' ),
			        'item_label' => array(
			            'selector'     => "[id*='repeat_text']",
			            'update_event' => 'change',
			            'value_method' => 'val'
			        ),
			        'fields' => array(
			            'icon_header' => array(
			                'type' => 'text',
			                'label' => __( 'Nagłowek', 'engine' )
			            ),
				        'icon_desc' => array(
				        	'type' => 'textarea',
				            'label' => __( 'Opis', 'engine' )
				        ),
						'icon_picture' => array(
							'type' => 'media',
							'label' => __( 'Grafika', 'engine' ),
							'choose' => __( 'Dodaj grafike', 'engine' ),
							'update' => __( 'Ustaw grafike', 'engine' ),
							'library' => 'image'
						),
			        )
			    ),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'icons-slider-widget',
				__('Ikony z sliderem', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_icons_slider/'
				);
		}

        function initialize(){
            $this->register_frontend_scripts(
                array(
                    array(SO_PLUGIN_SLUG_IconsSlider, SO_PLUGIN_URL_IconsSlider . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER_IconsSlider, true)
                )
            );
        }

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('IconsSlider Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_icons_slider/', 'IconsSlider_Widget');
?>
