<?php
$nazwa_widgetu = "icons-slider-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-icons-slider so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" data-column="<?php echo $instance['column']; ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header"><?php echo $instance['header']; ?></div>
				<div class="icons slick">
					<div class="items">
						<?php
						foreach ($instance['icons'] as $key => $value) { ?>
							<div class="icon">
								<div class="picture">
									<?php
									$imageInfo = pathinfo(wp_get_attachment_image_src($value['icon_picture'], 'full', true)[0]);
									$svgconvert = '';
									if($imageInfo['extension'] == 'svg')
										$svgconvert = 'svg-convert';
									 ?>
									<img class="<?php echo $imageInfo['extension']; ?> <?php echo $svgconvert; ?>" src="<?php	echo wp_get_attachment_image_src($value['icon_picture'], 'full', true)[0]; ?>" alt="<?php echo $value['icon_header']; ?>" />
								</div>
								<div class="title"><?php echo $value['icon_header']; ?></div>
								<div class="desc"><?php echo $value['icon_desc']; ?></div>
							</div>
						<?php
						if(($key+1)%6 == 0 && ($key+1) != sizeof($instance['icons']))
							echo '</div><div class="items">';
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="background" style="background-image: url(<?php echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>

	<?php //echo '<pre>',print_r($instance,1),'</pre>'; ?>
</div>
