<?php
$query = siteorigin_widget_post_selector_process_query(array(
	'post_type' => 'areas',
	'posts_per_page' => -1,
	'order' => 'DESC',
	'ignore_sticky_posts' => 0,
	'post_status' => 'publish'
));
$query_result = new WP_Query( $query );
?>

<div class="widget widget-areaslist">

	<!-- Modal message -->
	<div id="modalMessage" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<svg x="0px" y="0px" width="20.707px" height="20.707px" viewBox="6.125 -50.625 20.707 20.707" enable-background="new 6.125 -50.625 20.707 20.707" xml:space="preserve">
							<path class="line1" fill="none" stroke="#8A8CFF" stroke-miterlimit="10" d="M6.479-50.271l20,20"/>
							<path class="line2" fill="none" stroke="#8A8CFF" stroke-miterlimit="10" d="M26.479-50.271l-20,20"/>
						</svg>
			        </button>
					<div class="header">
						<h3>Wyślij wiadomość</h3>
						<p>Działka nr.</p>
					</div>
					<?php echo do_shortcode('[contact-form-7 id="492" title="Lista działek: Formularz"]'); ?>
				</div>
			</div>
		</div>
	</div>


	<div id="areamap">
		<div class="popup" data-status="">
			<div class="in">
				<div class="row table-row">
					<div class="left col-xs-12 col-sm-5 col-md-5">
						<div class="line"></div>
						<div class="pastePhoto"></div>
					</div>
					<div class="right col-xs-12 col-sm -7 col-md-7">
						<div class="pasteHeader"></div>
						<div class="pasteData"></div>
						<div class="pasteBts"></div>
					</div>
					<div class="pasteExtra"></div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-1 col-md-1 hidden-xs"></div>
				<div class="box col-xs-12 col-sm-10 col-md-10">
					<div class="header"><?php echo $instance['header']; ?></div>

					<div class="text1">
						<div class="line"></div>
						<?php echo $instance['text']; ?>
					</div>
					<?php
					if($instance['button_active'] == 'active'){
						$href = '#';
						if(strlen($instance['button_url']) > 0)
							$href = $instance['button_url'];
								 ?>

								 <div class="text2 scrollToAreamap">
			 						<span><?php echo $instance['button_text']; ?></span>
			 						<svg x="0px" y="0px" width="11.271px" height="25px" viewBox="0 0 11.271 25" enable-background="new 0 0 11.271 25" xml:space="preserve">
			 							<path fill="none" stroke-miterlimit="10" d="M5.688,24.716v-25"/>
			 							<path fill="none" stroke-miterlimit="10" d="M11.271,19.054l-5.688,5.662l-5.583-6"/>
			 						</svg>
			 					</div>

					<?php } ?>
				</div>
				<div class="col-sm-1 col-md-1 hidden-xs"></div>
			</div>
		</div>
		<div class="map">
			<?php
			$fillColor = array(
				'free' => '079207',
				'reservation' => 'ffa302',
				'sold' => 'd32407'
			);

			$activeArr = array();
			$fillColorArr = array();
			$fillArr = array();
			if($query_result->have_posts()) :
				while($query_result->have_posts()) : $query_result->the_post();
					$variable = get_post_meta(get_the_ID());

					if(strlen($variable['number'][0]))
						$fillColorArr[$variable['number'][0]] = $fillColor[$variable['status'][0]];

					if($variable['active'][0] == 1)
						$activeArr[$variable['number'][0]] = 'true';
					else
						$activeArr[$variable['number'][0]] = 'false';




					$fillArr[$variable['number'][0]] = ($fillColorArr[$variable['number'][0]] && $activeArr[$variable['number'][0]] == 'true' ? '#'.$fillColorArr[$variable['number'][0]] : 'none');
					if($variable['active'][0] != 1)
						$fillArr[$variable['number'][0]] = 'red';
				endwhile;
			endif;
			?>
			<div class="areamap_static">
				<svg x="0px" y="0px" width="1920px" height="452px" viewBox="0 0 1920 452" enable-background="new 0 0 1920 452" xml:space="preserve">
					<?php
					// $fill = ($fillColorArr[x] && $activeArr[x] == 'true' ? '#'.$fillColorArr[x] : 'none');
					// $fill = ($activeArr[x] == 'false' ? 'red' : $fill);
					?>
					<polygon data-id="1" class="<?php echo ($fillColorArr[1] == 'd32407' || $activeArr[1] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[1]; ?>" points="1293.25,76.25 1306.5,64.5 1355,67.5 1351.833,122.167 1290.25,118.25"/>
					<polygon data-id="2" class="<?php echo ($fillColorArr[2] == 'd32407' || $activeArr[2] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[2]; ?>" points="1290.25,118.25 1351.833,122.167 1366.167,174.667 1287.833,168.5"/>
					<polygon data-id="3" class="<?php echo ($fillColorArr[3] == 'd32407' || $activeArr[3] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[3]; ?>" points="1351.833,122.167 1366.167,174.667 1436.667,155.167 1417,82.167 1386.167,70 1355,67.5"/>
					<polygon data-id="4" class="<?php echo ($fillColorArr[4] == 'd32407' || $activeArr[4] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[4]; ?>" points="1388.833,257 1459.333,237.5 1436.667,155.167 1366.167,174.667"/>
					<polygon data-id="5" class="<?php echo ($fillColorArr[5] == 'd32407' || $activeArr[5] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[5]; ?>" points="1287.833,168.5 1286.167,181.667 1304.833,282.167 1388.833,257 1366.167,174.667"/>
					<polygon data-id="6" class="<?php echo ($fillColorArr[6] == 'd32407' || $activeArr[6] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[6]; ?>" points="1218.667,194.667 1286.167,181.667 1304.833,282.167 1322.5,345.5 1250.5,359.333"/>
					<polygon data-id="7" class="<?php echo ($fillColorArr[7] == 'd32407' || $activeArr[7] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[7]; ?>" points="1153,208 1218.667,194.667 1250.5,359.333 1183.5,372"/>
					<polygon data-id="8" class="<?php echo ($fillColorArr[8] == 'd32407' || $activeArr[8] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[8]; ?>" points="1086.25,220 1153,208 1183.5,372 1117.75,384.75"/>
					<polygon data-id="9" class="<?php echo ($fillColorArr[9] == 'd32407' || $activeArr[9] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[9]; ?>" points="1020,233 1086.25,220 1117.75,384.75 1051,398.25"/>
					<polygon data-id="10" class="<?php echo ($fillColorArr[10] == 'd32407' || $activeArr[10] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[10]; ?>" points="953.25,246.25 1020,233 1051,398.25 985,410.75"/>
					<polygon data-id="11" class="<?php echo ($fillColorArr[11] == 'd32407' || $activeArr[11] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[11]; ?>" points="876.5,268.75 923.5,252 953.25,246.25 985,410.75 963.75,415 932.25,426"/>
					<polygon data-id="12" class="<?php echo ($fillColorArr[12] == 'd32407' || $activeArr[12] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[12]; ?>" points="813.5,293 876.5,268.75 932.25,426 869,449.25"/>
					<polygon data-id="13" class="<?php echo ($fillColorArr[13] == 'd32407' || $activeArr[13] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[13]; ?>" points="794.75,192 862,179.75 875,240.75 821.5,261.25 805.25,252.5"/>
					<polygon data-id="14" class="<?php echo ($fillColorArr[14] == 'd32407' || $activeArr[14] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[14]; ?>" points="796.25,97.25 848.25,101.5 862,179.75 794.75,192 791,175.25"/>
					<polygon data-id="15" class="<?php echo ($fillColorArr[15] == 'd32407' || $activeArr[15] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[15]; ?>" points="905.5,105.5 918,169.5 862,179.75 848.25,101.5 892.75,104.25"/>
					<polygon data-id="16" class="<?php echo ($fillColorArr[16] == 'd32407' || $activeArr[16] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[16]; ?>" points="918,169.5 928.5,224 915.5,226.25 875,240.75 862,179.75"/>
					<polygon data-id="17" class="<?php echo ($fillColorArr[17] == 'd32407' || $activeArr[17] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[17]; ?>" points="918,169.5 973,159 983.5,212.75 928.5,224"/>
					<polygon data-id="18" class="<?php echo ($fillColorArr[18] == 'd32407' || $activeArr[18] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[18]; ?>" points="905.5,105.5 919,106.167 964,110 973,159 918,169.5"/>
					<polygon data-id="19" class="<?php echo ($fillColorArr[19] == 'd32407' || $activeArr[19] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[19]; ?>" points="964,110 1016.25,114 1033.75,203.25 983.5,212.75 973,159"/>
					<polygon data-id="20" class="<?php echo ($fillColorArr[20] == 'd32407' || $activeArr[20] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[20]; ?>" points="1068.875,118.25 1084.5,194.667 1033.75,203.25 1016.25,114"/>
					<polygon data-id="21" class="<?php echo ($fillColorArr[21] == 'd32407' || $activeArr[21] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[21]; ?>" points="1123,122 1134.875,184 1084.5,194.667 1068.875,118.25"/>
					<polygon data-id="22" class="<?php echo ($fillColorArr[22] == 'd32407' || $activeArr[22] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[22]; ?>" points="1176.25,118.25 1187.5,174.667 1134.875,184 1123,122"/>
					<polygon data-id="23" class="<?php echo ($fillColorArr[23] == 'd32407' || $activeArr[23] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[23]; ?>" points="1264.75,110 1262.5,149 1252.75,161.375 1187.5,174.667 1176.25,118.25"/>
					<polygon data-id="24" class="<?php echo ($fillColorArr[24] == 'd32407' || $activeArr[24] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[24]; ?>" points="1180.333,54.5 1176.25,118.25 1264.75,110 1267.166,74.167 1255.333,60.5"/>
					<polygon data-id="25" class="<?php echo ($fillColorArr[25] == 'd32407' || $activeArr[25] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[25]; ?>" points="1127.333,50.667 1180.333,54.5 1176.25,118.25 1123,122"/>
					<polygon data-id="26" class="<?php echo ($fillColorArr[26] == 'd32407' || $activeArr[26] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[26]; ?>" points="1073.5,46.167 1127.333,50.667 1123,122 1068.875,118.25"/>
					<polygon data-id="27" class="<?php echo ($fillColorArr[27] == 'd32407' || $activeArr[27] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[27]; ?>" points="1020.5,42.333 1073.5,46.167 1068.875,118.25 1016.25,114"/>
					<polygon data-id="28" class="<?php echo ($fillColorArr[28] == 'd32407' || $activeArr[28] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[28]; ?>" points="967.5,38.833 1020.5,42.333 1016.25,114 964,110"/>
					<polygon data-id="29" class="<?php echo ($fillColorArr[29] == 'd32407' || $activeArr[29] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[29]; ?>" points="925.666,35.5 967.5,38.833 964,110 919,106.167 918.667,98.834 913.167,93.167 915.833,45 	"/>
					<polygon data-id="30" class="<?php echo ($fillColorArr[30] == 'd32407' || $activeArr[30] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[30]; ?>" points="852,30 893.75,32.75 902.5,43.75 900,91.25 893,97.25 892.75,104.25 848.25,101.5"/>
					<polygon data-id="31" class="<?php echo ($fillColorArr[31] == 'd32407' || $activeArr[31] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[31]; ?>" points="800.5,26 852,30 848.25,101.5 796.25,97.25"/>

					<!-- <polygon data-id="32" class="<?php echo ($fillColorArr[32] == 'd32407' || $activeArr[32] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[32]; ?>" points="1271.334,2.333 1303.334,2.333 1301.834,37.333 1253.834,33.5 1253.834,17.5 1271.001,18.333"/>
					<polygon data-id="33" class="<?php echo ($fillColorArr[33] == 'd32407' || $activeArr[33] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[33]; ?>" points="1351.833,3.167 1350.668,41.167 1301.834,37.333 1303.334,2.333"/>
					<polygon data-id="34" class="<?php echo ($fillColorArr[34] == 'd32407' || $activeArr[34] == 'false' ? 'off' : ''); ?>" fill="<?php echo $fillArr[34]; ?>" points="1404.668,4.5 1403.334,32.333 1388.001,43.167 1388.668,31 1380.501,30.833 1379.668,43.167 1350.668,41.167 1351.833,3.167"/> -->

					<text transform="matrix(1 0 0 1 1382.7383 120.9624)" fill="#ffffff" font-size="16">03</text>
					<text transform="matrix(1 0 0 1 1312.9043 95.1294)" fill="#ffffff" font-size="16">01</text>
					<text transform="matrix(1 0 0 1 1312.5713 144.7954)" fill="#ffffff" font-size="16">02</text>
					<text transform="matrix(1 0 0 1 1401.4043 209.1294)" fill="#ffffff" font-size="16">04</text>
					<text transform="matrix(1 0 0 1 1324.4043 227.1289)" fill="#ffffff" font-size="16">05</text>
					<text transform="matrix(1 0 0 1 1260.0703 277.2959)" fill="#ffffff" font-size="16">06</text>
					<text transform="matrix(1 0 0 1 1191.9033 290.7959)" fill="#ffffff" font-size="16">07</text>
					<text transform="matrix(1 0 0 1 1125.4033 307.1289)" fill="#ffffff" font-size="16">08</text>
					<text transform="matrix(1 0 0 1 1057.5693 321.2959)" fill="#ffffff" font-size="16">09</text>
					<text transform="matrix(1 0 0 1 994.7363 337.9629)" fill="#ffffff" font-size="16">10</text>
					<text transform="matrix(1 0 0 1 925.7358 356.9629)" fill="#ffffff" font-size="16">11</text>
					<text transform="matrix(1 0 0 1 861.4023 372.1299)" fill="#ffffff" font-size="16">12</text>
					<text transform="matrix(1 0 0 1 826.4023 228.1299)" fill="#ffffff" font-size="16">13</text>
					<text transform="matrix(1 0 0 1 888.0693 210.9634)" fill="#ffffff" font-size="16">16</text>
					<text transform="matrix(1 0 0 1 932.9028 138.1294)" fill="#ffffff" font-size="16">18</text>
					<text transform="matrix(1 0 0 1 874.2358 142.1294)" fill="#ffffff" font-size="16">15</text>
					<text transform="matrix(1 0 0 1 814.2358 150.2964)" fill="#ffffff" font-size="16">14</text>
					<text transform="matrix(1 0 0 1 995.0703 170.2964)" fill="#ffffff" font-size="16">19</text>
					<text transform="matrix(1 0 0 1 942.9033 199.4634)" fill="#ffffff" font-size="16">17</text>
					<text transform="matrix(1 0 0 1 1047.9023 161.7964)" fill="#ffffff" font-size="16">20</text>
					<text transform="matrix(1 0 0 1 1098.7363 156.6294)" fill="#ffffff" font-size="16">21</text>
					<text transform="matrix(1 0 0 1 1151.9023 152.7974)" fill="#ffffff" font-size="16">22</text>
					<text transform="matrix(1 0 0 1 1217.7363 147.7974)" fill="#ffffff" font-size="16">23</text>
					<text transform="matrix(1 0 0 1 1217.9023 90.2974)" fill="#ffffff" font-size="16">24</text>
					<text transform="matrix(1 0 0 1 1096.9023 84.9634)" fill="#ffffff" font-size="16">26</text>
					<text transform="matrix(1 0 0 1 1148.9023 87.4634)" fill="#ffffff" font-size="16">25</text>
					<text transform="matrix(1 0 0 1 1042.5703 79.2974)" fill="#ffffff" font-size="16">27</text>
					<text transform="matrix(1 0 0 1 985.7363 76.6304)" fill="#ffffff" font-size="16">28</text>
					<text transform="matrix(1 0 0 1 932.0698 71.9634)" fill="#ffffff" font-size="16">29</text>
					<text transform="matrix(1 0 0 1 868.4033 68.7974)" fill="#ffffff" font-size="16">30</text>
					<text transform="matrix(1 0 0 1 816.9033 65.7974)" fill="#ffffff" font-size="16">31</text>
					<!-- <text transform="matrix(1 0 0 1 1278.7686 25.2974)" fill="#ffffff" font-size="16">32</text>
					<text transform="matrix(1 0 0 1 1319.7686 25.2974)" fill="#ffffff" font-size="16">33</text>
					<text transform="matrix(1 0 0 1 1367.1016 25.2974)" fill="#ffffff" font-size="16">34</text> -->
				</svg>
			</div>
		</div>
	</div>

	<div class="container" id="arealist" data-page="0">
		<div class="row">
			<div class="col-xs-4 col-sm-7 col-md-9">
				<div class="header">Lista działek</div>
			</div>
			<div class="col-xs-8 col-sm-5 col-md-3">
				<div class="pages top">
					<span class="info_left">Strona</span>
					<a class="prev off">
						<svg class="icon" x="0px" y="0px" width="26.253px" height="11.949px" viewBox="0 0 26.253 11.949" enable-background="new 0 0 26.253 11.949" xml:space="preserve">
							<path class="start" fill="none" stroke-miterlimit="10" d="M6.917,0.366L1.254,6.054l6,5.583"/>
							<path class="end" fill="none" stroke-miterlimit="10" d="M1.254,5.949h25"/>
						</svg>
					</a>
					<a class="next">
						<svg class="icon" x="0px" y="0px" width="26.253px" height="11.949px" viewBox="0 0 26.253 11.949" enable-background="new 0 0 26.253 11.949" xml:space="preserve">
							<path class="end" fill="none" stroke-miterlimit="10" d="M25,5.949H0"/>
							<path class="start" fill="none" stroke-miterlimit="10" d="M19.337,0.366L25,6.054l-6,5.583"/>
						</svg>
					</a>
					<div class="info"><b><span class="act">1</span></b> z <span class="max">5</span></div>
				</div>
			</div>
		</div>

		<div class="row disclaimer">
			<div class="col-md-12">
				<?php echo $instance['disclaimer']; ?>
			</div>
		</div>

		<div class="row sorts">
			<div class="col-md-12">
				<span>Sortuj po:</span>
				<a class="active down" data-sort="id">
					Numer działki
					<svg x="0px" y="0px" width="9.875px" height="16.584px" viewBox="0 0 9.875 16.584" enable-background="new 0 0 9.875 16.584" xml:space="preserve">
						<polyline class="up" fill="none" stroke-miterlimit="10" points="0.562,5.625 4.969,1.035 9.375,5.625 "/>
						<polyline class="down" fill="none" stroke-miterlimit="10" points="9.375,11.035 4.969,15.625 0.562,11.035 "/>
					</svg>
				</a>
				<a class="up" data-sort="price_net">Cena
					<svg x="0px" y="0px" width="9.875px" height="16.584px" viewBox="0 0 9.875 16.584" enable-background="new 0 0 9.875 16.584" xml:space="preserve">
						<polyline class="up" fill="none" stroke-miterlimit="10" points="0.562,5.625 4.969,1.035 9.375,5.625 "/>
						<polyline class="down" fill="none" stroke-miterlimit="10" points="9.375,11.035 4.969,15.625 0.562,11.035 "/>
					</svg>
				</a>
				<a class="up" data-sort="price_net_area">Cena za m<sup>2</sup>
					<svg x="0px" y="0px" width="9.875px" height="16.584px" viewBox="0 0 9.875 16.584" enable-background="new 0 0 9.875 16.584" xml:space="preserve">
						<polyline class="up" fill="none" stroke-miterlimit="10" points="0.562,5.625 4.969,1.035 9.375,5.625 "/>
						<polyline class="down" fill="none" stroke-miterlimit="10" points="9.375,11.035 4.969,15.625 0.562,11.035 "/>
					</svg>
				</a>
				<a class="up" data-sort="area">
					Powierzchnia
					<svg x="0px" y="0px" width="9.875px" height="16.584px" viewBox="0 0 9.875 16.584" enable-background="new 0 0 9.875 16.584" xml:space="preserve">
						<polyline class="up" fill="none" stroke-miterlimit="10" points="0.562,5.625 4.969,1.035 9.375,5.625 "/>
						<polyline class="down" fill="none" stroke-miterlimit="10" points="9.375,11.035 4.969,15.625 0.562,11.035 "/>
					</svg>
				</a>
			</div>
		</div>
		<div class="plots">
			<?php
			if($query_result->have_posts()) :
				while($query_result->have_posts()) : $query_result->the_post();
					$variable = get_post_meta(get_the_ID());

						$price_net = $variable['price_net'][0];
						if(strlen($variable['price_net_new'][0]) > 0)
							$price_net = $variable['price_net_new'][0];

						$price_net_area = $variable['price_net_area'][0];
						if(strlen($variable['price_net_area_new'][0]) > 0)
							$price_net_area = $variable['price_net_area_new'][0];
				?>

				<div class="plot row no-gutters" data-active="<?php echo $variable['active'][0]; ?>" data-status="<?php echo $variable['status'][0]; ?>" data-id="<?php echo $variable['number'][0]; ?>" data-area="<?php echo $variable['areafield'][0]; ?>" data-price_net="<?php echo $price_net; ?>" data-price_net_area="<?php echo $price_net_area; ?>">
					<div class="left col-xs-12 col-md-6 col-md-6">
						<div class="row">
							<div class="cl1 col-xs-4 col-sm-4 col-md-4">
								<div class="title1">Działka</div>
								<div class="title2">
									<?php
									if(strlen($variable['number'][0]) == 1)
										echo '0'.$variable['number'][0];
									else
										echo $variable['number'][0];
									 ?>
								</div>
								<div class="status">
									<span class="point"></span>
									<?php
									switch ($variable['status'][0]) {
									    case 'reservation':
									        echo 'Zarezerwowana';
									        break;
									    case 'sold':
									        echo 'Sprzedana';
									        break;
									    case 'free':
									        echo 'Wolna';
									        break;
										default:
											echo '';
											break;
									}
									?>
								</div>
							</div>
							<div class="cl2 col-xs-4 col-sm-4 col-md-4">
								<?php
									if(strlen($variable['photo1'][0])){
										echo '<img class="photo" src="'.wp_get_attachment_image_src($variable['photo1'][0], 'full', true)[0].'" style="max-width: '.wp_get_attachment_image_src($variable['photo1'][0], 'full', true)[1].'px" alt="" />';
									}
								?>
							</div>
							<div class="cl3 col-xs-4 col-sm-4 col-md-4">
								<?php
									if(strlen($variable['photo2'][0])){
										echo '<img class="photo" src="'.wp_get_attachment_image_src($variable['photo2'][0], 'full', true)[0].'" style="max-width: '.wp_get_attachment_image_src($variable['photo2'][0], 'full', true)[1].'px" alt="" />';
									}
								?>
							</div>
						</div>
					</div>
					<div class="right col-xs-12 col-md-6 col-md-6">
						<div class="row">
							<div class="data col-xs-12 col-sm-8 col-md-8">
								<div>
									<div>Nr ewidencyjny:</div>
									<div><?php echo $variable['number_registration'][0]; ?></div>
								</div>
								<div>
									<div>Typ działki:</div>
									<div><?php echo $variable['type'][0]; ?></div>
								</div>
								<div>
									<div>Powierzchnia:</div>
									<div><?php echo $variable['areafield'][0]; ?> m<sup>2</sup></div>
								</div>

								<?php
								if($variable['active'][0] == 1){
									if($variable['status'][0] != 'sold'){
									?>
									<div>
										<div>Cena netto / m<sub>2</sub></div>
										<div>
											<?php
											if(strlen($variable['price_net_area_new'][0]) > 0){
												echo '<span class="new">'.$variable['price_net_area_new'][0].' zł</span>';
												echo '<span class="old">'.$variable['price_net_area'][0].' zł</span>';
											}else{
												echo '<span class="new">'.$variable['price_net_area'][0].' zł</span>';
											}
											?>
										</div>
									</div>
									<div>
										<div>Cena netto:</div>
										<div>
											<?php
											if(strlen($variable['price_net_new'][0]) > 0){
												echo '<span class="new">'.$variable['price_net_new'][0].' zł</span>';
												echo '<span class="old">'.$variable['price_net'][0].' zł</span>';
											}else{
												echo '<span class="new">'.$variable['price_net'][0].' zł</span>';
											}
											?>
										</div>
									</div>
									<?php }else{ ?>

										<div>
											<div>&nbsp;</div>
											<div>&nbsp;</div>
										</div>
										<div>
											<div>&nbsp;</div>
											<div>&nbsp;</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
							<div class="bts col-xs-12 col-sm-4 col-md-4">
								<?php if($variable['active'][0] == 1 || $variable['status'][0] == 'sold'){ ?>
								<?php
									if(strlen($variable['pdf'][0])){
										$icon = '<svg x="0px" y="0px" width="12.103px" height="14.793px" viewBox="-228.644 35.66 12.103 14.793" enable-background="new -228.644 35.66 12.103 14.793"
											 xml:space="preserve">
											<path d="M-216.639,39.458l-3.698-3.698c-0.063-0.063-0.148-0.099-0.238-0.099h-7.732c-0.186,0-0.336,0.151-0.336,0.336v14.121
												c0,0.186,0.15,0.336,0.336,0.336h11.431c0.186,0,0.336-0.151,0.336-0.336V39.695C-216.541,39.605-216.577,39.52-216.639,39.458z
												 M-220.239,36.809l2.55,2.55h-2.55V36.809z M-227.972,49.781V36.333h7.06v3.362c0,0.186,0.151,0.336,0.336,0.336h3.362v9.75
												H-227.972z"/>
											<path d="M-225.173,48.224c-0.19,0-0.367-0.075-0.515-0.223c-0.383-0.383-0.42-0.854-0.1-1.262c0.326-0.415,1.077-0.785,1.964-1.055
												c0.1-0.219,0.197-0.449,0.289-0.687c0.105-0.273,0.199-0.543,0.28-0.807c-0.644-1.072-1.047-2.181-0.89-2.818
												c0.114-0.46,0.479-0.704,1.001-0.668c0.002,0,0.004,0.001,0.006,0.001c0.263,0.023,0.728,0.171,0.879,0.907
												c0.125,0.603,0.002,1.517-0.264,2.474c0.266,0.415,0.566,0.821,0.877,1.177c0.94-0.082,1.788-0.016,2.238,0.258
												c0.386,0.234,0.503,0.622,0.314,1.037c-0.008,0.017-0.017,0.032-0.027,0.047c-0.33,0.496-0.875,0.623-1.496,0.349
												c-0.412-0.181-0.859-0.535-1.292-0.984c-0.473,0.057-0.968,0.147-1.431,0.267c-0.441,0.898-0.953,1.623-1.419,1.874
												C-224.897,48.187-225.038,48.224-225.173,48.224z M-224.261,46.531c-0.455,0.18-0.816,0.392-0.998,0.624
												c-0.097,0.123-0.114,0.21,0.046,0.371c0.026,0.026,0.043,0.043,0.137-0.007C-224.847,47.396-224.556,47.035-224.261,46.531z
												 M-221.002,45.905c0.225,0.191,0.447,0.341,0.657,0.434c0.426,0.188,0.578,0.022,0.65-0.083c0.038-0.09,0.02-0.111-0.062-0.161
												C-219.989,45.953-220.445,45.896-221.002,45.905z M-222.782,44.911c-0.066,0.188-0.138,0.375-0.212,0.559
												c0.184-0.04,0.37-0.075,0.556-0.106C-222.557,45.217-222.671,45.065-222.782,44.911z M-223.248,41.373
												c-0.205,0-0.227,0.088-0.244,0.16c-0.085,0.347,0.12,0.995,0.484,1.708c0.127-0.618,0.164-1.145,0.092-1.494
												c-0.072-0.348-0.213-0.367-0.277-0.373C-223.212,41.374-223.231,41.373-223.248,41.373z"/>
										</svg>';
										echo '<a class="element-bt2" target="_blank" href="'.wp_get_attachment_url($variable['pdf'][0]).'"><span class="icon">'.$icon.'</span><span class="text">Pobierz kartę</span></a>';
									}
								?>
								<a class="element-bt send_message"><span class="text">Wiadomość</span></a>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php if($variable['description'][0]){ ?>
						<div class="extra">
							<svg x="0px" y="0px" width="159px" height="94.449px" viewBox="0 0 159 94.449" enable-background="new 0 0 159 94.449" xml:space="preserve">
								<polygon fill="#253862" points="4,0 64,-0.5 159,54.541 159,89.069 "/>
								<polygon fill="#0e1525" points="149,94.449 159,89.069 149,83.323 "/>
								<polygon fill="#0e1525" points="4,0 0.152,5.5 13.499,5.458 "/>
							</svg>
							<div class="text"><?php echo $variable['description'][0]; ?></div>
						</div>
					<?php } ?>
				</div>

				<?php endwhile;	?>
			<?php endif; ?>
		</div>


		<div class="row">
			<div class="col-md-12">
				<div class="pages bottom">
					<a class="prev off">
						<svg class="icon" x="0px" y="0px" width="26.253px" height="11.949px" viewBox="0 0 26.253 11.949" enable-background="new 0 0 26.253 11.949" xml:space="preserve">
							<path class="start" fill="none" stroke-miterlimit="10" d="M6.917,0.366L1.254,6.054l6,5.583"/>
							<path class="end" fill="none" stroke-miterlimit="10" d="M1.254,5.949h25"/>
						</svg>
						<span class="text">Poprzednie</span>
					</a>
					<a class="next">
						<span class="text">Następne</span>
						<svg class="icon" x="0px" y="0px" width="26.253px" height="11.949px" viewBox="0 0 26.253 11.949" enable-background="new 0 0 26.253 11.949" xml:space="preserve">
							<path class="end" fill="none" stroke-miterlimit="10" d="M25,5.949H0"/>
							<path class="start" fill="none" stroke-miterlimit="10" d="M19.337,0.366L25,6.054l-6,5.583"/>
						</svg>
					</a>
					<div class="info">Strona <b><span class="act">1</span></b> z <span class="max">5</span></div>
				</div>
			</div>
		</div>


		<div class="row disclaimer">
			<div class="col-md-12">
				<?php echo $instance['disclaimer']; ?>
			</div>
		</div>

	</div>



</div>
