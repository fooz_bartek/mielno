clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

var areas_on_pages = 25;
//var areas_on_pages = 10;

$(document).on('mouseenter touchstart', '.widget-areaslist #areamap .areamap_static svg polygon', function(event) {
    $(this).addClass('hover');
})
$(document).on('mouseleave touchend', '.widget-areaslist #areamap .areamap_static svg polygon', function(event) {
    $(this).removeClass('hover');
})



$(document).on('mouseenter', '.widget-areaslist #areamap .areamap_static svg polygon', function(event) {
    id = $(this).data('id');

    if (!$(this).hasClass('off')) {
        status = $('.widget-areaslist #arealist .plot[data-id="' + id + '"]').data('status')
        $('.widget-areaslist #areamap .popup').attr('data-status', status);

        photo = $('.widget-areaslist #arealist .plot[data-id="' + id + '"] .left .cl2 .photo').clone();
        $('.widget-areaslist #areamap .popup .pastePhoto').html(photo);

        header = $('.widget-areaslist #arealist .plot[data-id="' + id + '"] .left .cl1').clone()[0].innerHTML;
        $('.widget-areaslist #areamap .popup .pasteHeader').html(header);
        data = $('.widget-areaslist #arealist .plot[data-id="' + id + '"] .right .data').clone()[0].innerHTML;
        $('.widget-areaslist #areamap .popup .pasteData').html(data);
        // bts = $('.widget-areaslist #arealist .plot[data-id="' + id + '"] .right .bts').clone()[0].innerHTML;
        // $('.widget-areaslist #areamap .popup .pasteBts').html(bts);
        if ($('.widget-areaslist #arealist .plot[data-id="' + id + '"] .extra').length > 0)
            extra = $('.widget-areaslist #arealist .plot[data-id="' + id + '"] .extra').clone()[0].innerHTML;
        else
            extra = '';
        $('.widget-areaslist #areamap .popup .pasteExtra').html(extra);

        posY = $(this).offset().top - $('.widget-areaslist #areamap .popup').height() - 200;
        posX = $(this).offset().left - $('.widget-areaslist #areamap .popup').width() / 2
        posXMax = $(this).offset().left - $('.widget-areaslist #areamap .popup').width() / 2 + $('.widget-areaslist #areamap .popup').width();



        if (posXMax > $(window).width()) {
            posX = posX - (posXMax - $(window).width()) - 50;
        }

        $('.widget-areaslist #areamap .popup').css({
            'top': posY + 'px',
            'left': posX + 'px'
        });

        $('.widget-areaslist #areamap .popup').addClass('open');
    }
})

$(document).on('mouseleave', '.widget-areaslist #areamap .areamap_static svg polygon', function(event) {
    $('.widget-areaslist #areamap .popup').removeClass('open');
})



$(document).on(clickHandler, '.widget-areaslist #areamap .scrollToAreamap', function(event) {
    $('html, body').stop().animate({
        scrollTop: $('.widget-areaslist .areamap_static').offset().top - 200
    }, 350);
})




//
// START
//
function areasSetStart() {
    areasSort('id', 'down');
    areasDisplay();
};
areasSetStart();

// Sort list

function areasSort(sortType, sortDir) {
    $('.widget-areaslist #arealist').attr('data-page', 0);

    items = $('.plots .plot');
    if (sortDir == 'up')
        items.sort(function(a, b) {
            return +$(b).data(sortType) - +$(a).data(sortType);
        });
    if (sortDir == 'down')
        items.sort(function(a, b) {
            return +$(a).data(sortType) - +$(b).data(sortType);
        });
    items.appendTo('.plots');
}

// Display list

function areasDisplay() {
    page = Number($('.widget-areaslist #arealist').attr('data-page'));
    pageMax = Math.ceil($('.plots .plot').length / areas_on_pages) - 1;

    $('.widget-areaslist .pages .info .act').html((page + 1));
    $('.widget-areaslist .pages .info .max').html((pageMax + 1));

    $('.widget-areaslist .plot').fadeOut();
    for (var i = 0; i < areas_on_pages; i++) {
        $('.widget-areaslist .plot').eq((page * areas_on_pages) + i).fadeIn();
    }
}

// Sort

$(document).on('click', '.widget-areaslist .sorts a', function(event) {
    $('.widget-areaslist .sorts a').removeClass('active');
    $(this).addClass('active');
    $(this).toggleClass('up down');

    if ($(this).hasClass('up'))
        dir = 'up'
    if ($(this).hasClass('down'))
        dir = 'down'

    areasSort($(this).data('sort'), dir);
    areasDisplay();
})

// Change page

$(document).on('click', '.widget-areaslist .pages a', function(event) {
    page = Number($('.widget-areaslist #arealist').attr('data-page'));
    pageMax = Math.ceil($('.plots .plot').length / areas_on_pages) - 1;

    if ($(this).hasClass('prev'))
        page--
        if ($(this).hasClass('next'))
            page++

            $('.widget-areaslist .pages a').removeClass('off');

    if (page <= 0) {
        page = 0;
        $('.widget-areaslist .pages a.prev').addClass('off');
    }
    if (page >= pageMax) {
        page = pageMax;
        $('.widget-areaslist .pages a.next').addClass('off');
    }
    $('.widget-areaslist #arealist').attr('data-page', page);

    areasDisplay($(this));

    $('html, body').stop().animate({
        scrollTop: $('.widget-areaslist .plots').offset().top - 200
    }, 350);

})

// Send mesage

$(document).on('click', '.widget-areaslist .send_message', function(event) {
    id = $(this).closest('.plot').data('id');
    $('#modalMessage input[name="nr_dzialki"]').val('Działka nr.' + id)
    $('#modalMessage .header p').html('Działka nr.' + id)
    $('#modalMessage').modal();
});

$(document).on('focus', '.widget-areaslist input[type="text"], .widget-areaslist input[type="email"], .widget-areaslist textarea', function() {
    $(this).closest('label').addClass('focus');
})
$(document).on('blur', '.widget-areaslist input[type="text"], .widget-areaslist input[type="email"], .widget-areaslist textarea', function() {
    $('footer label').removeClass('focus');

    input_text_length = $(this).val()
    if (input_text_length.length > 0)
        $(this).closest('label').addClass('edit');
    else
        $(this).closest('label').removeClass('edit');
});
$('.widget-areaslist input[type="checkbox"]').on('click', function() {
    $(this).closest('label').toggleClass('checked');
});
