<?php

/*
Widget Name: Mielno -
Description:
Author: Bartek Szewczyk
*/


class InvestStep_Widget extends SiteOrigin_Widget {
		function __construct() {
			define('SO_PLUGIN_SLUG', 'mielno_invest_step');
	        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_invest_step/');
	        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_invest_step/');
	        define('SO_PLUGIN_VER', '1');

			$form_options = array(
				'id' => array(
					 'type' => 'number',
					 'label' => __( 'Unikalne ID', 'engine' ),
				),
				'background' => array(
					'type' => 'media',
					'label' => __( 'Tło', 'engine' ),
					'choose' => __( 'Choose image', 'engine' ),
					'update' => __( 'Set image', 'engine' ),
					'library' => 'image',
					'fallback' => true
				),
				'background-color' => array(
					'type' => 'color',
					'label' => __('Wybierz kolor', 'engine'),
					'default' => '#ffffff'
				),
				'background-dark' => array(
	                'type' => 'checkbox',
	                'label' => __('Ciemne tło (białe napisy) ', 'engine'),
	                'default' => false
	            ),


				'html1' => array(
					'type' => 'html',
					'label' => '',
					'rows' => '<br/><h1>Górna sekcja:</h1><hr /><br />'
				),

				'header' => array(
					'type' => 'tinymce',
					'label' => __('Nagłówek', 'engine' ),
					'rows' => 5,
					'default_editor' => 'tinymce',
					'button_filters' => array(
					),
				),


				'buttonTop_active' => array(
					'type' => 'checkbox',
					'label' => __( 'Przycisk - aktywacja', 'engine' ),
					'default' => false
				),
				'buttonTop_text' => array(
					 'type' => 'text',
					 'label' => __( 'Przycisk - tekst', 'engine' ),
				),
				'buttonTop_target' => array(
					'type' => 'checkbox',
				    'label' => __('Otwórz w nowym oknie', 'engine'),
				    'default' => false
				),
				'buttonTop_url' => array(
					'type' => 'link',
					'label' => __( 'Przycisk - link do podstrony', 'engine' ),
				),

				'html13453' => array(
					'type' => 'html',
					'label' => '',
					'rows' => '<br/><h1>Sekcje z kwadratami:</h1><hr /><br />'
				),

				'lines_side' => array(
					'type' => 'checkbox',
					'label' => __('Zacznij od lustrzanego odbicia', 'engine' ),
					'default' => false
				),
				'lines' => array(
			        'type' => 'repeater',
			        'label' => __( 'Linia' , 'engine' ),
			        'item_name'  => __( 'Linia', 'engine' ),
			        'item_label' => array(
			            'selector'     => "[id*='opinion_author']",
			            'update_event' => 'change',
			            'value_method' => 'val'
			        ),
			        'fields' => array(
						'line_background' => array(
							'type' => 'media',
							'label' => __( 'Zdjęcie dla kwadratów', 'engine' ),
							'choose' => __( 'Wybierz zdjęcie', 'engine' ),
							'update' => __( 'Zapisz zdjęcie', 'engine' ),
							'library' => 'image'
						),
			            'line_text' => array(
			                'type' => 'tinymce',
			                'label' => __( 'Treść', 'engine' ),
							'rows' => 5,
							'default_editor' => 'tinymce',
							'button_filters' => array(
							),
			            ),

						'line_links' => array(
			                'type' => 'repeater',
			                'label' => __('Linki', 'engine'),
			                'item_name' => __('Dodaj link', 'siteorigin-widgets'),
			                'scroll_count' => 10,
			                'item_label' => array(
			                    'selector' => "[id*='some_text']",
			                    'update_event' => 'change',
			                    'value_method' => 'val'
			                ),
			                'fields' => array(
								'button_text' => array(
									 'type' => 'text',
									 'label' => __( 'Przycisk - tekst', 'engine' ),
								),
								'button_url' => array(
									'type' => 'link',
									'label' => __( 'Przycisk - link do podstrony', 'engine' ),
								),
			                )
			            ),
			        )
			    ),

				'html2' => array(
					'type' => 'html',
					'label' => '',
					'rows' => '<br/><h1>Dolna sekcja:</h1><hr /><br />'
				),

				'button_active' => array(
					'type' => 'checkbox',
					'label' => __( 'Przycisk - aktywacja', 'engine' ),
					'default' => false
				),
				'button_text' => array(
					 'type' => 'text',
					 'label' => __( 'Przycisk - tekst', 'engine' ),
				),
				'button_target' => array(
					'type' => 'checkbox',
				    'label' => __('Otwórz w nowym oknie', 'engine'),
				    'default' => false
				),
				'button_url' => array(
					'type' => 'link',
					'label' => __( 'Przycisk - link do podstrony', 'engine' ),
				),

				'footer' => array(
					'type' => 'tinymce',
					'label' => __('Tekst na dole', 'engine' ),
					'rows' => 5,
					'default_editor' => 'tinymce',
					'button_filters' => array(
					),
				),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'mielno_invest_step',
				__('Sekcja: Zysk', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_invest_step/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('InvestStep Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_invest_step/', 'InvestStep_Widget');
?>
