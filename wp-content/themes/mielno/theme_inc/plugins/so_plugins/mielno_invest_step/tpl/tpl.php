<?php
$nazwa_widgetu = "mielno_invest_step";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>




<?php
$backgroundDark = '';
if($instance['background-dark'] == true)
    $backgroundDark = 'dark';

$background = '';
if(strlen($instance['background']) > 0 && $instance['background'] != 0)
    $background = 'background-image: url('.wp_get_attachment_image_src($instance['background'], 'full', true)[0].');';

$backgroundcolor = '';
if(strlen($instance['background-color']) > 0)
    $backgroundcolor = 'background-color: '.$instance['background-color'].';';
?>

<a id="<?php echo $instance['id']; ?>"></a>
<div class="widget widget-invest-step <?php echo $backgroundDark; ?> so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
    <div class="container">
        <div class="row">
			<div class="col-md-12">
				<div class="header"><?php echo $instance['header']; ?></div>

                <?php
                if($instance['buttonTop_active'] == 'active'){
                    $href = '#';
                    if(strlen($instance['buttonTop_url']) > 0){
                        if(strpos($instance['buttonTop_url'], 'post:') === false){
                            $href = $instance['buttonTop_url'];
                        } else {
                            $hreflId = str_replace('post: ','', $instance['buttonTop_url']);
                            $href = get_permalink($hreflId);
                        }
                    }

                    $target = '';
                    if($instance['buttonTop_target'] == true){
                        $target = 'target="_blank"';
                    }
                                      ?>
                    <a class="bt-top element-bt-arrow hover-black" href="<?php echo $href; ?>" <?php echo $target; ?>>
                        <?php if($instance['button_icon']){ ?>
                            <span class="icon"><?php echo siteorigin_widget_get_icon( $instance['buttonTop_icon'] ); ?></span>
                        <?php } ?>
                        <span class="text"><?php echo $instance['buttonTop_text']; ?></span>
                        <svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
                            <line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
                            <line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
                            <line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
                        </svg>
                    </a>
                <?php } ?>

			</div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?php
                $x = 0;
                if($instance['lines_side'])
                    $x = 1;
                foreach ($instance['lines'] as $keyLine => $valueLine) {
                    $links = '';
                    if(sizeof($valueLine['line_links']) > 0){
                        foreach ($valueLine['line_links'] as $keyLink => $valueLink) {
                            $links .= '<a class="element-bt-arrow hover-black" href="'.$valueLink['button_url'].'"><span class="text">'.$valueLink['button_text'].'</span>';
                            $links .= '<svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
    							<line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
    							<line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
    							<line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
    						</svg>';
                            $links .= '</a>';
                        }
                    }
                    if($x%2 == 0){
                        ?>

                        <div class="row line">
                            <div class="col-sm-4 col-md-4 hidden-xs">
                                <div class="photo photo-small" style="background-image: url('<?php echo wp_get_attachment_image_src($valueLine['line_background'], 'full', true)[0]; ?>')"></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="photo photo-big" style="background-image: url('<?php echo wp_get_attachment_image_src($valueLine['line_background'], 'full', true)[0]; ?>')"></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="description"><?php echo $valueLine['line_text']; ?></div>
                                <div class="links"><?php echo $links; ?></div>
                            </div>
                        </div>

                    <?php }else{ ?>

                        <div class="row line">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="description"><?php echo $valueLine['line_text']; ?></div>
                                <div class="links"><?php echo $links; ?></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="photo photo-big" style="background-image: url('<?php	echo wp_get_attachment_image_src($valueLine['line_background'], 'full', true)[0]; ?>')"></div>
                            </div>
                            <div class="col-sm-4 col-md-4 hidden-xs">
                                <div class="photo photo-small" style="background-image: url('<?php	echo wp_get_attachment_image_src($valueLine['line_background'], 'full', true)[0]; ?>')"></div>
                            </div>
                        </div>

                        <?php
                    }
                    ?>

                <?php
                $x++;
            }
            ?>

            <?php
            if($instance['button_active'] == 'active'){
                $href = '#';
                if(strlen($instance['button_url']) > 0){
                    if(strpos($instance['button_url'], 'post:') === false){
                        $href = $instance['button_url'];
                    } else {
                        $hreflId = str_replace('post: ','', $instance['button_url']);
                        $href = get_permalink($hreflId);
                    }
                }

                $target = '';
                if($instance['button_target'] == true){
                    $target = 'target="_blank"';
                }
                                  ?>
                <a class="bt-bottom element-bt hover-black" href="<?php echo $href; ?>" <?php echo $target; ?>>
                    <?php if($instance['button_icon']){ ?>
                        <span class="icon"><?php echo siteorigin_widget_get_icon( $instance['button_icon'] ); ?></span>
                    <?php } ?>
                    <span class="text"><?php echo $instance['button_text']; ?></span>
                    <svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
                        <line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
                        <line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
                        <line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
                    </svg>
                </a>
            <?php } ?>


            <?php if(strlen($instance['footer']) > 0){ ?>
                <div class="footer">
                    <?php echo $instance['footer']; ?>
                </div>
            <?php } ?>


            <svg class="arrow-section" x="0px" y="0px" width="42.156px" height="85.074px" viewBox="0 0 42.156 85.074" enable-background="new 0 0 42.156 85.074" xml:space="preserve">
                <polyline fill="#ffffff" stroke="#bf9e66" stroke-miterlimit="10" points="10.823,0 10.823,45.5 0.823,45.5 20.823,84 41.323,45.5 30.823,45.5 30.823,0 "/>
            </svg>

            </div>
        </div>
    </div>

    <div class="background" style="<?php echo $background; ?><?php echo $backgroundcolor; ?>"></div>

</div>
