$(document).ready(function() {
    $('.gallery').isotope({
        itemSelector: '.gallery-item',
        layoutMode: 'fitRows'
    })
    $(document).on('click', '.gallery-filters a', function() {
        filterValue = $(this).attr('data-filter');
        console.log(filterValue);
        $('.gallery').isotope({
            filter: filterValue
        })

        $('.gallery-filters a').removeClass('active');
        $(this).addClass('active');
    });

    // Lightbox
    // Open
    $(document).on('click', '.widget-gallery .gallery .gallery-item', function() {
        $(this).addClass('active');

        //i = $(this).index();
        i = -1;
        $('.widget-gallery .gallery .gallery-item').each(function() {
            if ($(this).css('display') == 'block'){
                i++;
            }
            if ($(this).hasClass('active'))
                return false;

        })

        $('.lightbox').addClass('open');

        lightboxLoad(i);
    });
    // Next / Prev
    $(document).on('click', '.widget-gallery .lightbox .arrow', function() {
        i = -1;
        $('.widget-gallery .gallery .gallery-item').each(function() {
            if ($(this).css('display') == 'block'){
                i++;
            }
            if ($(this).hasClass('active'))
                return false;
        })

        iMax = $('.widget-gallery .gallery .gallery-item:visible').length - 1;
        if ($(this).hasClass('prev'))
            i--;
        if ($(this).hasClass('next'))
            i++;

        if(i < 0)
            i = iMax;
        if(i > iMax)
            i = 0;

        $('.widget-gallery .gallery .gallery-item').removeClass('active');
        $('.widget-gallery .gallery .gallery-item:visible').eq(i).addClass('active');

        lightboxLoad(i);
    });

    // Lightbox:
    // Close
    $(document).on('click', '.widget-gallery .lightbox .x', function() {
        $('.lightbox').removeClass('open');
        $('.lightbox .wrap').html('');
        $('.widget-gallery .gallery .gallery-item').removeClass('active');
    });

    // Lightbox:
    // load
    function lightboxLoad(i) {
        $('.lightbox').addClass('load');

        url = $('.widget-gallery .gallery .gallery-item.active').data('url')
        title = $('.widget-gallery .gallery .gallery-item.active').data('title')
        desc = $('.widget-gallery .gallery .gallery-item.active').data('desc')
        type = $('.widget-gallery .gallery .gallery-item.active').data('type')

        $('.lightbox .wrap').html('');

        $(".lightbox .wrap").css("width","");
        $(".lightbox .wrap").css("height","");


        // photo
        if (type == 'photo' || type == 'visualisation') {
            $('.lightbox .wrap').append('<img src="' + url + '" />');
            $('.lightbox .wrap').append('<div class="info"><div class="title">' + title + '</div><div class="desc">' + desc + '</div></div>');
            $('.lightbox .wrap img').bind('load', function() {
                ratio = $(this).height() / $(this).width()

                $('.lightbox .wrap').attr('data-ratio', ratio);
                $('.lightbox .wrap img').css({
                    'max-width': $(this).width() + 'px',
                    'max-height': $(this).height() + 'px'
                })
                $('.lightbox').removeClass('load');

                lightboxResize();
            });
        }
        // video
        if (type == 'video') {
            $('.lightbox .wrap').attr('data-ratio', 0.5);

            if(url.search('youtube') > -1)
                $('.lightbox .wrap').append('<iframe src="'+url+'?autoplay=1" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            if(url.search('vimeo') > -1)
                $('.lightbox .wrap').append('<iframe src="'+url+'?autoplay=1" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');

            $('.lightbox .wrap').append('<div class="info"><div class="title">' + title + '</div><div class="desc">' + desc + '</div></div>');
            $('.lightbox').removeClass('load');
            lightboxResize()
        }

    }

    // Lightbox:
    // resize
    function lightboxResize() {
        ratio = Number($('.lightbox .wrap').attr('data-ratio'));

        wWindow = $(window).width();
        hWindow = $(window).height();

        marg = 400;
        if(wWindow < 992)
            marg = 300;
        if(wWindow < 767)
            marg = 150;

        wLightbox = (wWindow - marg);
        hLightbox = wLightbox * ratio;

        if(hLightbox > (hWindow - marg)){
            hLightbox = (hWindow - marg)
            wLightbox = (1/ratio) * hLightbox;
        }

        $('.lightbox .wrap').css({
            'width': wLightbox + 'px',
            'height': hLightbox + 'px'
        });
    }


    $(window).resize(function() {
        lightboxResize()
    });
})
