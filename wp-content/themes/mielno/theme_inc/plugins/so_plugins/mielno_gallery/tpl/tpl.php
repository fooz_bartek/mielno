<?php
$nazwa_widgetu = "gallery-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>


<div class="widget widget-gallery so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">

	<div class="lightbox load">
		<a class="x">
			<svg x="0px" y="0px" width="41.457px" height="41.457px" viewBox="0 0 41.457 41.457" enable-background="new 0 0 41.457 41.457" xml:space="preserve">
				<path class="line1" fill="none" stroke="#bf9e66" stroke-miterlimit="10" d="M0.354,0.354l40.75,40.75"/>
				<path class="line2" fill="none" stroke="#bf9e66" stroke-miterlimit="10" d="M41.104,0.354l-40.75,40.75"/>
			</svg>
		</a>
		<a class="arrow prev">
			<svg x="0px" y="0px" width="25px" height="49.5px" viewBox="0 0 25 49.5" enable-background="new 0 0 25 49.5" xml:space="preserve">
				<path fill="none" stroke="#bf9e66" stroke-miterlimit="10" d="M24.5,49.5L0,25L25,0"/>
			</svg>
		</a>
		<a class="arrow next">
			<svg x="0px" y="0px" width="25px" height="49.5px" viewBox="0 0 25 49.5" enable-background="new 0 0 25 49.5" xml:space="preserve">
				<path fill="none" stroke="#bf9e66" stroke-miterlimit="10" d="M0.499,49.5L25,25L0,0"/>
			</svg>
		</a>
		<div class="loader"></div>
		<div class="wrap"></div>
	</div>


	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="header"><?php echo $instance['header']; ?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="gallery-filters">
				<a class="active" data-filter="*"><?php echo __('wszystkie', 'textdomain' ); ?></a>
				<a class="" data-filter=".photo"><?php echo __('zdjęcia', 'textdomain' ); ?></a>
				<a class="" data-filter=".video"><?php echo __('filmy', 'textdomain' ); ?></a>
				<a class="" data-filter=".visualisation"><?php echo __('wizualizacje', 'textdomain' ); ?></a>
		    </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="gallery">
				<?php
				function getYoutubeId($url) {
					if (stristr($url, 'youtu.be/')) {
						preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID);
						return $final_ID[4];
					} else {
						@preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD);
						return $IDD[5];
					}
				}
				function getVimeoId($url) {
					if (preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $url, $output_array)) {
						return $output_array[5];
					}
				}

				foreach ($instance['gallery'] as $key => $value) {
					// ========
					// Photo, Visualisation
					$background = $url = $title = $desc = '';
					$title = $value['gallery_title'];
					$desc = $value['gallery_desc'];
					if($value['gallery_type'] == 'photo' || $value['gallery_type'] == 'visualisation'){
						$background = wp_get_attachment_image_src($value['gallery_photo'], 'medium', true)[0];
						$url = wp_get_attachment_image_src($value['gallery_photo'], 'full', true)[0];
					}
					// ========
					// Video
					if($value['gallery_type'] == 'video'){
						$pos = strpos($value['gallery_video'], 'youtube', 1);
						if ($pos) {
							$background = 'http://img.youtube.com/vi/'.getYoutubeId($value['gallery_video']).'/maxresdefault.jpg';
							$url = 'http://www.youtube.com/embed/'.getYoutubeId($value['gallery_video']);
						}
						$pos = strpos($value['gallery_video'], 'vimeo', 1);
						if ($pos) {
							$id = getVimeoId($value['gallery_video']);

						    $ch = curl_init();
						    curl_setopt($ch, CURLOPT_URL, "http://vimeo.com/api/v2/video/$id.php");
						    curl_setopt($ch, CURLOPT_HEADER, 0);
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
						    $output = unserialize(curl_exec($ch));
						    $output = $output[0];
						    curl_close($ch);

							//echo '<pre>',print_r($output,1),'</pre>';

							$background = $output['thumbnail_large'];
							$url = 'http://player.vimeo.com/video/'.$id;
						}
					}

					?>
					<div class="gallery-item <?php echo $value['gallery_type']; ?>" data-type="<?php echo $value['gallery_type']; ?>" data-url="<?php echo $url; ?>" data-title="<?php echo $title; ?>"  data-desc="<?php echo $desc; ?>">
						<!-- Photo -->
						<?php if($value['gallery_type'] == 'photo' || $value['gallery_type'] == 'visualisation'){ ?>
							<div class="hover">
								<div class="icon-open">
									<svg x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
									<path class="line1" fill="none" stroke="#bf9e66" stroke-width="1.5" stroke-miterlimit="10" d="M25.5,0v50"/>
									<path class="line2" fill="none" stroke="#bf9e66" stroke-width="1.5" stroke-miterlimit="10" d="M50,25.5H0"/>
									</svg>
								</div>
							</div>
							<div class="background" style="background-image: url(<?php echo $background; ?>)"></div>
						<?php } ?>

						<!-- Video -->
						<?php if($value['gallery_type'] == 'video'){ ?>
							<div class="icon-play">
								<svg x="0px" y="0px" width="26.25px" height="31.25px" viewBox="0 0 26.25 31.25" enable-background="new 0 0 26.25 31.25" xml:space="preserve">
									<line fill="none" stroke="#bf9e66" stroke-width="1.5" stroke-miterlimit="10" x1="1.25" y1="1.25" x2="25" y2="15.5"/>
									<line fill="none" stroke="#bf9e66" stroke-width="1.5" stroke-miterlimit="10" x1="1.5" y1="1" x2="1.5" y2="30"/>
									<line fill="none" stroke="#bf9e66" stroke-width="1.5" stroke-miterlimit="10" x1="1.25" y1="29.75" x2="25" y2="15.5"/>
								</svg>
							</div>
							<div class="hover">
								<div class="icon-open">
									<svg x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
									<path class="line1" fill="none" stroke="#bf9e66" stroke-width="1.5" stroke-miterlimit="10" d="M25.5,0v50"/>
									<path class="line2" fill="none" stroke="#bf9e66" stroke-width="1.5" stroke-miterlimit="10" d="M50,25.5H0"/>
									</svg>
								</div>
							</div>
							<div class="background" style="background-image: url(<?php echo $background; ?>)"></div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-10 col-md-8 center-block">
				<div class="disclaimer"><?php echo $instance['disclaimer']; ?></div>
		</div>
	</div>
</div>
