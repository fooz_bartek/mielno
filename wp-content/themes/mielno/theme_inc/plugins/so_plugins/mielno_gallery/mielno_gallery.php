<?php

/*
Widget Name: Mielno
Description:
Author:
*/

class Gallery_Widget extends SiteOrigin_Widget {

		function __construct() {
            define('SO_PLUGIN_SLUG_Gallery', 'gallery-widget');
            define('SO_PLUGIN_DIR_Gallery', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_gallery/');
            define('SO_PLUGIN_URL_Gallery', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_gallery/');
            define('SO_PLUGIN_VER_Gallery', '1');

			$form_options = array(
                'header' => array(
    				'type' => 'tinymce',
    				'label' => __('Nagłówek', 'engine' ),
    				'rows' => 5,
    				'default_editor' => 'tinymce',
    				'button_filters' => array(
    				),
    			),
    			'gallery' => array(
    				'type' => 'repeater',
    				'label' => __( 'Galeria' , 'engine' ),
    				'item_name'  => __( 'Zdjęcie / Video / Wizualizacja', 'siteorigin-widgets' ),
    				'item_label' => array(
    					'selector'     => "[id*='gallery_type']",
    					'update_event' => 'change',
    					'value_method' => 'val'
    				),
    				'fields' => array(
    					'gallery_type' => array(
    						'type' => 'radio',
    						'label' => __( 'Rodzaj materiału', 'widget-form-fields-text-domain' ),
    						'default' => 'photo',
    						'options' => array(
    							'photo' => __('Zdjęcie', 'engine' ),
    							'video' => __('Video', 'engine' ),
    							'visualisation' => __('Wizualizacja', 'engine' )
    						)
    					),
    					'gallery_photo' => array(
    						'type' => 'media',
    						'label' => __( 'Grafika', 'engine' ),
    						'choose' => __( 'Dodaj grafike', 'engine' ),
    						'update' => __( 'Ustaw grafike', 'engine' ),
    						'library' => 'image'
    					),
    					'gallery_video' => array(
    						'type' => 'text',
    						'label' => __('Link (bezpośredni link do youtube lub vimeo)', 'engine' )
    					),
    					'gallery_title' => array(
    						'type' => 'text',
    						'label' => __('Tytuł', 'engine' )
    					),
    					'gallery_desc' => array(
    						'type' => 'textarea',
    						'label' => __('Opis', 'engine' )
    					),
    				)
    			),
    			'disclaimer' => array(
    				'type' => 'textarea',
    				'label' => __('Uwaga prawna', 'engine' ),
    				'rows' => 5
    			),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'mielno_gallery',
				__('Gallery widget', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_gallery/'
				);
		}

        function initialize(){
            $this->register_frontend_scripts(
                array(
                    array(SO_PLUGIN_SLUG_Gallery, SO_PLUGIN_URL_Gallery . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER_Gallery, true)
                )
            );
            $this->register_frontend_styles(
                array(
                    array(SO_PLUGIN_SLUG_Gallery, SO_PLUGIN_URL_Gallery . 'tpl/main.css', array(), SO_PLUGIN_VER_Gallery)
                )
            );
        }

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Gallery Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_gallery/', 'Gallery_Widget');
?>
