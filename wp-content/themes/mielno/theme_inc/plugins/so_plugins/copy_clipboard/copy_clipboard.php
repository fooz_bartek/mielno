<?php

/*
Widget Name: Przykładowy widget
Description: Wzorcowy widget.
Author: Wojciech Zdziejowski
*/

siteorigin_widget_register('Copy_clipboard', THEME_PLUGIN_DIRECTORY . 'so_plugins/copy_clipboard/', 'copy_clipboard');
class copy_clipboard extends SiteOrigin_Widget
{

    function __construct()
    {
        define('SO_PLUGIN_SLUG', 'copy_clipboard');
        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/copy_clipboard/');
        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/copy_clipboard/');
        define('SO_PLUGIN_VER', '1');
        //================================
        //Call the parent constructor with the required arguments.
        //DATA FOR WIDGET
        //================================
        parent::__construct(
        // The unique id for your widget.
            SO_PLUGIN_SLUG,

            // The name of the widget for display purposes.
            __('Copy to clipboard', 'engine'),

            // The $widget_options array, which is passed through to WP_Widget.
            // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
            array(
                'description' => __('Copy text to clipboard.', 'engine'),
                'help' => TEMPL_OWNER_URL,
                'panels_groups' => array(TEMPL_NAME),
                'panels_icon' => 'dashicons dashicons-welcome-add-page'
            ),

            //The $control_options array, which is passed through to WP_Widget
            array(),

            //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
            false,

            //! The $base_folder path string.
            SO_PLUGIN_DIR
        );
        add_filter('siteorigin_widgets_sanitize_field_date', array($this, 'sanitize_date'));

    }

    function get_widget_form()
    {
        //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.
        return array(
            'some_tinymce_editor' => array(
                'type' => 'tinymce',
                'label' => __('Edytor wizualny.', 'engine'),
                'default' => 'Wprowadź tekst',
                'rows' => 15,
                'default_editor' => 'html',
                'button_filters' => array(
                    'mce_buttons' => array($this, 'filter_mce_buttons'),
                    'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
                    'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
                    'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
                    'quicktags_settings' => array($this, 'filter_quicktags_settings'),
                ),
            ),
            'some_text' => array(
                'type' => 'text',
                'label' => __('Wprowadź tekst', 'engine'),
                'default' => ''
            ),
            'some_url' => array(
                'type' => 'link',
                'label' => __('Dodaj link', 'engine'),
                'default' => 'http://www.example.com'
            ),
            'some_color' => array(
                'type' => 'color',
                'label' => __('Wybierz kolor', 'engine'),
                'default' => '#bada55'
            ),
            'some_number' => array(
                'type' => 'number',
                'label' => __('Wprowadź numer', 'engine'),
                'default' => '12654'
            ),
            'some_long_message' => array(
                'type' => 'textarea',
                'label' => __('Wprowadź wiadomość', 'engine'),
                'default' => '',
                'rows' => 5
            ),
            'some_number_in_a_range' => array(
                'type' => 'slider',
                'label' => __('Wybierz numer', 'engine'),
                'default' => 24,
                'min' => 0,
                'max' => 42
            ),
            // Zwykła lista z nagłówkiem
            'some_selection' => array(
                'type' => 'select',
                'label' => __('Wybierz opcję z listy', 'engine'),
                'default' => 'thing_1',
                'options' => array(
                    'thing_1' => __('Opcja 1', 'engine'),
                    'thing_2' => __('Opcja 2', 'engine'),
                    'thing_3' => __('Opcja 3', 'engine'),
                    'thing_4' => __('Opcja 4', 'engine'),
                    'thing_5' => __('Opcja 5', 'engine'),
                    'thing_6' => __('Opcja 6', 'engine'),
                )
            ),
            // lista, gdzie nagłówek to pierwszy element listy, bez default
            'another_selection' => array(
                'type' => 'select',
                'prompt' => __('Wybierz opcję', 'engine'),
                'options' => array(
                    'thing_1' => __('Opcja 1', 'engine'),
                    'thing_2' => __('Opcja 2', 'engine'),
                    'thing_3' => __('Opcja 3', 'engine'),
                    'thing_4' => __('Opcja 4', 'engine'),
                    'thing_5' => __('Opcja 5', 'engine'),
                    'thing_6' => __('Opcja 6', 'engine'),
                )
            ),
            'some_boolean' => array(
                'type' => 'checkbox',
                'label' => __('Zaznacz jeśli ', 'engine'),
                'default' => false
            ),
            'radio_selection' => array(
                'type' => 'radio',
                'label' => __('Wybierz jedną z opcji', 'engine'),
                'default' => '1_element',
                'options' => array(
                    '1_element' => __('Opcja 1', 'engine'),
                    '2_element' => __('Opcja 2', 'engine'),
                    '3_element' => __('Opcja 3', 'engine')
                )
            ),
            'some_media' => array(
                'type' => 'media',
                'label' => __('Dodaj obraz', 'engine'),
                'choose' => __('Wybierz', 'engine'),
                'update' => __('Ustaw', 'engine'),
                'library' => 'image',//'image', 'audio', 'video', 'file', 'application'
                'fallback' => true
            ),
            'some_posts' => array(
                'type' => 'posts',
                'label' => __('Lista wpisów', 'engine'),
            ),
            'section' => array(
                'type' => 'section',
                'label' => __('Sekcja do rozwinięcia.', 'engine'),
                'hide' => true,
                'fields' => array(
                    'some_text' => array(
                        'type' => 'text',
                        'label' => __('Wprowadź tekst', 'engine'),
                        'default' => ''
                    ),
                    'some_media' => array(
                        'type' => 'media',
                        'label' => __('Dodaj obraz', 'engine'),
                        'choose' => __('Wybierz', 'engine'),
                        'update' => __('Ustaw', 'engine'),
                        'library' => 'image',//'image', 'audio', 'video', 'file', 'application'
                        'fallback' => true
                    ),
                    'some_url' => array(
                        'type' => 'link',
                        'label' => __('Dodaj link', 'engine'),
                        'default' => 'http://www.example.com'
                    )
                )
            ),
            'repeater' => array(
                'type' => 'repeater',
                'label' => __('Element z możliwością dodania kolejnych.', 'engine'),
                'item_name' => __('Dodaj kolejny element', 'siteorigin-widgets'),
                'scroll_count' => 10,
                'item_label' => array(
                    'selector' => "[id*='some_text']",
                    'update_event' => 'change',
                    'value_method' => 'val'
                ),
                'fields' => array(
                    'some_text' => array(
                        'type' => 'text',
                        'label' => __('Wprowadź tekst', 'engine'),
                        'default' => ''
                    ),
                    'some_media' => array(
                        'type' => 'media',
                        'label' => __('Dodaj obraz', 'engine'),
                        'choose' => __('Wybierz', 'engine'),
                        'update' => __('Ustaw', 'engine'),
                        'library' => 'image',//'image', 'audio', 'video', 'file'
                        'fallback' => true
                    ),
                    'some_url' => array(
                        'type' => 'link',
                        'label' => __('Dodaj link', 'engine'),
                        'default' => 'http://www.example.com'
                    )
                )
            ),
            /*
            'some_widget' => array(
                'type' => 'widget',
                'label' => __( 'Button Widget', 'engine' ),
                'class' => 'SiteOrigin_Widget_Button_Widget',
                'hide' => true
            ),*/
            'some_icon' => array(
                'type' => 'icon',
                'label' => __('Wybierz ikonę', 'engine'),
            ),

            'some_font' => array(
                'type' => 'font',
                'label' => __('Wybierz krój pisma (font)', 'engine'),
            ),
            'some_date' => array(
                'type' => 'text',
                'label' => __('Data', 'engine'),
                'sanitize' => 'date',
            ),

            /* element stały dla paneli */

            'ustawienia' => array(
                'type' => 'section',
                'label' => __('Ustawienia sekcji.', 'engine'),
                'hide' => true,
                'fields' => array(
                    'border-top' => array(
                        'type' => 'number',
                        'label' => __('Ustaw górną grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),
                    'border-bottom' => array(
                        'type' => 'number',
                        'label' => __('Ustaw dolną grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),
                    'border-left' => array(
                        'type' => 'number',
                        'label' => __('Ustaw lewą grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),
                    'border-right' => array(
                        'type' => 'number',
                        'label' => __('Ustaw prawą grubość obramowania (px)', 'engine'),
                        'default' => '0'
                    ),

                    'animation' => array(
                        'type' => 'checkbox',
                        'label' => __('Zaznacz jeśli chcesz dodać animację do tego elementu', 'engine'),
                        'default' => false
                    ),
                    'makeanimation' => array(
                        'type' => 'select',
                        'label' => __('wybierz typ animacji pojawienia się elementu', 'engine'),
                        'prompt' => __('Wybierz opcję', 'engine'),
                        'options' => array(
                            'bounceIn' => 'bounceIn',
                            'bounceInDown' => 'bounceInDown',
                            'bounceInLeft' => 'bounceInLeft',
                            'bounceInRight' => 'bounceInRight',
                            'bounceInUp' => 'bounceInUp',
                            'fadeIn' => 'fadeIn',
                            'fadeInDown' => 'fadeInDown',
                            'fadeInDownBig' => 'fadeInDownBig',
                            'fadeInLeft' => 'fadeInLeft',
                            'fadeInLeftBig' => 'fadeInLeftBig',
                            'fadeInRight' => 'fadeInRight',
                            'fadeInRightBig' => 'fadeInRightBig',
                            'fadeInUp' => 'fadeInUp',
                            'fadeInUpBig' => 'fadeInUpBig',
                            'flipInX' => 'flipInX',
                            'flipInY' => 'flipInY',
                            'slideInUp' => 'slideInUp',
                            'slideInDown' => 'slideInDown',
                            'slideInLeft' => 'slideInLeft',
                            'slideInRight' => 'slideInRight',
                            'zoomIn' => 'zoomIn',
                            'zoomInDown' => 'zoomInDown',
                            'zoomInLeft' => 'zoomInLeft',
                            'zoomInRight' => 'zoomInRight',
                            'zoomInUp' => 'zoomInUp',
                        )
                    ),
                )
            ),
            /* end element stały dla paneli */
        );
    }

    function initialize()
    {

        $this->register_frontend_scripts(
            array(
                array(SO_PLUGIN_SLUG, SO_PLUGIN_URL . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER, true)
            )
        );

        $this->register_frontend_styles(
            array(
                array(SO_PLUGIN_SLUG, SO_PLUGIN_URL . 'tpl/main.css', array(), SO_PLUGIN_VER)
            )
        );

    }

    public function widget($args, $instance)
    {
        include('tpl/tpl.php');
    }

    function get_template_name($instance)
    {
        return 'tpl';
    }

    function get_style_name($instance)
    {
        return '';
    }

    function filter_mce_buttons($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_2($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_3($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    function filter_mce_buttons_4($buttons, $editor_id)
    {
        if (($key = array_search('fullscreen', $buttons)) !== false ||
            ($key = array_search('dfw', $buttons)) !== false
        ) {
            unset($buttons[$key]);
        }
        return $buttons;
    }

    public function quicktags_settings($settings, $editor_id)
    {
        $settings['buttons'] = preg_replace('/,fullscreen/', '', $settings['buttons']);
        $settings['buttons'] = preg_replace('/,dfw/', '', $settings['buttons']);
        return $settings;
    }

    function sanitize_date($date_to_sanitize)
    {
        // Perform custom date sanitization here.
        $sanitized_date = sanitize_text_field($date_to_sanitize);
        return $sanitized_date;
    }


}

/*
wszystkie dostępne animacje

        <optgroup label="Attention Seekers' => '
          'bounce' => 'bounce',
          'flash' => 'flash',
          'pulse' => 'pulse',
          'rubberBand' => 'rubberBand',
          'shake' => 'shake',
          'swing' => 'swing',
          'tada' => 'tada',
          'wobble' => 'wobble',
          'jello' => 'jello',
        </optgroup>

        <optgroup label="Bouncing Entrances' => '
          'bounceIn' => 'bounceIn',
          'bounceInDown' => 'bounceInDown',
          'bounceInLeft' => 'bounceInLeft',
          'bounceInRight' => 'bounceInRight',
          'bounceInUp' => 'bounceInUp',
        </optgroup>

        <optgroup label="Bouncing Exits' => '
          'bounceOut' => 'bounceOut',
          'bounceOutDown' => 'bounceOutDown',
          'bounceOutLeft' => 'bounceOutLeft',
          'bounceOutRight' => 'bounceOutRight',
          'bounceOutUp' => 'bounceOutUp',
        </optgroup>

        <optgroup label="Fading Entrances' => '
          'fadeIn' => 'fadeIn',
          'fadeInDown' => 'fadeInDown',
          'fadeInDownBig' => 'fadeInDownBig',
          'fadeInLeft' => 'fadeInLeft',
          'fadeInLeftBig' => 'fadeInLeftBig',
          'fadeInRight' => 'fadeInRight',
          'fadeInRightBig' => 'fadeInRightBig',
          'fadeInUp' => 'fadeInUp',
          'fadeInUpBig' => 'fadeInUpBig',
        </optgroup>

        <optgroup label="Fading Exits' => '
          'fadeOut' => 'fadeOut',
          'fadeOutDown' => 'fadeOutDown',
          'fadeOutDownBig' => 'fadeOutDownBig',
          'fadeOutLeft' => 'fadeOutLeft',
          'fadeOutLeftBig' => 'fadeOutLeftBig',
          'fadeOutRight' => 'fadeOutRight',
          'fadeOutRightBig' => 'fadeOutRightBig',
          'fadeOutUp' => 'fadeOutUp',
          'fadeOutUpBig' => 'fadeOutUpBig',
        </optgroup>

        <optgroup label="Flippers' => '
          'flip' => 'flip',
          'flipInX' => 'flipInX',
          'flipInY' => 'flipInY',
          'flipOutX' => 'flipOutX',
          'flipOutY' => 'flipOutY',
        </optgroup>

        <optgroup label="Lightspeed' => '
          'lightSpeedIn' => 'lightSpeedIn',
          'lightSpeedOut' => 'lightSpeedOut',
        </optgroup>

        <optgroup label="Rotating Entrances' => '
          'rotateIn' => 'rotateIn',
          'rotateInDownLeft' => 'rotateInDownLeft',
          'rotateInDownRight' => 'rotateInDownRight',
          'rotateInUpLeft' => 'rotateInUpLeft',
          'rotateInUpRight' => 'rotateInUpRight',
        </optgroup>

        <optgroup label="Rotating Exits' => '
          'rotateOut' => 'rotateOut',
          'rotateOutDownLeft' => 'rotateOutDownLeft',
          'rotateOutDownRight' => 'rotateOutDownRight',
          'rotateOutUpLeft' => 'rotateOutUpLeft',
          'rotateOutUpRight' => 'rotateOutUpRight',
        </optgroup>

        <optgroup label="Sliding Entrances' => '
          'slideInUp' => 'slideInUp',
          'slideInDown' => 'slideInDown',
          'slideInLeft' => 'slideInLeft',
          'slideInRight' => 'slideInRight',

        </optgroup>
        <optgroup label="Sliding Exits' => '
          'slideOutUp' => 'slideOutUp',
          'slideOutDown' => 'slideOutDown',
          'slideOutLeft' => 'slideOutLeft',
          'slideOutRight' => 'slideOutRight',

        </optgroup>

        <optgroup label="Zoom Entrances' => '
          'zoomIn' => 'zoomIn',
          'zoomInDown' => 'zoomInDown',
          'zoomInLeft' => 'zoomInLeft',
          'zoomInRight' => 'zoomInRight',
          'zoomInUp' => 'zoomInUp',
        </optgroup>

        <optgroup label="Zoom Exits' => '
          'zoomOut' => 'zoomOut',
          'zoomOutDown' => 'zoomOutDown',
          'zoomOutLeft' => 'zoomOutLeft',
          'zoomOutRight' => 'zoomOutRight',
          'zoomOutUp' => 'zoomOutUp',
        </optgroup>

        <optgroup label="Specials' => '
          'hinge' => 'hinge',
          'rollIn' => 'rollIn',
          'rollOut' => 'rollOut',
        </optgroup>


*/
?>
