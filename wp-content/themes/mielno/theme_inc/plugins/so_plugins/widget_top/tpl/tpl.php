<?php
$widget_name = "top";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-top so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
	<div class="center">
        <div class="col-lg-12"><div class="header styleText40"><?php echo $instance['header']; ?></div></div>

        <?php if($instance['button_active'] == 'active'){ ?>
            <div class="bt play">
                <svg x="0px" y="0px" width="45.292px" height="45.292px" viewBox="0 0 45.292 45.292" enable-background="new 0 0 45.292 45.292" xml:space="preserve">
                    <path class="line1" fill="none" stroke-miterlimit="10" d="M18.978,12.261l12.183,9.485L16.917,32.789V15.583"/>
                    <path class="line2" fill="none" stroke-miterlimit="10" d="M43.729,22.646c0,11.645-9.439,21.084-21.084,21.084 c-11.644,0-21.083-9.439-21.083-21.084c0-11.644,9.439-21.083,21.083-21.083C34.29,1.562,43.729,11.002,43.729,22.646z"/>
                </svg>
                <div class="text"><?php echo __('Poznaj Intrę', 'textdomain' ); ?></div>
            </div>
		<?php } ?>
	</div>
	<div class="background" style="background-image: url(<?php if(strlen($instance['background']) > 0) echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>

    <?php
    if(strlen($instance['video_mp4']) > 0){
        $src = wp_get_attachment_url( $instance['video_mp4'] );
    ?>
        <div class="video_container">
            <video class="videoCenter" data-parent="window">
                <source src="<?php echo $src; ?>" type="video/mp4">
            </video>
        </div>
    <?php } ?>
</div>
