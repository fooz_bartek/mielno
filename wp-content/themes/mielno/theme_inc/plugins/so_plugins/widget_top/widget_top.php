<?php

/*
Widget Name: Intra
Description:
Author:
*/


class Top_Widget extends SiteOrigin_Widget {

	const SO_PLUGIN_SLUG = 'widget_top';
    const SO_PLUGIN_DIR = THEME_PLUGIN_DIRECTORY . 'so_plugins/widget_top/';
    const SO_PLUGIN_URL = THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/widget_top/';
    const SO_PLUGIN_VER = '1';

		function __construct() {
			$form_options = array(
				'background' => array(
		    		'type' => 'media',
		       		'label' => __( 'Tło', 'engine' ),
				    'choose' => __( 'Choose image', 'engine' ),
				    'update' => __( 'Set image', 'engine' ),
				    'library' => 'image',
				    'fallback' => true
		    	),
				'video_mp4' => array(
					'type' => 'media',
					'label' => __('Video (mp4)', 'engine'),
					'choose' => __('Wybierz', 'engine'),
					'update' => __('Ustaw', 'engine'),
					'library' => 'video'
				),
				'header' => array(
					'type' => 'tinymce',
				   	'label' => __('Nagłówek', 'engine' ),
				   	'rows' => 5,
				   	'default_editor' => 'tinymce',
				   	'button_filters' => array(
				   ),
			    ),

				'html2' => array(
					 'type' => 'html',
					 'label' => '',
					 'rows' => '<br/><h3>Przycisk</h3>'
				),

				'button_active' => array(
					'type' => 'checkbox',
					'label' => __( 'Przycisk: aktywacja', 'engine' ),
					'default' => false
				),
				'button_text' => array(
					 'type' => 'text',
					 'label' => __( 'Przycisk: tekst', 'engine' ),
				),
				'button_url' => array(
					 'type' => 'link',
					 'label' => __( 'Przycisk: url', 'engine' ),
				),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'widget_top',
				__('Top', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/widget_top/'
				);
		}

		function initialize(){
	        $this->register_frontend_scripts (
	            array(
	                array(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/script.js', array('jquery'), self::SO_PLUGIN_VER, true)
	            )
	        );
	    }
		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Top Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/widget_top/', 'Top_Widget');
?>
