<?php
$nazwa_widgetu = "welcome2-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-map so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6"></div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div id="map"></div>
				<div class="info">
					<div class="row">
						<div class="col-xs-12 col-sm-3 col-md-6">
							<?php if($instance['logo']){ ?>
								<img class="logo" src="<?php	echo wp_get_attachment_image_src($instance['logo'], 'full', true)[0]; ?>" alt="SAVEINVEST SP. Z O.O." />
							<?php } ?>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-6">
							<?php echo $instance['text']; ?>
							<?php
							if($instance['button_active'] == 'active'){
								$href = '#';
                                if(strlen($instance['button_url']) > 0){
                                    if(strpos($instance['button_url'], 'post:') === false){
                                        $href = $instance['button_url'];
                                    } else {
                                        $hreflId = str_replace('post: ','', $instance['button_url']);
                                        $href = get_permalink($hreflId);
                                    }
                                }
										 ?>
								<a class="element-bt-arrow hover-black" href="<?php echo $href; ?>">
									<?php if($instance['button_icon']){ ?>
										<span class="icon"><?php echo siteorigin_widget_get_icon( $instance['button_icon'] ); ?></span>
									<?php } ?>
									<?php echo $instance['button_text']; ?>
									<svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
										<line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
										<line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
										<line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
									</svg>
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $instance['apikey']; ?>"></script>
	<script>
	  function initialize() {
	      var block_bg = colorToHex('#FFFFFF'),
	              map_canvas = document.getElementById('map'),
	              pos = new google.maps.LatLng(<?php echo $instance['latlng']; ?>),
	              map_type = 'roadmap' == 'hybrid' ? google.maps.MapTypeId.HYBRID : google.maps.MapTypeId.ROADMAP,

	              map = new google.maps.Map(map_canvas, {
	                  center: pos,
	                  zoom: 15,
	                  scaleControl: false,
	                  zoomControl: true,
	                  panControl: false,
	                  overviewMapControl: false,
	                  navigationControl: false,
	                  mapTypeControl: false,
	                  scaleControl: false,
	                  draggable: true,
	                  disableDoubleClickZoom: false,
	                  scrollwheel: false
	              }),
	              grayscale =[
					  {
					    "featureType": "landscape",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#f8f9ff"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "geometry.fill",
					    "stylers": [
					      {
					        "color": "#e7eaf3"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "labels",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "road",
					    "elementType": "geometry.fill",
					    "stylers": [
					      {
					        "color": "#ffffff"
					      }
					    ]
					  }
					],
	              mapType = new google.maps.StyledMapType(grayscale, {name: "Grayscale Map"});

				  map.mapTypes.set('grayscale', mapType);
				  map.setMapTypeId('grayscale');


	      // Marker
	      image = {
	          url: "<?php echo get_template_directory_uri(); ?>/img/contact_pin.png",
	          size: new google.maps.Size(64, 80),
	          origin: new google.maps.Point(0, 0),
	          anchor: new google.maps.Point(32, 80)
	      }
	      var marker = new google.maps.Marker({
	          position: new google.maps.LatLng(<?php echo $instance['latlng']; ?>),
	          map: map,
	          title: '',
	          icon: image
	      });

	      // Click Enable Scroll
	      google.maps.event.addListener(map, 'click', function (event) {
	          map.setOptions({scrollwheel: true})
	      });

	      google.maps.event.addListener(map, 'dragend', function (event) {
	          map.setOptions({scrollwheel: false})
	      });

	      // Hover Out Disable Scroll
	      map_canvas.onmouseout = function (event) {
	          map.setOptions({scrollwheel: false})
	      };


	      // Resize Event
	      jQuery(window).bind('afterresize', function (ev) {
	          map.panTo(<?php echo $instance['latlng']; ?>);
	      });
	  }

	  function colorToHex(color) {
	      if (color.substr(0, 1) === '#') {
	          return color;
	      }
	      var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

	      var red = parseInt(digits[2]);
	      var green = parseInt(digits[3]);
	      var blue = parseInt(digits[4]);

	      var rgb = blue | (green << 8) | (red << 16);
	      return digits[1] + '#' + rgb.toString(16);
	  }


	  // When the window has finished loading create our google map below
	  google.maps.event.addDomListener(window, 'load', initialize);
	</script>
</div>
