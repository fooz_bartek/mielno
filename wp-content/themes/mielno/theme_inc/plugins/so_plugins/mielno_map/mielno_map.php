<?php

/*
Widget Name: Mielno - Map
Description: Widget
Author: Bartek Szewczyk
*/


class Map_Widget extends SiteOrigin_Widget {
		function __construct() {
			define('SO_PLUGIN_SLUG', 'map-widget');
	        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_map/');
	        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_map/');
	        define('SO_PLUGIN_VER', '1');

			$form_options = array(
				// maps
				'apikey' => array(
					'type' => 'text',
					'label' => __( 'Google Maps Api Key', 'engine' ),
					'default' => ''
				),
				'latlng' => array(
					'type' => 'text',
					'label' => __('Współrzędne dla lokalizacji', 'engine' ),
					'default' => ''
				),

				// info
				'logo' => array(
					'type' => 'media',
					'label' => __( 'Logotyp', 'engine' ),
					'choose' => __( 'Dodaj logotyp', 'engine' ),
					'update' => __( 'Ustaw logotyp', 'engine' ),
					'library' => 'image'
				),
				'text' => array(
			        'type' => 'tinymce',
			        'label' => __('Opis', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			        ),
			    ),
				'button_active' => array(
					'type' => 'checkbox',
					'label' => __( 'Przycisk - aktywacja', 'engine' ),
					'default' => false
				),
				'button_icon' => array(
			        'type' => 'icon',
			        'label' => __('Przycisk - ikona', 'engine'),
			    ),
				'button_text' => array(
					 'type' => 'text',
					 'label' => __( 'Przycisk - tekst', 'engine' ),
				),
				'button_url' => array(
					'type' => 'link',
					'label' => __( 'Przycisk - link do podstrony', 'engine' ),
				)
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'map-widget',
				__('Kontakt - Mapa', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_map/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Map Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_map/', 'Map_Widget');
?>
