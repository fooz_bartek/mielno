<?php

/*
Widget Name: Przykładowy widget
Description: Wzorcowy widget.
Author: Wojciech Zdziejowski
Author URI: http://www.fpweb.pl/
Widget URI: http://www.fpweb.pl/
Video URI: http://www.fpweb.pl/
*/


class Tekst_Widget extends SiteOrigin_Widget {

		function __construct() {
		//Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.
			$form_options = array(
				'tekst' => array(
					'type' => 'text',
					'label' => __( 'Wprowadź tekst', 'engine' ),
					'default' => ''
				),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

	//================================
	//Call the parent constructor with the required arguments.
	//
	//DATA FOR WIDGET
	//================================
		parent::__construct(
				// The unique id for your widget.
				'tekst-widget',

				// The name of the widget for display purposes.
				__('Tekstowy widget', 'engine'),

				// The $widget_options array, which is passed through to WP_Widget.
				// It has a couple of extras like the optional help URL, which should link to your sites help or support page.
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),

				//The $control_options array, which is passed through to WP_Widget
				array(
				),

				//The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
				$form_options,

				//The $base_folder path string.
				THEME_PLUGIN_DIRECTORY.'so_plugins/tekst/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Tekst Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/tekst/', 'Tekst_Widget');

/*
wszystkie dostępne animacje

        <optgroup label="Attention Seekers' => '
          'bounce' => 'bounce',
          'flash' => 'flash',
          'pulse' => 'pulse',
          'rubberBand' => 'rubberBand',
          'shake' => 'shake',
          'swing' => 'swing',
          'tada' => 'tada',
          'wobble' => 'wobble',
          'jello' => 'jello',
        </optgroup>

        <optgroup label="Bouncing Entrances' => '
          'bounceIn' => 'bounceIn',
          'bounceInDown' => 'bounceInDown',
          'bounceInLeft' => 'bounceInLeft',
          'bounceInRight' => 'bounceInRight',
          'bounceInUp' => 'bounceInUp',
        </optgroup>

        <optgroup label="Bouncing Exits' => '
          'bounceOut' => 'bounceOut',
          'bounceOutDown' => 'bounceOutDown',
          'bounceOutLeft' => 'bounceOutLeft',
          'bounceOutRight' => 'bounceOutRight',
          'bounceOutUp' => 'bounceOutUp',
        </optgroup>

        <optgroup label="Fading Entrances' => '
          'fadeIn' => 'fadeIn',
          'fadeInDown' => 'fadeInDown',
          'fadeInDownBig' => 'fadeInDownBig',
          'fadeInLeft' => 'fadeInLeft',
          'fadeInLeftBig' => 'fadeInLeftBig',
          'fadeInRight' => 'fadeInRight',
          'fadeInRightBig' => 'fadeInRightBig',
          'fadeInUp' => 'fadeInUp',
          'fadeInUpBig' => 'fadeInUpBig',
        </optgroup>

        <optgroup label="Fading Exits' => '
          'fadeOut' => 'fadeOut',
          'fadeOutDown' => 'fadeOutDown',
          'fadeOutDownBig' => 'fadeOutDownBig',
          'fadeOutLeft' => 'fadeOutLeft',
          'fadeOutLeftBig' => 'fadeOutLeftBig',
          'fadeOutRight' => 'fadeOutRight',
          'fadeOutRightBig' => 'fadeOutRightBig',
          'fadeOutUp' => 'fadeOutUp',
          'fadeOutUpBig' => 'fadeOutUpBig',
        </optgroup>

        <optgroup label="Flippers' => '
          'flip' => 'flip',
          'flipInX' => 'flipInX',
          'flipInY' => 'flipInY',
          'flipOutX' => 'flipOutX',
          'flipOutY' => 'flipOutY',
        </optgroup>

        <optgroup label="Lightspeed' => '
          'lightSpeedIn' => 'lightSpeedIn',
          'lightSpeedOut' => 'lightSpeedOut',
        </optgroup>

        <optgroup label="Rotating Entrances' => '
          'rotateIn' => 'rotateIn',
          'rotateInDownLeft' => 'rotateInDownLeft',
          'rotateInDownRight' => 'rotateInDownRight',
          'rotateInUpLeft' => 'rotateInUpLeft',
          'rotateInUpRight' => 'rotateInUpRight',
        </optgroup>

        <optgroup label="Rotating Exits' => '
          'rotateOut' => 'rotateOut',
          'rotateOutDownLeft' => 'rotateOutDownLeft',
          'rotateOutDownRight' => 'rotateOutDownRight',
          'rotateOutUpLeft' => 'rotateOutUpLeft',
          'rotateOutUpRight' => 'rotateOutUpRight',
        </optgroup>

        <optgroup label="Sliding Entrances' => '
          'slideInUp' => 'slideInUp',
          'slideInDown' => 'slideInDown',
          'slideInLeft' => 'slideInLeft',
          'slideInRight' => 'slideInRight',

        </optgroup>
        <optgroup label="Sliding Exits' => '
          'slideOutUp' => 'slideOutUp',
          'slideOutDown' => 'slideOutDown',
          'slideOutLeft' => 'slideOutLeft',
          'slideOutRight' => 'slideOutRight',

        </optgroup>

        <optgroup label="Zoom Entrances' => '
          'zoomIn' => 'zoomIn',
          'zoomInDown' => 'zoomInDown',
          'zoomInLeft' => 'zoomInLeft',
          'zoomInRight' => 'zoomInRight',
          'zoomInUp' => 'zoomInUp',
        </optgroup>

        <optgroup label="Zoom Exits' => '
          'zoomOut' => 'zoomOut',
          'zoomOutDown' => 'zoomOutDown',
          'zoomOutLeft' => 'zoomOutLeft',
          'zoomOutRight' => 'zoomOutRight',
          'zoomOutUp' => 'zoomOutUp',
        </optgroup>

        <optgroup label="Specials' => '
          'hinge' => 'hinge',
          'rollIn' => 'rollIn',
          'rollOut' => 'rollOut',
        </optgroup>


*/
?>
