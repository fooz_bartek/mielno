<?php
$nazwa_widgetu="Twoja nazwa";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
$widgetstyle = ' style="';
if( $instance['ustawienia']['border-top'] > 0){
    $widgetstyle .= ' border-top:'.$instance['ustawienia']['border-top'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-botom'] > 0){
    $widgetstyle .= ' border-bottom:'.$instance['ustawienia']['border-bottom'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-left'] > 0){
    $widgetstyle .= ' border-left:'.$instance['ustawienia']['border-left'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
if( $instance['ustawienia']['border-right'] > 0){
    $widgetstyle .= ' border-right:'.$instance['ustawienia']['border-right'].'px solid '.$instance['ustawienia']['border-color'].'; ';
}
$widgetstyle .= '" ';
$imgatr = array( 'class' => 'img-responsive' );
?>
<div id="<?php echo $widgetid; ?>" class="so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="<?php echo $nazwa_widgetu . ' ' . $instance['panels_info']['style']['class'] . ' ' . $widgetpanelstyle; ?>" id="<?php echo $instance['panels_info']['style']['id']; ?>" <?php
    echo $widgetanimation . $widgetstyle; ?> >
        <?php /** Treść twojego widgetu **/ ?>

        Tekst: <?php echo $instance['some_text']; ?>
        <br>
        Link: <?php echo $instance['some_url']; ?>
        <br>
        Wiadomość: <?php echo $instance['some_long_message']; ?>
        </br>
        <?php
        echo wp_get_attachment_image( $instance['some_media'], 'thumbnail', '', $imgatr );
        ?>

        <?php /** Koniec treśći twojego widgetu **/ ?>
    </div>
</div>
