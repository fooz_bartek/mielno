<?php
$nazwa_widgetu = "invest-button";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>


<div class="widget widget-invest-button so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="boxs">
        <svg class="arrow arrow1" x="0px" y="0px" width="85.074px" height="42.156px" viewBox="0 0 85.074 42.156" enable-background="new 0 0 85.074 42.156" xml:space="preserve">
            <polyline fill="#ffffff" stroke="none" stroke-miterlimit="10" points="0,31.583 45.5,31.583 45.5,41.333 84,21.333 45.5,0.833 45.5,11.583 0,11.583 "/>
        </svg>
        <svg class="arrow arrow2" x="0px" y="0px" width="85.074px" height="42.156px" viewBox="0 0 85.074 42.156" enable-background="new 0 0 85.074 42.156" xml:space="preserve">
            <polyline fill="#ffffff" stroke="none" stroke-miterlimit="10" points="0,31.583 45.5,31.583 45.5,41.333 84,21.333 45.5,0.833 45.5,11.583 0,11.583 "/>
        </svg>
        <svg class="arrow arrow3" x="0px" y="0px" width="85.074px" height="42.156px" viewBox="0 0 85.074 42.156" enable-background="new 0 0 85.074 42.156" xml:space="preserve">
            <polyline fill="#ffffff" stroke="none" stroke-miterlimit="10" points="0,31.583 45.5,31.583 45.5,41.333 84,21.333 45.5,0.833 45.5,11.583 0,11.583 "/>
        </svg>
        
        <?php
        $x = 1;
        if(sizeof($instance['bts']) > 0)
        foreach ($instance['bts'] as $keyBts => $valueBts) {
        ?>
    		<a class="box box<?php echo $x; ?>" href="#<?php echo $x; ?>">
                <div class="in">
                    <div class="border border-top-left"></div>
                    <div class="border border-top-right"></div>
                    <div class="border border-bottom-left"></div>
                    <div class="border border-bottom-right"></div>
                    <div class="text">
                        <div class="line1"><?php echo $valueBts['bt_text']; ?></div>
                        <div class="line2"><?php echo $valueBts['bt_text2']; ?></div>
                    </div>
                    <div class="see">Zobacz</div>
                </div>
                <div class="number"><?php echo $x; ?></div>
                <div class="background" style="background-image: url(<?php echo wp_get_attachment_image_src($valueBts['bt_background'], 'full', true)[0]; ?>)"></div>
    		</a>
        <?php
            $x++;
        } ?>
	</div>
</div>
