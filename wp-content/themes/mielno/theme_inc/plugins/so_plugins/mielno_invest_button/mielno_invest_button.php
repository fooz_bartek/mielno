<?php

/*
Widget Name: Mielno
Description:
Author:
*/

class InvestButton_Widget extends SiteOrigin_Widget {

		function __construct() {
            define('SO_PLUGIN_SLUG_InvestButton', 'investbutton-widget');
            define('SO_PLUGIN_DIR_InvestButton', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_invest_button/');
            define('SO_PLUGIN_URL_InvestButton', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_invest_button/');
            define('SO_PLUGIN_VER_InvestButton', '1');

			$form_options = array(
				'bts' => array(
			        'type' => 'repeater',
			        'label' => __( 'Przyciski' , 'engine' ),
			        'item_name'  => __( 'Przycisk', 'engine' ),
			        'item_label' => array(
			            'selector'     => "[id*='opinion_author']",
			            'update_event' => 'change',
			            'value_method' => 'val'
			        ),
			        'fields' => array(
						'bt_background' => array(
							'type' => 'media',
							'label' => __( 'Tło', 'engine' ),
							'choose' => __( 'Wybierz zdjęcie', 'engine' ),
							'update' => __( 'Zapisz zdjęcie', 'engine' ),
							'library' => 'image'
						),
			            'bt_text' => array(
			                'type' => 'text',
			                'label' => __( 'Linia 1', 'engine' ),
			            ),
						'bt_text2' => array(
			                'type' => 'text',
			                'label' => __( 'Linia 2', 'engine' ),
			            ),
			        )
			    ),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'mielno_invest_button',
				__('Sekcja: Zysk, przyciski', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_invest_button/'
				);
		}

        function initialize(){
            $this->register_frontend_scripts(
                array(
                    array(SO_PLUGIN_SLUG_InvestButton, SO_PLUGIN_URL_InvestButton . 'tpl/script.js', array('jquery'), SO_PLUGIN_VER_InvestButton, true)
                )
            );
        }

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('InvestButton Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_invest_button/', 'InvestButton_Widget');
?>
