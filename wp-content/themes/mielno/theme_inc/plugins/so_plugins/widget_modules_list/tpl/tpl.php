<?php
$nazwa_widgetu = "widget_modules_list";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<?php
$c = 0;
foreach ($instance['modules'] as $keyModule => $valueModule) {
    if($valueModule == 1)
        $c++;
}
?>

<div class="widget widget-modules-list so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
    	<div class="row">
    		<div class="col-lg-12"><div class="header styleText24"><?php echo $instance['header']; ?></div></div>
    	</div>
    	<div class="row">
    		<div class="col-lg-12">
    			<div class="modules" data-count="<?php echo $c; ?>">
    			<?php
    			$active = 'active';


                if(sizeof($instance['modules'])> 0)
    			foreach ($instance['modules'] as $keyModule => $valueModule) {
    				if($valueModule == 1){
                        $module = get_page_by_path($keyModule, OBJECT, 'modules');
    					$variable = get_post_meta($module->ID);
    					?>
    					<div class="module <?php echo $active; ?>">
    						<div class="bt">
    							<div class="icon"><img src="<?php bloginfo('template_url'); ?>/img/_tmpModulesIcon.jpg" alt="<?php echo $module->post_title; ?>" /></div>
    							<div class="title"><?php echo $module->post_title; ?></div>
                                <div class="arrow">
                                    <svg x="0px" y="0px" width="17.5px" height="9.5px" viewBox="0 0 17.5 9.5" enable-background="new 0 0 17.5 9.5" xml:space="preserve">
                                        <polygon points="8.75,0 0,0.042 8.75,9.5 17.5,0.042 "/>
                                    </svg>
                                </div>
    						</div>
    						<div class="more">
    							<div class="row">
    								<div class="col-lg-6 col-md-6">
                                        <div class="media-container">
    									<?php
                                        if($variable['presentation_media'][0]) {
                                            $src = wp_get_attachment_url( $variable['presentation_media'][0] );
                                            $src_parts = pathinfo($src);
                                            if($src_parts['extension'] == 'mp4' || $src_parts['extension'] == 'ogg' || $src_parts['extension'] == 'ogv' || $src_parts['extension'] == 'webm'){
                                                echo '<div class="play"><div class="in">';
                                                echo '<svg x="0px" y="0px" width="45.292px" height="45.292px" viewBox="0 0 45.292 45.292" enable-background="new 0 0 45.292 45.292" xml:space="preserve">
                                                <path class="line1" fill="none" stroke-miterlimit="10" d="M18.978,12.261l12.183,9.485L16.917,32.789V15.583"/>
                                                <path class="line2" fill="none" stroke-miterlimit="10" d="M43.729,22.646c0,11.645-9.439,21.084-21.084,21.084 c-11.644,0-21.083-9.439-21.083-21.084c0-11.644,9.439-21.083,21.083-21.083C34.29,1.562,43.729,11.002,43.729,22.646z"/>
                                                </svg>';
                                                echo '<div class="text">Zobacz video</div>';
                                                echo '</div></div>';

                                                echo '<video loop>';
                                                if($src_parts['extension'] == 'mp4')
                                                    echo '<source src="'.$src.'" type="video/mp4"></video>';
                                                if($src_parts['extension'] == 'ogg' || $src_parts['extension'] == 'ogv')
                                                    echo '<source src="'.$src.'" type="video/ogg"></video>';
                                                if($src_parts['extension'] == 'webm')
                                                    echo '<source src="'.$src.'" type="video/webm"></video>';
                                                echo '</video>';
                                            }
                                            if($src_parts['extension'] == 'jpg'){
                                                echo '<img class="cover" src="'.$src.'" />';
                                            }
                                        }
    									?>
                                        </div>
    								</div>
    								<div class="col-lg-6 col-md-6">
    									<div class="title styleText24"><?php echo $variable['presentation_title'][0]; ?></div>
    									<div class="description styleText20"><?php echo $variable['presentation_description'][0]; ?></div>
    								</div>

    							</div>
    						</div>
    					</div>
    					<?php
    					$active = '';
    				}
    			}
    			?>
    			</div>
    		</div>
    	</div>
    </div>
</div>
