clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");
//hoverHandler = ('touchstart' in document.documentElement ? "touchstart" : "hover");

function modulesHeight(){
    hMax = 0;
    $('.widget-modules-list .modules .module').each(function(){
        if(hMax < $(this).find('.more').outerHeight())
            hMax = $(this).find('.more').outerHeight();
    });
    $('.widget-modules-list .modules').css('padding-bottom', hMax+'px');
}
modulesHeight();

$(document).on(clickHandler, '.widget-modules-list .modules .module .bt', function(event) {

    $('.widget-modules-list .modules .module').each(function(){
        if($(this).closest('.module').find('video').length > 0)
            $(this).closest('.module').find('video')[0].pause();
    });
    $('.widget-modules-list .modules .module .media-container').removeClass('on');
    $('.widget-modules-list .modules .module').removeClass('active');
    $(this).closest('.module').addClass('active');
});


$(document).on(clickHandler, '.widget-modules-list .modules .module .media-container .play', function(event) {
    if($(this).closest('.media-container').find('video')[0].paused) {
        $(this).closest('.media-container').find('video')[0].play();
        $(this).closest('.media-container').addClass('on');
    } else {
        $(this).closest('.media-container').find('video')[0].pause();
        $(this).closest('.media-container').removeClass('on');
    }
});


$(window).load(function() {
    modulesHeight();
})
$(window).resize(function() {
    modulesHeight();
})
