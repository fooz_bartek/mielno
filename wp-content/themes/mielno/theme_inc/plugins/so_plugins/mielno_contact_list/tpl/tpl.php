<?php
$nazwa_widgetu = "opinion-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-contact_list so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header"><?php echo $instance['group1_header']; ?></div>
			</div>
		</div>
		<div class="row">
			<div class="group_people">
				<?php foreach ($instance['group1_people'] as $key => $value) { ?>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="people">
							<div class="row">
								<div class="col-xs-5 col-sm-5 col-md-5">
									<div class="photo" style="background-image: url(<?php echo wp_get_attachment_image_src($value['photo'], 'full', true)[0]; ?>)"></div>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7">
									<div class="name"><?php echo $value['name']; ?></div>
									<div class="position"><?php echo $value['position']; ?></div>
									<a class="phone" href="tel:<?php echo $value['phone']; ?>"><?php echo $value['phone']; ?></a>
									<a class="email" href="mailto:<?php echo $value['email']; ?>"><?php echo $value['email']; ?></a>
								</div>
							</div>
						</div>
					</div>
				<?php }	?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="infoline">
					<div class="title">
						<img src="<?php bloginfo('template_url'); ?>/img/contact_icon.png" alt="<?php echo $instance['infoline_number']; ?>" />
						Infolinia
					</div>
					<div class="hours"><?php echo $instance['infoline_hours']; ?></div>
					<a class="number" href="tel:<?php echo $instance['infoline_number']; ?>">tel. <?php echo $instance['infoline_number']; ?></a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="header"><?php echo $instance['group2_header']; ?></div>
			</div>
		</div>
		<div class="row">
			<div class="group_people">
				<?php foreach ($instance['group2_people'] as $key => $value) { ?>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="people">
							<div class="row">
								<div class="col-xs-5 col-sm-5 col-md-5">
									<div class="photo" style="background-image: url(<?php echo wp_get_attachment_image_src($value['photo'], 'full', true)[0]; ?>)"></div>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7">
									<div class="name"><?php echo $value['name']; ?></div>
									<div class="position"><?php echo $value['position']; ?></div>
									<a class="phone" href="tel:<?php echo $value['phone']; ?>"><?php echo $value['phone']; ?></a>
									<a class="email" href="mailto:<?php echo $value['email']; ?>"><?php echo $value['email']; ?></a>
								</div>
							</div>
						</div>
					</div>
				<?php }	?>
			</div>
		</div>
	</div>

	<div class="background" style="background-image: url(<?php	echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>
</div>
