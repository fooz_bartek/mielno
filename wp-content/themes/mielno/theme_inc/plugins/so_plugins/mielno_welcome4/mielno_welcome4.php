<?php

/*
Widget Name: Mielno
Description: Mielno
Author: Bartek Szewczyk
*/


class Welcome4_Widget extends SiteOrigin_Widget {
		function __construct() {
			define('SO_PLUGIN_SLUG', 'welcome4-widget');
	        define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_welcome4/');
	        define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_welcome4/');
	        define('SO_PLUGIN_VER', '1');

			$form_options = array(
				'background' => array(
		    		'type' => 'media',
		       		'label' => __( 'Tło', 'engine' ),
				    'choose' => __( 'Choose image', 'engine' ),
				    'update' => __( 'Set image', 'engine' ),
				    'library' => 'image',
				    'fallback' => true
		    	),
				'detail' => array(
		    		'type' => 'media',
		       		'label' => __( 'Grafika', 'engine' ),
				    'choose' => __( 'Choose image', 'engine' ),
				    'update' => __( 'Set image', 'engine' ),
				    'library' => 'image',
				    'fallback' => true
		    	),
				'header' => array(
			        'type' => 'tinymce',
			        'label' => __( 'Nagłówek', 'engine' ),
			        'rows' => 5,
			        'default_editor' => 'tinymce',
			        'button_filters' => array(
			            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
			            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
			            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
			            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
			            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
			        ),
		    	),
				'text' => array(
					 'type' => 'textarea',
					 'label' => __( 'Tekst: kolumna 1', 'engine' ),
					 'rows' => 5
				),
				'text2' => array(
					 'type' => 'textarea',
					 'label' => __( 'Tekst: kolumna 2', 'engine' ),
					 'rows' => 5
				),

				'button_active' => array(
					'type' => 'checkbox',
					'label' => __( 'Przycisk - aktywacja', 'engine' ),
					'default' => false
				),
				'button_icon' => array(
			        'type' => 'icon',
			        'label' => __('Przycisk - ikona', 'engine'),
			    ),
				'button_text' => array(
					 'type' => 'text',
					 'label' => __( 'Przycisk - tekst', 'engine' ),
				),
				'button_target' => array(
					'type' => 'checkbox',
				    'label' => __('Otwórz w nowym oknie', 'engine'),
				    'default' => false
				),
				'button_url' => array(
					'type' => 'link',
					'label' => __( 'Przycisk - link do podstrony', 'engine' ),
				)

			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'welcome4-widget',
				__('Top (1 kolumna) - Grafika (lewo), Tekst (prawo)', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_welcome4/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Welcome4 Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_welcome4/', 'Welcome4_Widget');
?>
