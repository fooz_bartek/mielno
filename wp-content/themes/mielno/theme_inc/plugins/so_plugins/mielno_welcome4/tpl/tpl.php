<?php
$nazwa_widgetu = "welcome4-widget";
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>

<div class="widget widget-welcome4 so-wrapper so-panel" id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-2 col-md-2">
				<img class="detail" src="<?php	echo wp_get_attachment_image_src($instance['detail'], 'full', true)[0]; ?>" />
				<img class="detail-hide" src="<?php	echo wp_get_attachment_image_src($instance['detail'], 'full', true)[0]; ?>" />
			</div>
			<div class="col-sm-1 col-md-1 hidden-xs"></div>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<div class="row">
					<div class="col-md-12 col-md-12"><div class="header"><?php echo $instance['header']; ?></div></div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-12 cl1">
						<div class="line"></div>
						<div class="text"><?php echo nl2br($instance['text']); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="background" style="background-image: url(<?php	echo wp_get_attachment_image_src($instance['background'], 'full', true)[0]; ?>)"></div>
</div>
