<?php

/*
Widget Name: Mielno - BigButton
Description: Ozdobny przycisk ze zdjeciem
Author: Bartek Szewczyk
*/


class BigButton_Widget extends SiteOrigin_Widget {
		function __construct() {
			define('SO_PLUGIN_SLUG', 'bigbutton');
			define('SO_PLUGIN_DIR', THEME_PLUGIN_DIRECTORY . 'so_plugins/mielno_bigbutton/');
			define('SO_PLUGIN_URL', THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/mielno_bigbutton/');
			define('SO_PLUGIN_VER', '1');

			$form_options = array(
				'buttons' => array(
			        'type' => 'repeater',
			        'label' => __('Przyciski' , 'engine' ),
			        'item_name'  => __( 'Przycisk', 'engine' ),
			        'item_label' => array(
			            'selector'     => "[id*='title']",
			            'update_event' => 'change',
			            'value_method' => 'val'
			        ),
			        'fields' => array(
						'title' => array(
					       'type' => 'text',
					       'label' => __('Tytuł', 'engine' ),
					    ),
						'subtitle' => array(
					       'type' => 'text',
					       'label' => __('Podtytuł', 'engine' ),
					    ),
						'description' => array(
					       'type' => 'textarea',
					       'label' => __('Opis', 'engine' ),
					       'rows' => 4
					    ),
						'url' => array(
					       'type' => 'link',
					       'label' => __('Link do podstrony', 'engine' ),
					    ),
						'background' => array(
						   'type' => 'media',
						   'label' => __('Tło', 'engine' ),
						   'choose' => __('Wybierz tło', 'engine' ),
						   'update' => __('Ustaw tło', 'engine' ),
						   'library' => 'image'
						),
			        )
			    ),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'bigbutton-widget',
				__('Przyciski duże', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_bigbutton/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('BigButton Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_bigbutton/', 'BigButton_Widget');
?>
