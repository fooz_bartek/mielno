<?php
$nazwa_widgetu = 'bigbutton';
$widgetid = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
if($instance['ustawienia']['animation']){
    $widgetanimationclass = 'animatecss';
    $widgetanimation = ' data-animationcss="'.$instance['ustawienia']['makeanimation'].'" ';
}
$widgetpanelstyle = ' panel-widget-style panel-widget-style-for-' . get_the_ID() . '-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'] . ' ' . $widgetanimationclass;
?>


<div class="widget widget-bigbutton so-wrapper so-panel " id="<?php echo $widgetid; ?>" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	<div class="boxs">
		<?php
		if($instance['buttons'])
		foreach ($instance['buttons'] as $key => $value) {
            $href = $value['url'];
            if(strlen($value['url']) > 0){
                if(strpos($value['url'], 'post:') === false){
                    $href = $value['url'];
                } else {
                    $hreflId = str_replace('post: ','', $value['url']);
                    $href = get_permalink($hreflId);
                }
            }
			?>
				<a class="box" href="<?php echo $href; ?>">
					<div class="in before">
						<div class="title"><?php echo $value['title']; ?></div>
						<div class="subtitle"><?php echo $value['subtitle']; ?></div>
					</div>
					<div class="in after">
						<div class="title"><?php echo $value['title']; ?></div>
						<div class="subtitle"><?php echo $value['subtitle']; ?></div>
						<div class="description">
							<div class="line"></div>
							<?php echo $value['description']; ?>
							<div class="element-bt-arrow">
								Czytaj więcej
								<svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
									<line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
									<line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
									<line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
								</svg>
							</div>
						</div>
					</div>
					<div class="in fake">
						<div class="title"><?php echo $value['title']; ?></div>
						<div class="subtitle"><?php echo $value['subtitle']; ?></div>
						<div class="description">
							<div class="line"></div>
							<?php echo $value['description']; ?>
							<div class="element-bt-arrow">
								Czytaj więcej
								<svg class="arrow" x="0px" y="0px" width="25.341px" height="11.949px" viewBox="0 0 25.341 11.949" enable-background="new 0 0 25.341 11.949" xml:space="preserve">
									<line fill="none" stroke-miterlimit="10" x1="25" y1="5.949" x2="0" y2="5.949"/>
									<line fill="none" stroke-miterlimit="10" x1="19" y1="0.366" x2="25" y2="5.949"/>
									<line fill="none" stroke-miterlimit="10" x1="19" y1="11.637" x2="25" y2="6.054"/>
								</svg>
							</div>
						</div>
					</div>
					<div class="background" style="background-image: url(<?php echo wp_get_attachment_image_src($value['background'], 'full', true)[0]; ?>)"></div>
				</a>
		<?php }	?>
	</div>

	<?php //echo '<pre>',print_r($instance,1),'</pre>'; ?>
</div>
