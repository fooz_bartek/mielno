<?php

/*
Widget Name: Mielno - TextPhoto
Description: Widget dla tekstu i zdjecie obok
Author: Bartek Szewczyk
*/


class TextPhoto_Widget extends SiteOrigin_Widget {

		function __construct() {
			$form_options = array(
				'side' => array(
					'type' => 'radio',
					'label' => __( 'Kierunek', 'widget-form-fields-text-domain' ),
					'default' => 'left',
					'options' => array(
						'left' => __( 'Tekst | Zdjęcie', 'engine' ),
						'right' => __( 'Zdjęcie | Tekst', 'engine' )
					)
				),

				'header' => array(
					 'type' => 'text',
					 'label' => __( 'Nagłówek', 'engine' ),
				),
				'text' => array(
					 'type' => 'textarea',
					 'label' => __( 'Tekst', 'engine' ),
					 'rows' => 5
				),

				'button_active' => array(
					'type' => 'checkbox',
					'label' => __( 'Przycisk - aktywacja', 'engine' ),
					'default' => false
				),
				'button_text' => array(
					 'type' => 'text',
					 'label' => __( 'Przycisk - tekst', 'engine' ),
				),
				'button_url' => array(
					'type' => 'link',
					'label' => __( 'Przycisk - link do podstrony', 'engine' ),
				),

				'picture' => array(
			       'type' => 'media',
			       'label' => __( 'Choose a media thing', 'engine' ),
			       'choose' => __( 'Choose image', 'engine' ),
			       'update' => __( 'Set image', 'engine' ),
			       'library' => 'image'
			    ),
			);
			add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'textphoto-widget',
				__('TextPhoto widget', 'engine'),
				array(
						'description' => __('tekst.', 'engine'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_textphoto/'
				);
		}

		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('TextPhoto Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/mielno_textphoto/', 'TextPhoto_Widget');
?>
