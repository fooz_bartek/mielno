<div class="widget widget-textphoto side-<?php echo $instance['side']; ?>">
	<div class="row table-row">

		<?php if($instance['side'] == 'left'){ ?>
			<div class="col-md-7">
				<div class="header"><?php echo $instance['header']; ?></div>
				<div class="pad">
					<svg class="deco" x="0px" y="0px" width="39.441px" height="8.25px" viewBox="0 0 39.441 8.25" enable-background="new 0 0 39.441 8.25" xml:space="preserve">
						<path fill="none" stroke="#8d9ab5" stroke-miterlimit="10" d="M0,0.589
							c3.943,0,3.943,7.072,7.886,7.072c3.944,0,3.944-7.072,7.888-7.072c3.944,0,3.944,7.072,7.888,7.072
							c3.945,0,3.945-7.072,7.89-7.072s3.945,7.072,7.89,7.072"/>
					</svg>
					<div class="text"><?php echo nl2br($instance['text']); ?></div>
					<?php if($instance['button_active'] == 'active'){ ?>
						<a class="element-bt" href="<?php echo get_permalink($instance['button_text']); ?>"><?php echo $instance['button_text']; ?></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
				<div class="photo" style="background-image: url(<?php	echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
			</div>
		<?php } ?>

		<?php if($instance['side'] == 'right'){ ?>
			<div class="col-md-4">
				<div class="photo" style="background-image: url(<?php	echo wp_get_attachment_image_src($instance['picture'], 'full', true)[0]; ?>)"></div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-7">
				<div class="header"><?php echo $instance['header']; ?></div>
				<div class="pad">
					<svg class="deco" x="0px" y="0px" width="39.441px" height="8.25px" viewBox="0 0 39.441 8.25" enable-background="new 0 0 39.441 8.25" xml:space="preserve">
						<path fill="none" stroke="#8d9ab5" stroke-miterlimit="10" d="M0,0.589
							c3.943,0,3.943,7.072,7.886,7.072c3.944,0,3.944-7.072,7.888-7.072c3.944,0,3.944,7.072,7.888,7.072
							c3.945,0,3.945-7.072,7.89-7.072s3.945,7.072,7.89,7.072"/>
					</svg>
					<div class="text"><?php echo nl2br($instance['text']); ?></div>
					<?php
					if($instance['button_active'] == 'active'){
						$href = '#';
						if(strlen($instance['button_url']) > 0)
							$href = $instance['button_url'];
						 ?>
						<a class="element-bt" href="<?php echo $href; ?>"><?php echo $instance['button_text']; ?></a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

	</div>
	<?php //echo '<pre>',print_r($instance,1),'</pre>'; ?>
</div>
