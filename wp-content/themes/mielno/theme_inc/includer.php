<?php
/*

Inlude specyfic theme function:
	# Wojciech Zdziejowski - main programmer

If you do not feel comfortable in modifying WP leave this file and the entire contents of the function alone.
wojtek@fpweb.pl


*/



define( 'THEME_PLUGIN_DIRECTORY', get_template_directory() . '/theme_inc/plugins/' );
define( 'THEME_PLUGIN_DIRECTORY_URL', get_template_directory_uri() . '/theme_inc/plugins/' );
define( 'THEME_CPT_DIRECTORY', get_template_directory() . '/theme_inc/custom_post_type/' );
define( 'THEME_CPT_DIRECTORY_URL', get_template_directory_uri() . '/theme_inc/custom_post_type/' );
define( 'THEME_SC_DIRECTORY', get_template_directory() . '/theme_inc/shortcodes/' );
define( 'THEME_SC_DIRECTORY_URL', get_template_directory_uri() . '/theme_inc/shortcodes/' );

/*inlude PLUGINS*/
require_once(THEME_PLUGIN_DIRECTORY.'includer.php');
require_once(THEME_CPT_DIRECTORY.'includer.php');
require_once(THEME_SC_DIRECTORY.'includer.php');
?>
