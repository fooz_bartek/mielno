<?php
$u_agent = $_SERVER['HTTP_USER_AGENT'];

if (preg_match('/linux/i', $u_agent)) {
    $platform = 'linux';
} elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
    $platform = 'mac';
} elseif (preg_match('/windows|win32/i', $u_agent)) {
    $platform = 'windows';
}

if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
    $bname = 'ie';
} elseif (preg_match('/Trident/i', $u_agent)) {
    $bname = 'ie';
} elseif (preg_match('/Edge/i', $u_agent)) {
    $bname = 'edge';
} elseif (preg_match('/Chrome/i', $u_agent)) {
    $bname = 'chrome';
} elseif (preg_match('/Safari/i', $u_agent)) {
    $bname = 'safari';
} elseif (preg_match('/Opera/i', $u_agent)) {
    $bname = 'opera';
}


$postId = get_post(get_the_ID());
if ($postId->post_type == 'areas') {
    header('Location: '.get_bloginfo('home').'/oferty-dzialek/');
    die();
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php
        global $page, $paged;

        wp_title('|', true, 'right');

        // Add the blog name.
        bloginfo('name');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page()))
            echo " | $site_description";
        ?></title>

    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/img/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/img/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/img/fav/site.webmanifest">
    <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/img/fav/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta name="format-detection" content="telephone=no">

    <?php wp_head(); ?>

    <!--[if lt IE 9]>
    <script src="<?php echo TEMPL_DIR; ?>/js/html5shiv.min.js" type="text/javascript"></script>
    <script src="<?php echo TEMPL_DIR; ?>/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam:700" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
</head>

<body <?php body_class($bname); ?>>
    <div id="loader-wrapper">
        <img src="<?php bloginfo('template_url'); ?>/img/logo_small.svg" />
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
  <header id="header">
      <div class="container-fluid">
          <div class="row row_seo hidden-xs">
             <?php
              if ( is_active_sidebar( 'Header sidebar' ) ) {
                  dynamic_sidebar( 'Header sidebar' );
              }
             ?>
          </div>
          <div class="row row_nav">
              <div class="col-md-12 links">
                <?php
                $actId = get_the_ID();

                $items = wp_get_nav_menu_items('Menu');
                $itemsFilter = array();
                $itemsFilter['left'] = array();
                $itemsFilter['right'] = array();

                //echo '<pre>',print_r($items,1),'</pre>';

                ob_start();
                include 'wp-content/themes/mielno/img/logo.svg';
                $logo = ob_get_clean();

                ob_start();
                include 'wp-content/themes/mielno/img/logo_small.svg';
                $logoSmall = ob_get_clean();

                // ob_start();
                // include 'wp-content/themes/mielno/img/logo_small_horizontal.svg';
                // $logoSmallHorizontal = ob_get_clean();

                $linkCenter = '';
                $linkCenter .= '<a class="logo big '.$classes.'" href="'.get_bloginfo('home').'" alt="'.$item->title.'">'.$logo.'</a>';
                $linkCenter .= '<a class="logo small '.$classes.'" href="'.get_bloginfo('home').'" alt="'.$item->title.'">'.$logoSmall.'</a>';
                $linkCenter .= '<a class="logo mobile '.$classes.'" href="'.get_bloginfo('home').'" alt="'.$item->title.'"><img src="'.get_bloginfo('template_url').'/img/logo_small_horizontal.svg" /></a>';

                $linkLeft = $linkRight = '';
                $x = 1;


                foreach ($items as $item) {
                    $href = $item->url;

                    $active = '';
                    if($actId == $item->object_id)
                        $active = 'active';

                    foreach ($item->classes as $keyClass => $valClass){
                        if($valClass == 'off')
                            $href = '#';
                    }
                    if($item->classes[0] == 'left'){
                        $linkLeft .= '<a class="link'.$x.' '.$active.'" href="'.$href.'" alt="'.$item->title.'">'.$item->title.'</a>';
                        $x++;
                    }
                    if($item->classes[0] == 'right'){
                        $linkRight .= '<a class="link'.$x.' '.$active.'" href="'.$href.'" alt="'.$item->title.'">'.$item->title.'</a>';
                        $x++;
                    }

                }
                echo '<span class="left">'.$linkLeft.'</span>'.$linkCenter.'<span class="right">'.$linkRight.'</span>';
                ?>
                <div class="toggleMenu">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z"/></svg>
                </div>
              </div>
          </div>
      </div>
  </header>
