<div class="loop"> <!-- loop-post -->
<?php

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('post row'); ?>>
					<div class="col-xs-12">
						<div class="page-title">
							<?php
							if(function_exists('bcn_display')){
								?>
								<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
									<?php
										bcn_display();
									?>
								</div>
								<?php
							}
							?>
							<h1><?php the_title(); ?></h1>
						</div>
						<?php
							if ( has_post_thumbnail() ) {
						?>
								<div class="full_width_element">
								<?php

									$atr = array(
										'class' => "img-responsive text-center"
									);
									the_post_thumbnail('slider', $atr);
								?>
									<div class="full_width_desc">
					          <div class="container">
					            <div class="row">
					              <div class="col-xs-12 col-sm-8 col-lg-7 slick-title">
					                <h2><?php echo $value['tytul']; ?></h2>
					              </div>
					            </div>
						        </div>
									</div>
								</div>
						<?php
							}

							the_content();
						?>

						<!-- Next and previous posts -->
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="row">
									<div class="col-xs-6 col-sm-6">
										<div class="previous_post">
											<?php
											$arrow_left = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
											$previous_link = previous_post_link('%link', 'Poprzedni wpis');
											if (strlen(get_previous_post()->post_title) > 0) {
												echo   $arrow_left . $previous_link;
											}
											?>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6">
										<div class="next_post">
											<?php
											$arrow_right = '<i class="fa fa-arrow-right" aria-hidden="true"></i>';
											$next_link = next_post_link('%link', 'Następny wpis');
											if (strlen(get_next_post()->post_title) > 0) {
												echo  $arrow_right . $next_link;
											}
											?>
										</div>
									</div>
								</div>
								</div>
							</div>

					</div>


				</article>
<?php	} // end while
	}
	else {
	?>
		<div class="row">
			<div class="col-xs-12">
				<h2>
			<?php echo _e( 'Nothing to Show Right Now', 'theme'); ?>
				</h2>
			</div>
		</div>
	<?php
	} // end if
wp_reset_query();
//wp_reset_postdata()
?>
</div> <!-- /.row -->
