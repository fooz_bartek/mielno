<?php
/**

Plik wsadowy pełnego frameworka przygotowanego przez:
	# Wojciech Zdziejowski - główny programista

Jesli nie czujesz się pewnie w modyfikowaniu WP zostaw ten plik oraz całą zawartość engine w spokoju.
wojtek@fingerprintweb.pl

Version: 3.2
Lastmod: 01.08.2017

**/

define( 'ENGINE_DIRECTORY', get_template_directory() . '/engine/' );
define( 'PLUGIN_DIRECTORY', get_template_directory() . '/engine/plugins/' );
define( 'PLUGIN_DIRECTORY_URL', get_template_directory_uri() . '/engine/plugins/' );

/*include Bootstrap NAVWalker*/
require_once('wp_bootstrap_navwalker.php');

/*include PLUGINS*/
require_once(PLUGIN_DIRECTORY.'includer.php');


function my_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'my_theme_add_editor_styles' );


// Use shortcodes in Widgets
add_filter('widget_text', 'do_shortcode');

/*=============================
 * usuń info o aktualizacji
 *=============================
 */
function wphidenag()
{
    remove_action('admin_notices', 'update_nag', 3);
}
add_action('admin_menu', 'wphidenag');

/*=============================
 * add HTML 5 form support
 *=============================
 */
function wpdocs_after_setup_theme()
{
    add_theme_support('html5', array('search-form'));
}
add_action('after_setup_theme', 'wpdocs_after_setup_theme');

?>
