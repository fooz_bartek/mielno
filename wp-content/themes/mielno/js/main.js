var $ = jQuery.noConflict();
var $window = jQuery(window);

var rtime;
var timeout = false;
var delta = 200;
var parallaxrunset = 0;

jQuery(window).resize(function() {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
    }
});

function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        timeout = false;
        fixedTopForMenu();
        height_100();
        roweqheight();
        fullWidthElement();
        tabsheight();
    }
}


$(window).load(function() {
    $('body').addClass('loaded');
});

$(document).ready(function() {

    function scrollEffect() {
        posBody = $('body').scrollTop();
        posHtml = $('html').scrollTop();
        if (posHtml > 100 || posBody > 100)
            $('body').addClass('scrolled')
        else
            $('body').removeClass('scrolled')
    }
    scrollEffect();


    $(window).scroll(function(event) {
        scrollEffect();
    });


    clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");
    $(document).on(clickHandler, '.toggleMenu', function() {
        $('header').toggleClass('open');
    });





    $(document).on(clickHandler, 'header.open', function(event) {
        if(event.target.tagName != 'A'){
            $('header').removeClass('open');
        }
    });





    // function mobileClass() {
    //     if($(window).width() <= 767){
    //         $('body').addClass('xxxxx');
    //     }else {
    //         $('body').removeClass('xxxxx');
    //     }
    // }
    // mobileClass();
    //
    // $(window).resize(function() {
    //     mobileClass();
    // });
    //











    /* animated burger icon mobile*/
    jQuery(".navbar-toggle").on("click", function() {
        jQuery(this).toggleClass("active");
    });

    fixedTopForMenu();
    animationCSS();
    smoothscroll();

    // Bootstrap - on hover show submenu
    toggleNavbarMethod();

    /* Stara funkcja
    jQuery('.dropdown').hover(function(){
      jQuery('.dropdown-toggle', this).trigger('click');
    });
     // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $('.dropdown').on('show.bs.dropdown', function(e){
      $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $('.dropdown').on('hide.bs.dropdown', function(e){
      $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });*/

    // scroll to colapsed element
    jQuery(".panel-default").on('shown.bs.collapse', function() {
        jQuery('html,body').animate({
                scrollTop: $(this).children('.panel-collapse').offset().top - 200
            },
            'slow');
    });

    //Menu mobile hover
    if ($(window).width() < 992) { // ograniczenie dla mobile menu
        //when mouse is on parent
        jQuery(".menu-item-has-children a").hover(function() {
            jQuery(".dropdown-menu").find("a").css("color", "white");
        });
        //when mouse is out
        jQuery(".menu-item-has-children").mouseout(function() {
            jQuery(".dropdown-menu").find("a").css("color", "#606060");
        });
    }
});

jQuery(window).load(function() {
    fixedTopForMenu();
    height_100();
    jQuery(window).trigger('resize');

    /* slick slider page */
    jQuery('.slick-slider-page').slick({
        adaptiveHeight: false,
        autoplay: true,
        autoplaySpeed: 10000,
        lazyLoad: 'progressive',
        slidesToShow: 1,
        infinite: true,
        dots: false,
        arrows: false,
        prevArrow: $('.slick-page-nav-wrapper .prev'),
        nextArrow: $('.slick-page-nav-wrapper .next')
    });
    jQuery('.slick-page-nav-wrapper .prev').click(function() {
        jQuery('.slick-slider').slick('slickPrev');
    })

    jQuery('.slick-page-nav-wrapper .next').click(function() {
        jQuery('.slick-slider').slick('slickNext');
    })






});

function showHiddenPosts(target) {
    jQuery(target).closest('.row').slideUp("slow", function() {
        jQuery(this).next('.row').slideDown("slow");
    });
}

function backToTop() {
    jQuery("html, body").animate({
        scrollTop: 0
    }, 1000);
}

function toggleNavbarMethod() {
    jQuery(function($) {
        if ($(window).width() > 769) { // ograniczenie dla mobile menu
            $('.dropdown').hover(function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

            }, function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
            });
            $('.dropdown > a').click(function() {
                //location.href = this.href;
            });
        }
    });
}

function height_100() {
    var screenheight = jQuery(window).height();
    var docheight = jQuery(document).height();
    jQuery('.height_100').css("min-height", screenheight + "px");
}

function fixedTopForMenu() {
    var getheight = jQuery('header#header').outerHeight();

    if ($(window).width() >= 992) {
        jQuery('#content').css('padding-top', getheight);
    } else {
        jQuery('#content').css('padding-top', '0');
    }
}

/* set same height for all col in row-eq-height*/
function roweqheight() {
    jQuery('.row-eq-height').each(function() {
        jQuery(this).children("[class*=' col']").height('auto');
        if ($(window).width() > 992) {
            var seth = 0;
            jQuery(this).children("[class*=' col']").each(function() {
                newh = jQuery(this).height();
                if (newh > seth) {
                    seth = newh;
                }
            });
            jQuery(this).children("[class*=' col']").height(seth);
        } else {
            jQuery(this).children("[class*=' col']").height('auto');
        }
    });
}
//PLUGINS


//WAYPOINT ANIMATE CSS
function is_touch_device() {
    return 'ontouchstart' in window // works on most browsers
        ||
        'onmsgesturechange' in window; // works on ie10
}

function animationCSS() {
    if (!is_touch_device()) {
        jQuery('*[data-animationcss]').addClass(" animated ");

        /* ================ ANIMATED CONTENT ================ */
        if (jQuery(".animated")[0]) {
            jQuery('.animated').css('opacity', '0');
        }

        /* jQuery('.triggerAnimation').waypoint(function() { */
        jQuery('*[data-animationcss]').waypoint(function() {
            var animation = jQuery(this).attr('data-animationcss');
            jQuery(this).css('opacity', 1);
            jQuery(this).addClass(" animated " + animation);
            //console.log(animation);
        }, {
            offset: '80%',
            triggerOnce: true
        });
    }
}

/* full screen width element */
function fullWidthElement() {
    var wrapper = jQuery('body');
    jQuery('.full_width_element').each(function() {
        jQuery(this).css({
            "margin-left": 0,
            "margin-right": 0,
            "padding-left": 0,
            "padding-right": 0
        })
        var left = jQuery(this).offset().left;
        var right = jQuery(this).offset().right;
        jQuery(this).css({
            "margin-left": -left,
            "margin-right": -left
        })
        if (jQuery('.slick-slider-page').length) {
            jQuery('.slick-slider-page').slick('reinit');
        }
    });

}

function tabsheight() {
    var maxHeight = 0;
    $(".tab-content").height('auto');
    $('.tab-content .tab-pane.active').data('tabactiv', 1);
    $(".tab-content .tab-pane").each(function() {
        $(this).addClass("active");
        var height = $(this).height();
        maxHeight = height > maxHeight ? height : maxHeight;
        $(this).removeClass("active");
    });

    if ($(".tab-content .tab-pane.in").length) {
        $(".tab-content .tab-pane.in").addClass("active").data('tabactiv', '0');
    } else {
        $(".tab-content .tab-pane:first").addClass("active");
    }
    $(".tab-content").height(maxHeight);
}

// Copy to clipboard
// document.getElementById("copyButton").addEventListener("click", function() {
//     copyToClipboard(document.getElementById("copyTarget"));
// });

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}






/* smootscroll special */
/*function smoothscroll() {
    function o() {
        b.keyboardSupport && H("keydown", w)
    }

    function p() {
        if (!f && document.body) {
            f = !0;
            var a = document.body,
                e = document.documentElement,
                k = window.innerHeight,
                l = a.scrollHeight;
            if (g = document.compatMode.indexOf("CSS") >= 0 ? e : a, h = a, o(), top != self) d = !0;
            else if (l > k && (a.offsetHeight <= k || e.offsetHeight <= k)) {
                var m = document.createElement("div");
                m.style.cssText = "position:absolute; z-index:-10000; top:0; left:0; right:0; height:" + g.scrollHeight + "px", document.body.appendChild(m);
                var n;
                j = function () {
                    n || (n = setTimeout(function () {
                        c || (m.style.height = "0", m.style.height = g.scrollHeight + "px", n = null)
                    }, 500))
                }, setTimeout(j, 10), H("resize", j);
                var p = {
                    attributes: !0,
                    childList: !0,
                    characterData: !1
                };
                if (i = new R(j), i.observe(a, p), g.offsetHeight <= k) {
                    var q = document.createElement("div");
                    q.style.clear = "both", a.appendChild(q)
                }
            }
            b.fixedBackground || c || (a.style.backgroundAttachment = "scroll", e.style.backgroundAttachment = "scroll")
        }
    }

    function q() {
        i && i.disconnect(), I(Y, v), I("mousedown", x), I("keydown", w), I("resize", j), I("load", p)
    }

    function u(a, c, d) {
        if (K(c, d), 1 != b.accelerationMax) {
            var e = Date.now(),
                f = e - t;
            if (f < b.accelerationDelta) {
                var g = (1 + 50 / f) / 2;
                g > 1 && (g = Math.min(g, b.accelerationMax), c *= g, d *= g)
            }
            t = Date.now()
        }
        if (r.push({
                x: c,
                y: d,
                lastX: 0 > c ? .99 : -.99,
                lastY: 0 > d ? .99 : -.99,
                start: Date.now()
            }), !s) {
            var h = a === document.body,
                i = function (e) {
                    for (var f = Date.now(), g = 0, j = 0, k = 0; k < r.length; k++) {
                        var l = r[k],
                            m = f - l.start,
                            n = m >= b.animationTime,
                            o = n ? 1 : m / b.animationTime;
                        b.pulseAlgorithm && (o = U(o));
                        var p = l.x * o - l.lastX >> 0,
                            q = l.y * o - l.lastY >> 0;
                        g += p, j += q, l.lastX += p, l.lastY += q, n && (r.splice(k, 1), k--)
                    }
                    h ? window.scrollBy(g, j) : (g && (a.scrollLeft += g), j && (a.scrollTop += j)), c || d || (r = []), r.length ? Q(i, a, 1e3 / b.frameRate + 1) : s = !1
                };
            Q(i, a, 0), s = !0
        }
    }

    function v(a) {
        f || p();
        var c = a.target,
            d = D(c);
        if (!d || a.defaultPrevented || a.ctrlKey) return !0;
        if (J(h, "embed") || J(c, "embed") && /\.pdf/i.test(c.src) || J(h, "object") || c.shadowRoot) return !0;
        var e = -a.wheelDeltaX || a.deltaX || 0,
            g = -a.wheelDeltaY || a.deltaY || 0;
        return l && (a.wheelDeltaX && N(a.wheelDeltaX, 120) && (e = -120 * (a.wheelDeltaX / Math.abs(a.wheelDeltaX))), a.wheelDeltaY && N(a.wheelDeltaY, 120) && (g = -120 * (a.wheelDeltaY / Math.abs(a.wheelDeltaY)))), e || g || (g = -a.wheelDelta || 0), 1 === a.deltaMode && (e *= 40, g *= 40), !b.touchpadSupport && M(g) ? !0 : (Math.abs(e) > 1.2 && (e *= b.stepSize / 120), Math.abs(g) > 1.2 && (g *= b.stepSize / 120), u(d, e, g), a.preventDefault(), void B())
    }

    function w(a) {
        var c = a.target,
            d = a.ctrlKey || a.altKey || a.metaKey || a.shiftKey && a.keyCode !== m.spacebar;
        document.body.contains(h) || (h = document.activeElement);
        var e = /^(textarea|select|embed|object)$/i,
            f = /^(button|submit|radio|checkbox|file|color|image)$/i;
        if (a.defaultPrevented || e.test(c.nodeName) || J(c, "input") && !f.test(c.type) || J(h, "video") || P(a) || c.isContentEditable || d) return !0;
        if ((J(c, "button") || J(c, "input") && f.test(c.type)) && a.keyCode === m.spacebar) return !0;
        if (J(c, "input") && "radio" == c.type && n[a.keyCode]) return !0;
        var g, i = 0,
            j = 0,
            k = D(h),
            l = k.clientHeight;
        switch (k == document.body && (l = window.innerHeight), a.keyCode) {
            case m.up:
                j = -b.arrowScroll;
                break;
            case m.down:
                j = b.arrowScroll;
                break;
            case m.spacebar:
                g = a.shiftKey ? 1 : -1, j = -g * l * .9;
                break;
            case m.pageup:
                j = .9 * -l;
                break;
            case m.pagedown:
                j = .9 * l;
                break;
            case m.home:
                j = -k.scrollTop;
                break;
            case m.end:
                var o = k.scrollHeight - k.scrollTop - l;
                j = o > 0 ? o + 10 : 0;
                break;
            case m.left:
                i = -b.arrowScroll;
                break;
            case m.right:
                i = b.arrowScroll;
                break;
            default:
                return !0
        }
        u(k, i, j), a.preventDefault(), B()
    }

    function x(a) {
        h = a.target
    }

    function B() {
        clearTimeout(A), A = setInterval(function () {
            z = {}
        }, 1e3)
    }

    function C(a, b) {
        for (var c = a.length; c--;) z[y(a[c])] = b;
        return b
    }

    function D(a) {
        var b = [],
            c = document.body,
            e = g.scrollHeight;
        do {
            var f = z[y(a)];
            if (f) return C(b, f);
            if (b.push(a), e === a.scrollHeight) {
                var h = F(g) && F(c),
                    i = h || G(g);
                if (d && E(g) || !d && i) return C(b, S())
            } else if (E(a) && G(a)) return C(b, a)
        } while (a = a.parentElement)
    }

    function E(a) {
        return a.clientHeight + 10 < a.scrollHeight
    }

    function F(a) {
        var b = getComputedStyle(a, "").getPropertyValue("overflow-y");
        return "hidden" !== b
    }

    function G(a) {
        var b = getComputedStyle(a, "").getPropertyValue("overflow-y");
        return "scroll" === b || "auto" === b
    }

    function H(a, b) {
        window.addEventListener(a, b, !1)
    }

    function I(a, b) {
        window.removeEventListener(a, b, !1)
    }

    function J(a, b) {
        return (a.nodeName || "").toLowerCase() === b.toLowerCase()
    }

    function K(a, b) {
        a = a > 0 ? 1 : -1, b = b > 0 ? 1 : -1, (e.x !== a || e.y !== b) && (e.x = a, e.y = b, r = [], t = 0)
    }

    function M(a) {
        return a ? (k.length || (k = [a, a, a]), a = Math.abs(a), k.push(a), k.shift(), clearTimeout(L), L = setTimeout(function () {
            window.localStorage && (localStorage.SS_deltaBuffer = k.join(","))
        }, 1e3), !O(120) && !O(100)) : void 0
    }

    function N(a, b) {
        return Math.floor(a / b) == a / b
    }

    function O(a) {
        return N(k[0], a) && N(k[1], a) && N(k[2], a)
    }

    function P(a) {
        var b = a.target,
            c = !1;
        if (-1 != document.URL.indexOf("www.youtube.com/watch"))
            do
                if (c = b.classList && b.classList.contains("html5-video-controls")) break;
            while (b = b.parentNode);
        return c
    }

    function T(a) {
        var c, d, e;
        return a *= b.pulseScale, 1 > a ? c = a - (1 - Math.exp(-a)) : (d = Math.exp(-1), a -= 1, e = 1 - Math.exp(-a), c = d + e * (1 - d)), c * b.pulseNormalize
    }

    function U(a) {
        return a >= 1 ? 1 : 0 >= a ? 0 : (1 == b.pulseNormalize && (b.pulseNormalize /= T(1)), T(a))
    }

    function Z(c) {
        for (var d in c) a.hasOwnProperty(d) && (b[d] = c[d])
    }

    var h, i, j, A, L, a = {
            frameRate: 150,
            animationTime: 500,
            stepSize: 130,
            pulseAlgorithm: !0,
            pulseScale: 4,
            pulseNormalize: 1,
            accelerationDelta: 50,
            accelerationMax: 3,
            keyboardSupport: !0,
            arrowScroll: 50,
            touchpadSupport: !1,
            fixedBackground: !0,
            excluded: ""
        },
        b = a,
        c = !1,
        d = !1,
        e = {
            x: 0,
            y: 0
        },
        f = !1,
        g = document.documentElement,
        k = [],
        l = /^Mac/.test(navigator.platform),
        m = {
            left: 37,
            up: 38,
            right: 39,
            down: 40,
            spacebar: 32,
            pageup: 33,
            pagedown: 34,
            end: 35,
            home: 36
        },
        n = {
            37: 1,
            38: 1,
            39: 1,
            40: 1
        },
        r = [],
        s = !1,
        t = Date.now(),
        y = function () {
            var a = 0;
            return function (b) {
                return b.uniqueID || (b.uniqueID = a++)
            }
        }(),
        z = {};
    window.localStorage && localStorage.SS_deltaBuffer && (k = localStorage.SS_deltaBuffer.split(","));
    var Y, Q = function () {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (a, b, c) {
                    window.setTimeout(a, c || 1e3 / 60)
                }
        }(),
        R = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
        S = function () {
            var a;
            return function () {
                if (!a) {
                    var b = document.createElement("div");
                    b.style.cssText = "height:10000px;width:1px;", document.body.appendChild(b);
                    var c = document.body.scrollTop;
                    document.documentElement.scrollTop;
                    window.scrollBy(0, 3), a = document.body.scrollTop != c ? document.body : document.documentElement, window.scrollBy(0, -3), document.body.removeChild(b)
                }
                return a
            }
        }(),
        V = window.navigator.userAgent,
        W = /mobile/i.test(V),
        X = !W;
    "onwheel" in document.createElement("div") ? Y = "wheel" : "onmousewheel" in document.createElement("div") && (Y = "mousewheel"), Y && X && (H(Y, v), H("mousedown", x), H("load", p)), Z.destroy = q, window.SmoothScrollOptions && Z(window.SmoothScrollOptions), "function" == typeof define && define.amd ? define(function () {
        return Z
    }) : "object" == typeof exports ? module.exports = Z : window.SmoothScroll = Z
}*/



/**
 * smootscroll special
 * use smoothscroll() in doc ready
 * https://wordpress.org/plugins/mousewheel-smooth-scroll/#developers
 */

function smoothscroll() {
    var D, M, T, E, C, H = {
        frameRate: 150,
        animationTime: 500,
        stepSize: 130,
        pulseAlgorithm: !0,
        pulseScale: 4,
        pulseNormalize: 1,
        accelerationDelta: 50,
        accelerationMax: 3,
        keyboardSupport: !0,
        arrowScroll: 1,
        fixedBackground: !0,
        excluded: ""
    }

    function e() {
        z.keyboardSupport && m("keydown", a)
    }

    function t() {
        if (!Y && document.body) {
            Y = !0;
            var t = document.body,
                o = document.documentElement,
                n = window.innerHeight,
                r = t.scrollHeight;
            if (A = document.compatMode.indexOf("CSS") >= 0 ? o : t, D = t, e(), top != self) O = !0;
            else if (te && r > n && (t.offsetHeight <= n || o.offsetHeight <= n)) {
                var a, i = document.createElement("div");
                i.style.cssText = "position:absolute; z-index:-10000; top:0; left:0; right:0; height:" + A.scrollHeight + "px", document.body.appendChild(i), T = function() {
                    a || (a = setTimeout(function() {
                        L || (i.style.height = "0", i.style.height = A.scrollHeight + "px", a = null)
                    }, 500))
                }, setTimeout(T, 10), m("resize", T);
                if (M = new W(T), M.observe(t, {
                        attributes: !0,
                        childList: !0,
                        characterData: !1
                    }), A.offsetHeight <= n) {
                    var l = document.createElement("div");
                    l.style.clear = "both", t.appendChild(l)
                }
            }
            z.fixedBackground || L || (t.style.backgroundAttachment = "scroll", o.style.backgroundAttachment = "scroll")
        }
    }

    function o() {
        M && M.disconnect(), w(I, r), w("mousedown", i), w("keydown", a), w("resize", T), w("load", t)
    }

    function n(e, t, o) {
        if (p(t, o), 1 != z.accelerationMax) {
            var n = Date.now() - q;
            if (n < z.accelerationDelta) {
                var r = (1 + 50 / n) / 2;
                r > 1 && (r = Math.min(r, z.accelerationMax), t *= r, o *= r)
            }
            q = Date.now()
        }
        if (R.push({
                x: t,
                y: o,
                lastX: t < 0 ? .99 : -.99,
                lastY: o < 0 ? .99 : -.99,
                start: Date.now()
            }), !j) {
            var a = e === document.body,
                i = function(n) {
                    for (var r = Date.now(), l = 0, c = 0, u = 0; u < R.length; u++) {
                        var d = R[u],
                            s = r - d.start,
                            f = s >= z.animationTime,
                            m = f ? 1 : s / z.animationTime;
                        z.pulseAlgorithm && (m = x(m));
                        var h = d.x * m - d.lastX >> 0,
                            w = d.y * m - d.lastY >> 0;
                        l += h, c += w, d.lastX += h, d.lastY += w, f && (R.splice(u, 1), u--)
                    }
                    a ? window.scrollBy(l, c) : (l && (e.scrollLeft += l), c && (e.scrollTop += c)), t || o || (R = []), R.length ? _(i, e, 1e3 / z.frameRate + 1) : j = !1
                };
            _(i, e, 0), j = !0
        }
    }

    function r(e) {
        Y || t();
        var o = e.target;
        if (e.defaultPrevented || e.ctrlKey) return !0;
        if (h(D, "embed") || h(o, "embed") && /\.pdf/i.test(o.src) || h(D, "object") || o.shadowRoot) return !0;
        var r = -e.wheelDeltaX || e.deltaX || 0,
            a = -e.wheelDeltaY || e.deltaY || 0;
        N && (e.wheelDeltaX && y(e.wheelDeltaX, 120) && (r = e.wheelDeltaX / Math.abs(e.wheelDeltaX) * -120), e.wheelDeltaY && y(e.wheelDeltaY, 120) && (a = e.wheelDeltaY / Math.abs(e.wheelDeltaY) * -120)), r || a || (a = -e.wheelDelta || 0), 1 === e.deltaMode && (r *= 40, a *= 40);
        var i = u(o);
        return i ? !!v(a) || (Math.abs(r) > 1.2 && (r *= z.stepSize / 120), Math.abs(a) > 1.2 && (a *= z.stepSize / 120), n(i, r, a), e.preventDefault(), void l()) : !O || !J || (Object.defineProperty(e, "target", {
            value: window.frameElement
        }), parent.wheel(e))
    }

    function a(e) {
        var t = e.target,
            o = e.ctrlKey || e.altKey || e.metaKey || e.shiftKey && e.keyCode !== K.spacebar;
        document.body.contains(D) || (D = document.activeElement);
        var r = /^(button|submit|radio|checkbox|file|color|image)$/i;
        if (e.defaultPrevented || /^(textarea|select|embed|object)$/i.test(t.nodeName) || h(t, "input") && !r.test(t.type) || h(D, "video") || g(e) || t.isContentEditable || o) return !0;
        if ((h(t, "button") || h(t, "input") && r.test(t.type)) && e.keyCode === K.spacebar) return !0;
        if (h(t, "input") && "radio" == t.type && P[e.keyCode]) return !0;
        var a = 0,
            i = 0,
            c = u(D);
        if (!c) return !O || !J || parent.keydown(e);
        var d = c.clientHeight;
        switch (c == document.body && (d = window.innerHeight), e.keyCode) {
            case K.up:
                i = -z.arrowScroll;
                break;
            case K.down:
                i = z.arrowScroll;
                break;
            case K.spacebar:
                i = -(e.shiftKey ? 1 : -1) * d * .9;
                break;
            case K.pageup:
                i = .9 * -d;
                break;
            case K.pagedown:
                i = .9 * d;
                break;
            case K.home:
                i = -c.scrollTop;
                break;
            case K.end:
                var s = c.scrollHeight - c.scrollTop - d;
                i = s > 0 ? s + 10 : 0;
                break;
            case K.left:
                a = -z.arrowScroll;
                break;
            case K.right:
                a = z.arrowScroll;
                break;
            default:
                return !0
        }
        n(c, a, i), e.preventDefault(), l()
    }

    function i(e) {
        D = e.target
    }

    function l() {
        clearTimeout(E), E = setInterval(function() {
            F = {}
        }, 1e3)
    }

    function c(e, t) {
        for (var o = e.length; o--;) F[V(e[o])] = t;
        return t
    }

    function u(e) {
        var t = [],
            o = document.body,
            n = A.scrollHeight;
        do {
            var r = F[V(e)];
            if (r) return c(t, r);
            if (t.push(e), n === e.scrollHeight) {
                var a = s(A) && s(o) || f(A);
                if (O && d(A) || !O && a) return c(t, $())
            } else if (d(e) && f(e)) return c(t, e)
        } while (e = e.parentElement)
    }

    function d(e) {
        return e.clientHeight + 10 < e.scrollHeight
    }

    function s(e) {
        return "hidden" !== getComputedStyle(e, "").getPropertyValue("overflow-y")
    }

    function f(e) {
        var t = getComputedStyle(e, "").getPropertyValue("overflow-y");
        return "scroll" === t || "auto" === t
    }

    function m(e, t) {
        window.addEventListener(e, t, !1)
    }

    function w(e, t) {
        window.removeEventListener(e, t, !1)
    }

    function h(e, t) {
        return (e.nodeName || "").toLowerCase() === t.toLowerCase()
    }

    function p(e, t) {
        e = e > 0 ? 1 : -1, t = t > 0 ? 1 : -1, X.x === e && X.y === t || (X.x = e, X.y = t, R = [], q = 0)
    }

    function v(e) {
        if (e) return B.length || (B = [e, e, e]), e = Math.abs(e), B.push(e), B.shift(), clearTimeout(C), C = setTimeout(function() {
            try {
                localStorage.SS_deltaBuffer = B.join(",")
            } catch (e) {}
        }, 1e3), !b(120) && !b(100)
    }

    function y(e, t) {
        return Math.floor(e / t) == e / t
    }

    function b(e) {
        return y(B[0], e) && y(B[1], e) && y(B[2], e)
    }

    function g(e) {
        var t = e.target,
            o = !1;
        if (-1 != document.URL.indexOf("www.youtube.com/watch"))
            do {
                if (o = t.classList && t.classList.contains("html5-video-controls")) break
            } while (t = t.parentNode);
        return o
    }

    function S(e) {
        var t, o;
        return (e *= z.pulseScale) < 1 ? t = e - (1 - Math.exp(-e)) : (e -= 1, t = (o = Math.exp(-1)) + (1 - Math.exp(-e)) * (1 - o)), t * z.pulseNormalize
    }

    function x(e) {
        return e >= 1 ? 1 : e <= 0 ? 0 : (1 == z.pulseNormalize && (z.pulseNormalize /= S(1)), S(e))
    }

    function k(e) {
        for (var t in e) H.hasOwnProperty(t) && (z[t] = e[t])
    }
    var z = H,
        L = !1,
        O = !1,
        X = {
            x: 0,
            y: 0
        },
        Y = !1,
        A = document.documentElement,
        B = [],
        N = /^Mac/.test(navigator.platform),
        K = {
            left: 37,
            up: 38,
            right: 39,
            down: 40,
            spacebar: 32,
            pageup: 33,
            pagedown: 34,
            end: 35,
            home: 36
        },
        P = {
            37: 1,
            38: 1,
            39: 1,
            40: 1
        },
        R = [],
        j = !1,
        q = Date.now(),
        V = function() {
            var e = 0;
            return function(t) {
                return t.uniqueID || (t.uniqueID = e++)
            }
        }(),
        F = {};
    if (window.localStorage && localStorage.SS_deltaBuffer) try {
        B = localStorage.SS_deltaBuffer.split(",")
    } catch (e) {}
    var I, _ = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(e, t, o) {
            window.setTimeout(e, o || 1e3 / 60)
        },
        W = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
        $ = function() {
            var e;
            return function() {
                if (!e) {
                    var t = document.createElement("div");
                    t.style.cssText = "height:10000px;width:1px;", document.body.appendChild(t);
                    var o = document.body.scrollTop;
                    document.documentElement.scrollTop, window.scrollBy(0, 3), e = document.body.scrollTop != o ? document.body : document.documentElement, window.scrollBy(0, -3), document.body.removeChild(t)
                }
                return e
            }
        }(),
        U = window.navigator.userAgent,
        G = /Edge/.test(U),
        J = /chrome/i.test(U) && !G,
        Q = /safari/i.test(U) && !G,
        Z = /mobile/i.test(U),
        ee = /Windows NT 6.1/i.test(U) && /rv:11/i.test(U),
        te = Q && (/Version\/8/i.test(U) || /Version\/9/i.test(U)),
        oe = (J || Q || ee) && !Z;
    "onwheel" in document.createElement("div") ? I = "wheel" : "onmousewheel" in document.createElement("div") && (I = "mousewheel"), I && oe && (m(I, r), m("mousedown", i), m("load", t)), k.destroy = o, window.SmoothScrollOptions && k(window.SmoothScrollOptions), "function" == typeof define && define.amd ? define(function() {
        return k
    }) : "object" == typeof exports ? module.exports = k : window.SmoothScroll = k;
}
