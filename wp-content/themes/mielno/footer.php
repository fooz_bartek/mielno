	<footer id="footer">
		<?php
		$variable = get_post_meta(get_the_ID());
		if(is_active_sidebar('footer-sidebar') && $variable['footer_form_visible'][0] == 1) {
		?>
			<section class="form">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'footer-sidebar' ); ?>
					</div>
				</div>
			</section>
		<?php }	?>

		<?php
		if(is_active_sidebar('footer-sidebar-info')) {
		?>
			<section class="info">
				<?php dynamic_sidebar('footer-sidebar-info'); ?>
			</section>
		<?php }	?>
	</footer>
	<?php wp_footer(); ?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
</body>
</html>
