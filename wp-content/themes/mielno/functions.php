<?php
/**
Template base on FPWEB framework
# Wojciech Zdziejowski - main programmer

If you do not feel comfortable in modifying WP leave this file and the entire contents of the function alone.

Version: 2
Lastmod: 01.08.2017

**/

/*=============================
 * GLOBALNIE
 *=============================
 */
define('TEMPL_NAME', 'funktional');
define('TEMPL_OWNER_URL', 'https://www.funktional.pl/');
define('TEMPL_AUTHOR', 'Funktional');

define('TEMPL_DIR', get_template_directory_uri() . '/');

require_once('engine/includer.php');
require_once('theme_inc/includer.php');

/*=============================
 * SKRYPTY I STYLE
 *=============================
 */
if (!is_admin()) {

//style
    wp_register_style('bootstrap', TEMPL_DIR . 'css/bootstrap.css');
    wp_enqueue_style('bootstrap');
    wp_register_style('slick', TEMPL_DIR . 'js/slick-1.6.0/slick/slick.css');
    wp_enqueue_style('slick');
    wp_register_style('slick-theme', TEMPL_DIR . 'js/slick-1.6.0/slick/slick-theme.css');
    wp_enqueue_style('slick-theme');
    wp_register_style('animatecss', TEMPL_DIR . 'js/waypoints-animate/animate.css');
    wp_enqueue_style('animatecss');

    wp_register_style('style-page', TEMPL_DIR . 'stylePage.css');
    wp_enqueue_style('style-page');

    wp_register_style(TEMPL_NAME, TEMPL_DIR . 'style.css');
    wp_enqueue_style(TEMPL_NAME);

//script
    wp_enqueue_script('jquery');

    wp_enqueue_script('bootstrap', TEMPL_DIR . 'js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('slick', TEMPL_DIR . 'js/slick-1.6.0/slick/slick.min.js', array('jquery'));
    wp_enqueue_script('waypoint', TEMPL_DIR . 'js/waypoints-animate/jquery.waypoints.min.js', array('jquery'));
    wp_enqueue_script('maphilight', TEMPL_DIR . 'js/jquery.maphilight.js', array('jquery'));
    wp_enqueue_script('svg-convert', TEMPL_DIR . 'js/svg-convert.js', array('jquery'));
    wp_enqueue_script('isotope', TEMPL_DIR . 'js/isotope.pkgd.js', array('jquery'));

    wp_enqueue_script('jquery.dataTables', 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js', array('jquery'));
    wp_enqueue_script('dataTables.bootstrap', 'https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js', array('jquery'));





    // wp_register_script('custom-js',get_stylesheet_directory_uri().'/js/custom.js',array(),NULL,true);
    // wp_enqueue_script('custom-js');
    //
    // $wnm_custom = array( 'stylesheet_directory_uri' => get_stylesheet_directory_uri() );
    // wp_localize_script( 'custom-js', 'directory_uri', $wnm_custom );



    wp_enqueue_script('mainjs', TEMPL_DIR . 'js/main.js');


    /* We add some JavaScript to pages with the comment form
     * to support sites with threaded comments (when in use).
     */
    if (is_singular() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');

}

/*
 * =====================
 *       NAVMENU
 * =====================
 */
register_nav_menu('menu', 'Główne menu');
if (!function_exists('fpweb_page_menu')) {
    function fpweb_page_menu($args = array())
    {
        //Add bootstrap classes
        $echo = $args['echo'];
        $args['echo'] = 0;
        $args['menu_class'] = $args['menu_class'] . ' main-menu navbar collapse navbar-collapse';
        $string = wp_page_menu($args);
        //ul
        $string = str_replace('<ul>', '<ul class="nav navbar-nav">', $string);
        //li dropdown
        $string = str_replace('page_item_has_children', 'dropdown page_item_has_children', $string);
        //li a dropdown-toggle
        $string = str_replace('page_item_has_children"><a ', 'page_item_has_children"><a class="dropdown-toggle" data-toggle="dropdown" ', $string);
        // koniec a w menu
        $string = str_replace('</a><ul', ' <b class="caret"></b></a><ul', $string);
        //ul children
        $string = str_replace("class='children", "class='children dropdown-menu", $string);


        if ($echo)
            echo $string;
        else
            return $string;

    }
} //end if

// Widgets menu
function wpb_custom_new_menu() {
	register_nav_menu('my-custom-menu',__( 'Widgets menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );
/*
 *=============================
 * SIDEBAR
 *=============================
 */

$argshead = array(
    'name' => sprintf(__('Header sidebar', 'engine'), $i),
    'id' => "header-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);

register_sidebar($argshead);

$argsfooter = array(
    'name' => sprintf(__('Footer sidebar', 'engine'), $i),
    'id' => "footer-sidebar",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argsfooter);

$argsfooter = array(
    'name' => sprintf(__('Footer sidebar info', 'engine'), $i),
    'id' => "footer-sidebar-info",
    'description' => '',
    'class' => 'sidebar-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-header  col-xs-12 %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '',
    'after_title' => "\n",
);
register_sidebar($argsfooter);

// $argsp = array(
//     'name' => sprintf(__('Right sidebar', 'engine'), $i),
//     'id' => "right-sidebar",
//     'description' => '',
//     'class' => 'sidebar-wrapper',
//     'before_widget' => '<div id="%1$s" class="widget %2$s">',
//     'after_widget' => "</div>\n",
//     'before_title' => '<h4 class="widget-title">',
//     'after_title' => "</h4>\n",
// );
// register_sidebar($argsp);

// $argsl = array(
//     'name' => sprintf(__('Left sidebar', 'engine'), $i),
//     'id' => "left-sidebar",
//     'description' => '',
//     'class' => 'sidebar-wrapper',
//     'before_widget' => '<div id="%1$s" class="widget %2$s">',
//     'after_widget' => "</div>\n",
//     'before_title' => '<h4 class="widget-title">',
//     'after_title' => "</h4>\n",
// );
// register_sidebar($argsl);

$argsf1 = array(
    'name' => sprintf(__('Footer 1 col', 'engine'), $i),
    'id' => "footer-sidebar1",
    'description' => '',
    'class' => 'footer-sidebar-wrapper',
    'before_widget' => '<div id="%1$s" class="widget-footer %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '<h4 class="widget-title">',
    'after_title' => "</h4>\n",
);

register_sidebar($argsf1);

$argsf2 = array(
    'name' => sprintf(__('Footer 2 col', 'engine'), $i),
    'id' => "footer-sidebar2",
    'description' => '',
    'class' => 'footer-sidebar-wrapper',
    'before_widget' => '<div id="%1$s" class="widget-footer %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '<h4 class="widget-title">',
    'after_title' => "</h4>\n",
);

register_sidebar($argsf2);

$argsf3 = array(
    'name' => sprintf(__('Footer 3 col', 'engine'), $i),
    'id' => "footer-sidebar3",
    'description' => '',
    'class' => 'footer-sidebar-wrapper row',
    'before_widget' => '<div id="%1$s" class="widget-footer %2$s">',
    'after_widget' => "</div>\n",
    'before_title' => '<h4 class="widget-title">',
    'after_title' => "</h4>\n",
);

register_sidebar($argsf3);



/*
 *=============================
 *       OBRAZKI
 *=============================
 */

add_theme_support('post-thumbnails');
set_post_thumbnail_size(1140, 500, true); //set proper thumbnail size


update_option('thumbnail_size_w', 375);
update_option('thumbnail_size_h', 228);
update_option('thumbnail_crop', 1);
update_option('medium_size_w', 570);
update_option('medium_size_h', 3000);
update_option('medium_crop', 0);
update_option('large_size_w', 1140);
update_option('large_size_h', 3000);
update_option('large_crop', 0);

add_image_size('post-thumbnails', 1920, 400, true);
add_image_size('slider', 1920, 800, true);
add_image_size('zespol-thumbnail', 150, 150, true);

function fpweb_get_image_id($image_url)
{
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url));
    return $attachment[0];
}

/*
 *=============================
 * OBSŁUGA TŁUMACZEŃ
 *=============================
 */

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup()
{
    load_theme_textdomain('theme', get_template_directory() . '/languages');
    load_theme_textdomain('engine', get_template_directory() . '/languages/framework/');
}

/*=============================
 * OPCJE SZABLONU
 *=============================
 */

function theme_customize_register( $wp_customize ) {
    //All our sections, settings, and controls will be added here
    //Adding a section
    $wp_customize->add_section(
        TEMPL_NAME,
        array(
            'title' => TEMPL_AUTHOR,
            'description' => 'Set custom data for this theme',
            'priority' => 9999,
        )
    );

    //Add a setting
    $wp_customize->add_setting(
        'theme_logo'
    );


    //Add control
    $wp_customize->add_control(
        new WP_Customize_Image_Control( $wp_customize, 'theme_logo', array(
            'label'    => __( 'Logo', 'engine' ),
            'section'  => TEMPL_NAME,
            'settings' => 'theme_logo',
        ) )
    );
    //Add a setting
    $wp_customize->add_setting(
        'theme_logo_svg'
    );
    //Add control
    $wp_customize->add_control(
        new WP_Customize_Image_Control( $wp_customize, 'theme_logo_svg', array(
            'label'    => __( 'Logo SVG', 'engine' ),
            'section'  => TEMPL_NAME,
            'settings' => 'theme_logo_svg',
        ) )
    );

    //Add a setting
    $wp_customize->add_setting(
        'footer_text'
    );

    //Add control
    $wp_customize->add_control(
        'footer_text',
        array(
            'label' => __('Footer text','engine'),
            'section' => TEMPL_NAME,
            'type' => 'text',
        )
    );

}
add_action( 'customize_register', 'theme_customize_register' );

/*=============================
 * WSTĘPY W POSTACH
 *=============================
 */

function custom_excerpt_length($length = 1000)
{
    return $length;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

function new_excerpt_more($more)
{
    return null;
}
add_filter('excerpt_more', 'new_excerpt_more');

add_action('init', 'my_add_excerpts_to_pages');
function my_add_excerpts_to_pages()
{
    add_post_type_support('page', 'excerpt');
    add_post_type_support('post', 'excerpt');
}

function the_excerpt_max_charlength($charlength)
{
    $str ="";
    $excerpt = get_the_content();
    $excerpt = strip_tags($excerpt, '<br>');
    //$excerpt = str_replace('&nbsp;','', $excerpt);
    $excerpt = ltrim($excerpt, '&nbsp; ');
    $charlength++;

    if (mb_strlen($excerpt) > $charlength) {
        //$str = '<p>';
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = -(mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            $str .= mb_substr($subex, 0, $excut);
        } else {
            $str .= $subex;
        }
        $str = rtrim($str, " .");
        $str .= "&nbsp;";
        $str .= '...';
    } else {
        $str .= $excerpt;
    }
    return $str;
}

/* simple title for all */
function get_simple_title()
{
    if (is_category()) {
        $title = sprintf('%s', single_cat_title('', false));
    } elseif (is_tag()) {
        $title = sprintf('%s', single_tag_title('', false));
    } elseif (is_author()) {
        $title = sprintf('%s', '<span class="vcard">' . get_the_author() . '</span>');
    } elseif (is_year()) {
        $title = sprintf('%s', get_the_date(_x('Y', 'yearly archives date format','theme')));
    } elseif (is_month()) {
        $title = sprintf('%s', get_the_date(_x('F Y', 'monthly archives date format', 'theme')));
    } elseif (is_day()) {
        $title = sprintf('%s', get_the_date(_x('F j, Y', 'daily archives date format', 'theme')));
    } elseif (is_tax('post_format')) {
        if (is_tax('post_format', 'post-format-aside', 'theme')) {
            $title = _x('Asides', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-gallery')) {
            $title = _x('Galleries', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-image')) {
            $title = _x('Images', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-video')) {
            $title = _x('Videos', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-quote')) {
            $title = _x('Quotes', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-link')) {
            $title = _x('Links', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-status')) {
            $title = _x('Statuses', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-audio')) {
            $title = _x('Audio', 'post format archive title', 'theme');
        } elseif (is_tax('post_format', 'post-format-chat')) {
            $title = _x('Chats', 'post format archive title', 'theme');
        }
    } elseif (is_post_type_archive()) {
        $title = sprintf('%s', post_type_archive_title('', false));
    } elseif (is_tax()) {
        $tax = get_taxonomy(get_queried_object()->taxonomy);
        /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
        $title = sprintf('%1$s: %2$s', $tax->labels->singular_name, single_term_title('', false));
    } else {
        $title = "";
    }

    /**
     * Filter the archive title.
     *
     * @since 4.1.0
     *
     * @param string $title Archive title to be displayed.
     */
    return apply_filters('get_the_archive_title', $title);
}

/*
/*=============================
 * WPML
 *=============================
 */
if (class_exists('SitePress')) {
    function language_selector_flags()
    {
        $languages = icl_get_languages('skip_missing=0&orderby=code');
        if (!empty($languages)) {
            foreach ($languages as $l) {
                if (!$l['active']) {
                    echo '<a href="' . $l['url'] . '">';
                    echo '<img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" />';
                    echo '</a>';
                }
            }
        }
    }

    function wpml_current_lang()
    {

        $languages = icl_get_languages('skip_missing=1');
        $curr_lang = array();
        if (!empty($languages)) {

            foreach ($languages as $language) {
                if (!empty($language['active'])) {
                    $curr_lang = $language; // This will contain current language info.
                    break;
                }
            }
        }

        return $curr_lang;
    }

    function languages_list_menu($currentlang = null)
    {
        $languages = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');

        $ret = '<div class="lang-chose">';
        /*if($currentlang){
          //$ret .=  '<img src="'.$currentlang['country_flag_url'].'" alt="'.$currentlang['native_name'].'"> <i class="fa fa-caret-down" aria-hidden="true"></i>';
          $ret .=  '<img src="'.$currentlang['country_flag_url'].'" alt="'.$currentlang['native_name'].'">';
        }*/
        $ret .= '<ul class="lang-switcher">';
        foreach ($languages as $language) {
            $flag = $language['country_flag_url'];
            $url = $language['url'];
            $isActive = $language['active'];
            $name = $language['native_name'];
            if ($isActive == 1) {
                $class = 'class="active"';
            } else {
                $class = "";
            }

            $ret .= '
            <li>
              <a href="' . $url . '" ' . $class . '>
                <img src="' . $flag . '" alt="' . $name . '" />
              </a>
            </li>
            ';

        }
        $ret .= '</ul>';
        $ret .= '</div>';
        return $ret;
    }
} //end check class

function disable_admin_bar_for_subscribers()
{
    if (is_user_logged_in()):
        global $current_user;
        if (!empty($current_user->caps['subscriber'])):
            add_filter('show_admin_bar', '__return_false');
        endif;
    endif;
}
add_action('init', 'disable_admin_bar_for_subscribers', 9);

function add_theme_caps()
{
    get_role('editor')->add_cap('edit_theme_options');
}
add_action('admin_init', 'add_theme_caps');


add_filter( 'wp_image_editors', 'change_graphic_lib' );
function change_graphic_lib($array) {
  return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}

?>
