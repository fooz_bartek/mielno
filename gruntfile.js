module.exports = function(grunt) {

    grunt.initConfig({
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass', 'ftp_push'],
                options: {
                    //livereload: true,
                },
            },

        },
        sass: {
            dist: {
                files: {
                    'wp-content/themes/mielno/stylePage.css': 'wp-content/themes/mielno/scss/main.scss'
                }
            }
        },
        ftp_push: {
            your_target: {
                options: {
                    authKey: "serverFooz",
                    host: "fooza.pro-linuxpl.com",
                    dest: "/public_html/mielno",
                    port: 21
                },
                files: [{
                    expand: true,
                    cwd: '.',
                    src: [
                        'wp-content/themes/mielno/stylePage.css'
                    ]
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ftp-push');

    grunt.registerTask('default', ['sass']);
    grunt.registerTask('default', ['sass']);

};
